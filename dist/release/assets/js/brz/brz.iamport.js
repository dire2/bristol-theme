var iamport = (function($){
	var DEFAULT_OPTIONS = {
		KEY : "8833553035500777",
		SECRET : "aOBWYkJRc8bKIxP13o289PQpOUBNKDXUryRqTAi5IuPr1Fs44vcdk3C9YdWIRUCjtuJm42sNwieyYHD2",

		HOST : "https://api.iamport.kr", 
		TOKEN_URL : "/users/getToken",
		GET_PAYMENT_URL : "/payments/",
		FIND_PAYMENT_URL : "/payments/find/",
		STATES_PAYMENT_URL : "/payments/status/",
		CANCEL_PAYMENT_URL : "/payments/cancel/",
		PREPARE_PAYMENT_URL : "/payments/prepare",
		SBCR_ONETIME_PAYMENT_URL : "/subscribe/payments/onetime/",
		SBCR_AGAIN_PAYMENT_URL : "/subscribe/payments/again/",
		TOKEN_HEADER : "Authorization"
	}

	function getToken(){
		var deffered = $.Deferred();
		if( !this.token ){
			$.ajax({
				method: "POST",
				url: DEFAULT_OPTIONS.HOST + DEFAULT_OPTIONS.TOKEN_URL,
				clossDomain : true,
				dataType : "json",
				data: { imp_key : DEFAULT_OPTIONS.KEY, imp_secret : DEFAULT_OPTIONS.SECRET }
			}).done(function( data ) {
				this.token = data.response;
				deffered.resolve( this.token );	
			}).fail(function( data ) {
				deffered.reject( data );	
			})
		}else{
			deffered.resolve( this.token );
		}
		return deffered.promise();
		/*
		if( !this.token ){
			$.ajax({
			  method: "POST",
			  url: DEFAULT_OPTIONS.HOST + DEFAULT_OPTIONS.TOKEN_URL,
			  clossDomain : true,
			  dataType : "json",
			  data: { imp_key : DEFAULT_OPTIONS.KEY, imp_secret : DEFAULT_OPTIONS.SECRET }
			})
			.done(function( data ) {
				console.log( "done" )
				console.log( data )
				this.token = data.response.access_token;
			})
			.fail(function( data ) {
				console.log( "fail" )
				console.log( data )
			})
			.always(function() {
				//alert( "complete" );
			});
		}else{
			return this.token;
		}

		headers: { "Authorization": token.access_token },
		beforeSend : function(xhr){
			xhr.setRequestHeader("Authorization",token.access_token);
			xhr.setRequestHeader("X-ImpTokenHeader", token.access_token);
		},

		*/
	}

	/*
	* 연동 처리 메소드 
	*/
	function getRequest(opt){
		return getToken()
			.then( function(token){
				opt.method = (opt.method || "GET").toUpperCase();
				var deffered = $.Deferred(),
					params = opt.params || {};

				if(opt.method=="GET"){ //
					opt.url += opt.params;
				}
				// header값으로 token 값이 전달되지 않아 우선 url로 넘기고있음
				opt.url += "?_token="+token.access_token;
				$.ajax({
					url: opt.url,
					method: opt.method,
					clossDomain : true,
					dataType : "json",
					data: params
				})
				.done(function( data ) {
					if( data.response){
						deffered.resolve( data );
					}else{
						deffered.reject( data );
					}
				})
				.fail(function( data ) {
					deffered.reject( data );
				});	

				return deffered.promise();
			});
	}

	/*
	* 상태별 상품 리스트 조회
	*/
	function getPaymentsByStatus(status){
		return getRequest({ 
			method : "GET",
			url : DEFAULT_OPTIONS.HOST + DEFAULT_OPTIONS.STATES_PAYMENT_URL,
			params : status
		})
	}


	/*
	* 아임포트 아이디로 결제 정보 조회
	*/
	function getPaymentByImpUid(imp_uid){
		return getRequest({ 
			method : "GET",
			url : DEFAULT_OPTIONS.HOST + DEFAULT_OPTIONS.GET_PAYMENT_URL,
			params : imp_uid
		})
	}

	/*
	* 가맹점지정 고유번호 아이디로 결제 정보 조회
	*/
	function getPaymentByMerchantUid(merchant_uid){
		return getRequest({ 
			method : "GET",
			url : DEFAULT_OPTIONS.HOST + DEFAULT_OPTIONS.FIND_PAYMENT_URL,
			params : merchant_uid
		})
	}

	/*
	* 주문 취소
	*/
	function cancel(params){
		return getRequest({ 
			method : "POST",
			url : DEFAULT_OPTIONS.HOST + DEFAULT_OPTIONS.CANCEL_PAYMENT_URL,
			params : params
		})
	}

	return {
		getPaymentsByStatus : getPaymentsByStatus,
		getPaymentByImpUid : getPaymentByImpUid,
		getPaymentByMerchantUid : getPaymentByMerchantUid,
		cancel : cancel
	}

})($);