'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var rename = require('gulp-rename');
var watch = require('gulp-watch');
var path = require('path');
var cache = require('gulp-cached');
var jslint = require('gulp-jslint');
var csslint = require('gulp-csslint');
var concatCss = require('gulp-concat-css');			// css 퍼일 병합을 위한 플러그인
var cleanCSS = require('gulp-clean-css');			// css 파일 정리를 위한 플러그인
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var serverConfiguration = require( "./server/server-config" );
var runSequence = require('run-sequence');			// gulp task 동기화 실행
var map = require('map-stream');
var options = require('gulp-options');
var dateUtils = require('date-utils');

var deployPath = path.join(__dirname + '/deploy/');
var builder;
var themePath;
var themePaths;
var framePaths;
var themeName;
var isSubTheme;

var serverJsName = 'index.js'
//var serverJsName = 'index_windows.js';

function getDate(format) {
	var d = new Date();
	return d.toFormat(format);
}

function build_srcJs(){
	var filePath = ( isSubTheme ) ? framePaths.srcJs.concat( themePaths.srcJs ) : framePaths.srcJs;
	return gulp.src(filePath)
		//.pipe(cache('build scripts'))
		//.pipe(jslint())
		.pipe(concat('index.js'))
		//.pipe(gulp.dest('./dist/js'))
		.pipe(gulp.dest(framePaths.releasePath+'assets/js'))
}

function build_vendors_js(){
	return gulp.src(['src/js/vendors/**/*.js'])
		//.pipe(cache('build scripts'))
		//.pipe(jslint())
		.pipe(concat('uikit.js'))
		//.pipe(gulp.dest('./dist/js'))
		.pipe(gulp.dest(framePaths.releasePath+'assets/js/libs'))
}

function build_sass(){

	var filePath = ( isSubTheme ) ? themePaths.basePath : __dirname;
	//console.log( "build_sass : ", filePath);
	return gulp.src(filePath + "/src/sass/*.scss")
		.pipe(sourcemaps.init({loadMaps: true, debug: true}))
		//.pipe(sass({outputStyle:"compressed"})
		.pipe(sass({outputStyle:"expanded"}))
		.on('error', sass.logError)
		.pipe(rename('common.css'))
		.pipe(sourcemaps.write('./src'))
		//.pipe(gulp.dest('./dist/css/'))
		.pipe(gulp.dest(framePaths.releasePath+'assets/css/'))
}

function initInfo( theme ){
	themeName = theme;
	isSubTheme = ( theme != "" );

	var basePath = __dirname;
	var releasePath = basePath + "/dist/release/";
	framePaths = {
		basePath : basePath,
		releasePath : releasePath,
		html : releasePath+'**/*.html',
		js : releasePath+'**/*.js',
		vendorsJs : ['src/js/vendors/**/*.js'],
		srcJs : [
			'src/js/*.js',
			'src/js/extands/*.js',
			'src/js/component/*.js',
			'src/js/modules/*.js'
		],
		css : releasePath+'**/*.css',
		images : releasePath+'assets/images/**/*.*',
		font : releasePath+'assets/fonts/**/*.*',
		sass : basePath +  "/src/sass/**/*.scss",
		meta : releasePath + 'metadata.xml'
	}

	if( themeName != "" ){
		var themeBasePath = path.join(__dirname, "theme/" + theme  + "/");
		var themeReleasePath = themeBasePath + "release/";
		themePaths = {
			basePath : themeBasePath,
			releasePath : themeReleasePath,
			html : themeReleasePath+'**/*.html',
			js : themeReleasePath+'**/*.js',
			srcJs : themeBasePath + "src/js/**/*.js",
			images : themeReleasePath+'assets/images/**/*.*',
			font : themeReleasePath+'assets/fonts/**/*.*',
			css : themeReleasePath +'**/*.css',
			sass : themeBasePath + "src/sass/**/*.scss",
			meta : themeReleasePath + 'metadata.xml'
		}
	}
}

function startWatch( theme ){
	console.log('Start Watch');

	builder = require('./server/'+serverJsName);

	initInfo( theme );

	frameWatch();


	//var buildlist = ['merge-meta', ['build-sass', 'build-srcJs', 'build-vendors-js'], 'dev-frame-html'];
	var buildlist = ['merge-meta', ['build-sass', 'build-srcJs', 'build-vendors-js']];
	if( theme != "" ){
		console.log( theme, " Theme");
		themeWatch();
		//gulp.src(themePaths.html).pipe(mapToBuildHtml());
		buildlist.push(['dev-theme-html']);
	}

	console.log( buildlist );

	buildlist.push( function(){
		console.log( "dev setting complete!!!!!!!!" );
	});

	runSequence.apply(null, buildlist);
}


function mergeMeta(){
	var themeMeta = null;
	if( themePaths != null ){
		themeMeta = themePaths.meta;
	}

	console.log( themeMeta );
	return builder.mergeMeta( deployPath, framePaths.meta, themeMeta, 'dev', themeName);
}
gulp.task('merge-meta', mergeMeta );


function frameWatch(){


	/*
	builder.upload_css(file, framePaths.releasePath);
	builder.upload_js(file, framePaths.releasePath);
	builder.upload_html(file, framePaths.releasePath);
	*/

	watch(framePaths.sass, function(file){
		build_sass();
	});

	watch(framePaths.html, function (file) {
		builder.upload_html(file, framePaths.releasePath);
	});

	watch(framePaths.js, function (file) {
		builder.upload_js(file, framePaths.releasePath);
	});

	watch(framePaths.srcJs, function (file) {
		build_srcJs();
	});

	watch(framePaths.vendorsJs, function (file) {
		build_vendors_js();
	});

	watch(framePaths.css, function (file) {
		builder.upload_css(file, framePaths.releasePath);
	});

	watch(framePaths.meta, function (file) {
		console.log( "Core Meta 변경됨");
	});
}

function themeWatch(){
	/*
	builder.upload_html(file, themePaths.releasePath);
	builder.upload_js(file, themePaths.releasePath);
	builder.upload_css(file, themePaths.releasePath);
	*/
	watch(themePaths.sass, function (file) {
		build_sass();
	});

	watch(themePaths.html, function (file) {
		builder.upload_html(file, themePaths.releasePath);
	});

	watch(themePaths.js, function (file) {
		builder.upload_js(file, themePaths.releasePath);
	});

	watch(themePaths.srcJs, function (file) {
		build_srcJs();
	});

	watch(themePaths.css, function (file) {
		builder.upload_css(file, themePaths.releasePath);
	});

	watch(themePaths.meta, function (file) {
		console.log( "Theme Meta 변경됨");
	});

}


gulp.task('build-sass', build_sass);
gulp.task('build-srcJs', build_srcJs);
gulp.task('build-vendors-js', build_vendors_js);

gulp.task('build-frame-html', function(){
	//console.log( framePaths.html )
	return gulp.src( framePaths.html )
		.pipe(gulp.dest(deployPath))
});
gulp.task('build-frame-css', function(){
	//console.log( framePaths.css )
	return gulp.src( framePaths.css )
		.pipe(gulp.dest(deployPath), {overWrite:true})

	//return gulp.src(framePaths.css ).pipe(mapToBuildCss());

});
gulp.task('build-frame-js', function(){
	//console.log( framePaths.js )
	return gulp.src( framePaths.js )
		.pipe(gulp.dest(deployPath))
});
gulp.task('build-frame-images', function(){
	//console.log( framePaths.images )
	return gulp.src( framePaths.images )
		.pipe(gulp.dest(deployPath+'assets/images'))
});
gulp.task('build-frame-font', function(){
	//console.log( framePaths.font )
	return gulp.src( framePaths.font )
		.pipe(gulp.dest(deployPath+'assets/fonts'))
});


gulp.task('build-theme-html', function(){
	//console.log( themePaths.html )
	return gulp.src( themePaths.html )
		.pipe(gulp.dest(deployPath))
});
gulp.task('build-theme-css', function(){
	//console.log( themePaths.css )
	return gulp.src( themePaths.css )
		.pipe(gulp.dest(deployPath))

	//return gulp.src(themePaths.css ).pipe(mapToBuildCss());
});
gulp.task('build-theme-js', function(){
	//console.log( themePaths.js )
	return gulp.src( themePaths.js )
		.pipe(gulp.dest(deployPath))
});
gulp.task('build-theme-images', function(){
	//console.log( themePaths.images )
	return gulp.src( themePaths.images )
		.pipe(gulp.dest(deployPath+'assets/images'))
});
gulp.task('build-theme-font', function(){
	//console.log( themePaths.font )
	return gulp.src( themePaths.font )
		.pipe(gulp.dest(deployPath+'assets/fonts'))
});

gulp.task('build-meta', function(){
	var filePath = ( isSubTheme ) ? themePaths.meta : framePaths.meta;
	return gulp.src( filePath )
		.pipe(gulp.dest(deployPath))
});


gulp.task('dev-frame-html', function(){
	return gulp.src(framePaths.html).pipe(mapToBuildFrameHtml());
});

gulp.task('dev-theme-html', function(){
	return gulp.src(themePaths.html).pipe(mapToBuildThemeHtml());
});


var mapToBuildFrameHtml = function(file, cb) {
	return map(function(file, cb) {
		//console.log(file.path);
		builder.upload_html(file, framePaths.releasePath, false);
		cb(null, file);
	});
};

var mapToBuildThemeHtml = function(file, cb) {
	return map(function(file, cb) {
		//console.log(file.path);
		builder.upload_html(file, themePaths.releasePath, false);
		cb(null, file);
	});
};



/*
var mapToBuildCss = function(file, cb) {
	console.log( 'mapToBuildCss');
	return map(function(file, cb) {
		return gulp.src( file.path ).pipe(gulp.dest(getDeployFolderPath(file.path)));
		cb(null, file);
	});
};


// 배포 위치 파일 경로 반환
function getDeployFilePath(filePath) {
	if( filePath.indexOf(themePaths.basePath) > -1 ) {
		filePath = themePaths.basePath + (filePath.replace(themePaths.releasePath,'')).replace('/assets/sass', '/assets/css');
		return filePath.replace(themePaths.basePath, deployPath);
	}
	return filePath.replace(framePaths.releasePath, deployPath);
}

// 배포 위치 폴더 경로 반환
function getDeployFolderPath(filePath) {
	return path.join( getDeployFilePath(filePath) + '/../' );
}
*/

function bulidAll( theme ){
	initInfo( theme );

	var name = ( isSubTheme ) ? theme + "-" : "";
	deployPath += '' + name + getDate('MMDD-HH24MI') +'/';

	var frameBuildList = [
		['build-sass', 'build-srcJs', 'build-vendors-js'],
		['build-frame-html', 'build-frame-css', 'build-frame-js', 'build-frame-images', 'build-frame-font']];

	var themeBuildList = [['build-theme-html', 'build-theme-css', 'build-theme-js','build-theme-images', 'build-theme-font']];

	var buildlist = frameBuildList;

	if( isSubTheme ){
		buildlist = frameBuildList.concat( themeBuildList );
	}

	console.log( buildlist );

	buildlist.push( function(){
		builder = require('./server/'+serverJsName);
		builder.mergeMeta( deployPath, framePaths.meta, themePaths.meta, 'build', themeName );
		console.log( "build complete!!!!!!!!" );
	});

	runSequence.apply(null, buildlist);
}

gulp.task('default', function(){
	startWatch("");
});

//gulp dev --theme=watsons
gulp.task('dev', function(){
	//build_sass(['theme/watsons/src/sass/**/*.scss']);
	var theme = "";
	if( options.has('theme') ){
		theme = options.get('theme');
	}

	startWatch(theme);
});

//gulp build --theme=watsons
gulp.task('build', function(){
	//build_sass(['theme/watsons/src/sass/**/*.scss']);
	//startWatch("watsons");

	var theme = "";
	if( options.has('theme') ){
		theme = options.get('theme');
	}

	bulidAll( theme );
});
