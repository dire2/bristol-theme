
var fs = require('fs');
var path = require('path');
var mysql = require('mysql');
var libxmljs = require("libxmljs");
var dt = require('date-utils');

/***
utils
***/
// 지정된 포맷으로 현재 일시 반환 
function getDate(format) {
	var d = new Date();
	return d.toFormat(format);
}
function logTime() {
	return "["+getDate('HH24:MI:SS')+"] ";
}
function isString(v) { return typeof v === 'string'; }
function log(arg, arg2) {
	if ( arg2 === void 0 ){
		if( typeof arg === 'string' ) {
			console.log(logTime(), arg);
		} else {
			console.log(logTime(), arg);
		}
	} else {
		if( typeof arg === 'string' && typeof arg2 === 'string') {
			console.log(logTime(), arg, arg2);
		} else if (typeof arg === 'string') {
			console.log(logTime(), arg, arg2);
			console.log(arg2);
		}
	}
}
function getValue(arg) {
	if( arg !== void 0 && arg !== null) {
		return arg.value();
	}
	return "";
}



// 설정 정보 
var serverConfiguration = require( "../server/server-config" );


// pool 생성 
var connectionPool = mysql.createPool(serverConfiguration.gulp.db);


//test
//libxmljs 

var metaXml;
var metaMap;
var metaInfo
var themeInfo;

function mergeMeta( deployPath, filePath, themeFilePath, type, themeName ){	
	console.log('mergeMeta : ', type );
	var metaData = "";
	var frameData = "";
	var themeData = "";

	if( filePath != null ){
		try {
			fs.readFile(filePath, {encoding: 'utf-8'}, function(err,fdata){
				if( !err ){
					metaData = fdata;
					metaXml = libxmljs.parseXml(metaData);
					metaInfo = metaXml.find('//theme')[0];
					metaInfo.attr("name", themeName);

					if( themeFilePath != null ){
						//log('themeFilePath : ', themeFilePath);
						try {
							themeData = fs.readFileSync(themeFilePath, 'utf-8' );

							//frame과 theme 병합
							metaData = metaData.replace('<!--/* theme merge */-->', String( libxmljs.parseXml(themeData).find('*') ).split(',').join('\n\t') );

							var themeXml = libxmljs.parseXml(themeData);
							metaXml = libxmljs.parseXml(metaData);
							metaInfo = metaXml.find('//theme')[0];

							var info = themeXml.find('//theme')[0];
							metaInfo.attr("autoImport", getValue( info.attr("autoImport") ));
							metaInfo.attr("deleteOlder", getValue( info.attr("deleteOlder") ));
							metaInfo.attr("name", getValue( info.attr("name") ));
							metaInfo.attr("overWrite", getValue( info.attr("overWrite") ));
						}catch (err) {
							log( '테마 xml 없음' );
							//log( err );
						}
					}else{
						//log( '테마 없음' );
					}

					if( type == 'build'){
						console.log(deployPath+'metadata.xml');
						fs.writeFile(deployPath+'metadata.xml', metaXml, encoding='utf8', function(){
							
						})
					}
					reloadMeta();
				}
			});
		} catch (err) {
			log(err);
		}
	} 
}

function reloadMeta() {
	console.log('reloadMeta');
	// https://github.com/polotek/libxmljs
	//var filePath = path.join(__dirname, "../dist/release/metadata.xml");
	//log( filePath );

	metaMap = {};
	themeInfo = {};

	var xmlDoc = metaXml;
	var info = xmlDoc.find('//theme');

	themeInfo[ 'name' ] = getValue( info[0].attr("name") );
	//log( "themeInfo", themeInfo );

	var list = xmlDoc.find('//template');
	var len = list.length;
	var el, key;
	for( var i = 0, len = list.length; i < len; i++ ){
		el = list[i];

		if( el !== void 0 && el !== null ) {
			key = getValue( el.attr("path") );
			if( key !== "" ) {
				metaMap[ key ] = {
					name : getValue( el.attr("name") ),
					fileType : getValue( el.attr("FileType") ),
					templateType : getValue( el.attr("TemplateType") ),
					path : key
				}
			}
		}
	}	



	/*
	try {
		fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
			if (!err){

				var xmlDoc = libxmljs.parseXml(data);
				var info = xmlDoc.find('//theme');

				themeInfo[ 'name' ] = getValue( info[0].attr("name") );
				log( "themeInfo", themeInfo );

				var list = xmlDoc.find('//template');
				var len = list.length;
				var el, key;
				for( var i = 0, len = list.length; i < len; i++ ){
					el = list[i];

					if( el !== void 0 && el !== null ) {
						key = getValue( el.attr("path") );
						if( key !== "" ) {
							metaMap[ key ] = {
								name : getValue( el.attr("name") ),
								fileType : getValue( el.attr("FileType") ),
								templateType : getValue( el.attr("TemplateType") ),
								path : key
							}
						}
					}
				}
			}else{
				log(err);
			}

			console.log( "완료");
		});
	} catch (err) {
		log(err);
	}
	*/
}

function upload( changePath, basePath, fileType ) {
	changePath = changePath.split(String.fromCharCode(92)).join('/');
	basePath = basePath.split(String.fromCharCode(92)).join('/');

	var realPath = "/" + changePath.replace( basePath, "").replace("."+fileType, "");
	log("changePath ", changePath);
	log("basePath ", basePath);
	log("realPath ", realPath);
	log("themeInfo ", themeInfo);

	//'/Users/chohhdd/dev/node/breeze-ui/dev/site/converse/cv/assets/css/partials/common.css'
	//<template name="defaultLayout" TemplateType="Layout" 	FileType="HTML" path="/layout/defaultLayout" />

	fs.readFile(changePath, {encoding: 'utf-8'}, function(err,data){
		if (!err){
			var replaceData = data;
			/* 
			replaceData = replaceData.split("/release").join("");
			replaceData = replaceData.split("release").join("");
			*/
			replaceData = replaceData.split('href="/assets/').join('href="/cmsstatic/theme/' + themeInfo.name + '/assets/');
			replaceData = replaceData.split("href='/assets/").join("href='/cmsstatic/theme/" + themeInfo.name + "/assets/");

			replaceData = replaceData.split('src="/assets/').join('src="/cmsstatic/theme/' + themeInfo.name + '/assets/');
			replaceData = replaceData.split("src='/assets/").join("src='/cmsstatic/theme/" + themeInfo.name + "/assets/");

			replaceData = replaceData.split('url("/assets/').join('url("/cmsstatic/theme/' + themeInfo.name + '/assets/');
			replaceData = replaceData.split("url('/assets/").join("url('/cmsstatic/theme/" + themeInfo.name + "/assets/");
			replaceData = replaceData.split('url(/assets/').join('url(/cmsstatic/theme/' + themeInfo.name + '/assets/');
			
			//log('replaceData', replaceData )
			updateContent(realPath, replaceData);
		}else{
			log(err);
		}
	});
}

function updateContent(realPath, content) {
	// DB 커넥션 풀에서 커넥션 얻기 
	try {
		connectionPool.getConnection(function(connectionError,connection){
			if(connectionError) {
				log(connectionError);
				return;
			}

			//var updateColumns = {tmplt_source : content, date_updated : 'now()'};

			// insert & update query 
			var queryString = 
				// ' select b.page_tmplt_id' + 
				// ' from brzc_page_tmplt b' + 
				// ' where b.page_tmplt_id = (' + 
				// 	' select a.page_tmplt_id ' + 
				// 	' from blc_page_tmplt a' + 
				// 	' where a.tmplt_path = ?' + 
				// 	' and a.page_tmplt_id > 1' + 
				// 	' limit 1' + 
				// ' )';
// 여러 테마가 있더라도 동일한 경로면 다 변경 처리
				' update brzc_page_tmplt' + 
				'    set tmplt_source = ?' + 
				'      , date_updated = now()' + 
				'  where page_tmplt_id in (' + 
					' select a.page_tmplt_id ' + 
					' from blc_page_tmplt a' + 
					' where a.tmplt_path = ?' + 
					' and brzc_page_tmplt.theme_id > 1'+ 
					' and a.page_tmplt_id > -10000' +  // admin theme 충돌문제가 있어 임시로 특정 번호 이상만 변경 처리
//					' limit 1' + 
				' )';

			// run query
			log("(---- inner query ==> ");
			//log("---- select a.page_tmplt_id from blc_page_tmplt a where a.tmplt_path = '" +realPath +  "' and a.page_tmplt_id > 0 limit 1");
			// 여러 테마가 있더라도 동일한 경로면 다 변경 처리
			log("---- select a.page_tmplt_id from blc_page_tmplt a where a.tmplt_path = '" +realPath +  "'");
			var query = connection.query(queryString, [content, realPath], function(queryError, result) {

				//log("mysql query", query.sql);

				// 에러 발생 시 
				if(queryError){
					// connection 반환 
					connection.release();
					log(queryError);
					return;
				}

				// connection 반환 
				connection.release();

				log("result=====================> " + realPath);
				log(result);
				log("\n\n\n");
			});
		});
	} catch (err) {
		connection.release();
		log(err);
	}
}

function changeMeta() {
	reloadMeta();
}

function upload_html(args, basePath) {
	//log(args);
	upload( args.path, basePath, "html" );
}
// js, css 변경 감지도 추가 할것.
// 단, theme upload 되야 함. 
// 못찾는 정보의 경우 예외처리 필요..
function upload_js(args, basePath) {
	log(args);

	// mockData 예외처리 
	if(args.path.indexOf("/mockData/") > -1 ) {
		//console.log("====> mockData 처리 예외처리");
		return;
	}

	upload( args.path, basePath, "js" );
}
function upload_css(args, basePath) {
	log(args);
	upload( args.path, basePath, "css" );
}



module.exports = {
	upload_html : upload_html,
	upload_js : upload_js,
	upload_css : upload_css,
	reloadMeta : reloadMeta,
	mergeMeta : mergeMeta
}


// exports.start = function (args) {
// }