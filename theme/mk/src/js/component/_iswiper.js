(function(Core){
	var ISwiper = function(){
		'use strict';

		var $this, args, $slider, opt, $list, defaultWidth, widthMatch;
		var setting = {
			selector:'[data-component-slider]',
			list:'.slider-wrapper, ul'
		}
		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;

				$this = $(setting.selector);
				$list = $this.find(setting.list);

				args = arguments[0];

				//var maxSlides = (Core.Utils.mobileChk === null) ? args.maxSlides||1 : args.minSlides||1;
				//console.log(_GLOBAL.LAYOUT.MAX_WIDTH / (args.maxSlides || 1) - (args.slideMargin||0));

				defaultWidth = args.slideWidth || _GLOBAL.LAYOUT.MAX_WIDTH / (args.maxSlides || 1) - (args.slideMargin || 0);
				opt = {
					//slideWidth:args.slideWidth || $this.width() / maxSlides,
					slideWidth:defaultWidth,
					minSlides:args.minSlides || 1,
					maxSlides:args.maxSlides || 1,
					moveSlides:args.moveSlide||args.maxSlide,
					slideMargin:parseInt(args.slideMargin)||0,
					auto:(args.auto != undefined) ? args.auto : false,
					autoHover: true,
					pager:(args.pager != undefined) ? args.pager : true,
					controls:(args.controls != undefined) ? args.controls : false,
					responsive:(args.responsive != undefined) ? args.responsive : true,
					infiniteLoop:(args.loop != undefined) ? args.loop  :  false,
					mode:args.mode || 'horizontal',
					onSliderLoad:function($slideElement, currnetIndex){
						setTimeout(function(){
							_self.fireEvent('onInit', $slider, [$slideElement, currnetIndex]);
						});
					},
					onSlideAfter: function($slideElement, oldIndex, newIndex){
						_self.fireEvent('slideAfter', $slider, [$slideElement, oldIndex, newIndex]);

                        /*setTimeout(function(e) {
                            $(window).trigger("scroll");
                        }, 10);*/
                    },
					onSlideBefore: function($slideElement, oldIndex, newIndex){
						_self.fireEvent('slideBefore', $slider, [$slideElement, oldIndex, newIndex]);

                        /*setTimeout(function(e) {
                            $(window).trigger("scroll");
                        }, 10);*/
                    }

				}

				if( opt.minSlides == 1 ){
					widthMatch = matchMedia("all and (max-width: 767px)");
					var widthHandler = function(matchList) {
					    if (matchList.matches) {
					    	opt.slideWidth = "767px";
					    	if( $slider ){
					    	    $slider.reloadSlider( opt );
					    	}else{
					    	    $slider = $list.bxSlider(opt);
					    	}
					    } else {
							opt.slideWidth = defaultWidth;
					    	if( $slider ){
					    	    $slider.reloadSlider( opt );
					    	}else{
					    	    $slider = $list.bxSlider(opt);
					    	}
					    }
					};
					widthMatch.addListener(widthHandler);
					widthHandler(widthMatch);
				}else{
					$slider = $list.bxSlider(opt);
				}

				$this.find('.bxslider-controls .btn-next').on('click', function(e) {
					e.preventDefault();
					$slider.goToNextSlide();
				});
				$this.find('.bxslider-controls .btn-prev').on('click', function(e) {
					e.preventDefault();
					$slider.goToPrevSlide();
				});

				return this;
			},
			reloadSlider:function(){
				$slider.reloadSlider( opt );
			},
			redrawSlider:function(){
				$slider.redrawSlider();
			},
			goToSlide:function(index){
				$slider.goToSlide(index);
			},
			goToNextSlide:function(){
				$slider.goToNextSlide();
			},
			goToPrevSlide:function(){
				$slider.goToPrevSlide();
			},
			destroySlider:function(){
				$slider.destroySlider();
			},
			getCurrentSlide:function(){
				return $slider.retCurrentSlide();
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_slider'] = {
		constructor:ISwiper,
		attrName:'data-component-slider',
		reInit:true
	}
})(Core);