(function(Core){
	var ThumbNail = function(){
		var setting = {
			selector:'[data-component-thumbnail-mk]',
			container:'.thumb-wrap',
			scrollWrap:'.scroll-Wrap',
			list:'.thumb-list'
		}

		var $this, $container, $list, $zoomGalleryWrap, arrThumbList=[], iScroll, args, currentScrollCount=0,
			prevCode, nextCode, arrGalleryOffsetTop, currentScrollTop, $galleryWrap, $mobileGalleryWrap, setTimeId, arrthumbClickOffsetTop = [], galleryWrapHeight=0;

		var Closure = function(){}
		Closure.prototype.setting = function(){
			var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
		}
		Closure.prototype.init = function(){
			var _self = this;

			$this = $(setting.selector);
			$galleryWrap = $this.siblings().filter('[data-gallery-pc]');
			$mobileGalleryWrap = $this.siblings().filter('[data-gallery-mobile]');
			$zoomGalleryWrap  = $this.siblings().filter('[data-gallery-zoom]');
			$container = $this.find(setting.container);
			$list = $this.find(setting.list);
			args = arguments[0];

			var arrList = [];

			$container.find(setting.list).each(function(i){
				var data = Core.Utils.strToJson($(this).attr('data-thumb'), true);
				var imgUrl = $(this).find('img').attr('src').replace(/\?[a-z]+/g, '');
				var imgAlt = $(this).find('img').attr('alt') || '';
				var pushIS = true;

				data.thumbUrl = imgUrl;
				data.imgAlt = imgAlt;

				/* 중복 이미지 처리 */
				for(var i=0; i < arrList.length; i++){
					if(arrList[i].thumbSort === data.thumbSort && arrList[i].thumbUrl === data.thumbUrl){
						pushIS = false;
						return;
					}
				}

				if(pushIS){
					arrList.push(data);
					arrThumbList.push(data);
				}
			});


			$container.on('click', 'li', function(e){
				e.preventDefault();

				if(!$(this).hasClass('active')){
					$('html, body').stop().animate({scrollTop:scrollArea.setScrollPer(arrthumbClickOffsetTop[$(this).index()])}, 300);
					_self.fireEvent('changeIndex', this, [$(this).index()]);
				}
			});

			$container.on('mouseenter', 'li', function(e){
				e.preventDefault();

				$(this).find('img').addClass('hover');
			});

			$container.on('mouseleave', 'li', function(e){
				e.preventDefault();

				$(this).find('img').removeClass('hover');
			});


			$galleryWrap.click(function(){
				$('html').addClass('uk-modal-page');
				$('body').css('paddingRight', 15);
				$zoomGalleryWrap.addClass('show');
				$zoomGalleryWrap.scrollTop($(document).scrollTop());
			});

			$zoomGalleryWrap.click(function(){
				if($('#quickview-wrap').length <= 0){
					$('html').removeClass('uk-modal-page');
					$('body').removeAttr('style');
				}
				$(this).removeClass('show');
			});


			scrollArea = Core.Scrollarea.setScrollArea(window, document, function(per, scrollTop){

				prevCode = (currentScrollCount <= 0) ? 0 : currentScrollCount - 1;
				nextCode = (currentScrollCount >= $galleryWrap.children().length - 1) ? $galleryWrap.children().length - 1 : currentScrollCount + 1;

				if(currentScrollTop >= per && per < arrGalleryOffsetTop[prevCode+1]){
					currentScrollCount = prevCode;
					$container.find('.thumb-list').eq(currentScrollCount).addClass('active').siblings().removeClass('active');
				}else if(currentScrollTop <= per && per > arrGalleryOffsetTop[nextCode]){
					currentScrollCount = nextCode;
					$container.find('.thumb-list').eq(currentScrollCount).addClass('active').siblings().removeClass('active');
				}

				currentScrollTop = per;
			}, 'thumbNail');

			this.setThumb(args.sort);
			return this;
		}
		Closure.prototype.setThumb = function(sort){

			var _self = this;
			var sortType = sort || args.sort;
			var arrThumbData = arrThumbList.filter(function(item, index, array){
				if(item.thumbSort === sortType || item.thumbSort === 'null'){
					return item;
				}
			});

			var galleryTemplate = Handlebars.compile($("#product-gallery-template").html())(arrThumbData);
			var thumbTemplate = Handlebars.compile($("#product-thumb-template").html())(arrThumbData);
			var mobileTemplate = Handlebars.compile($("#product-gallery-template-mobile").html())(arrThumbData);
			var zoomTemplate = Handlebars.compile($("#product-gallery-zoom").html())(arrThumbData);

			arrGalleryOffsetTop = [];
			currentScrollTop = Math.round($(document).scrollTop() / ($(document).height() - $(window).height()) * 100);

			$galleryWrap.empty().append(galleryTemplate);
			$container.empty().append(thumbTemplate);
			$mobileGalleryWrap.empty().append(mobileTemplate);
			$zoomGalleryWrap.find('.gallery-images').empty().append(zoomTemplate);
			$this.css('margin-top', -$container.height() / 2);

			Core.moduleEventInjection(mobileTemplate);



			// thumb nail offset 계산
			clearTimeout(setTimeId);
			setTimeId = setTimeout(function(){
				arrGalleryOffsetTop[0] = Math.round(($galleryWrap.children().eq(0).offset().top + $galleryWrap.children().eq(0).height() / 2 - $('header').height()) / ($(document).height() - $(window).height()) * 100);
				arrthumbClickOffsetTop[0] = 0;

				for(var i=0; i<$galleryWrap.children().length; i++){
					var top = $galleryWrap.children().eq(i).offset().top + $galleryWrap.children().eq(i).height() / 2 - $('header').height();
					var thumbTop = $galleryWrap.children().eq(i).offset().top + $galleryWrap.children().eq(i).height() - $('header').height();

					arrGalleryOffsetTop.push(Math.round(top / ($(document).height() - $(window).height()) * 100));
					arrthumbClickOffsetTop.push(Math.round(thumbTop / ($(document).height() - $(window).height()) * 100));
					if(currentScrollTop >= arrGalleryOffsetTop[i]){
						currentScrollCount = i;
					}
				}

				$container.find('.thumb-list').eq(currentScrollCount).addClass('active');
				//galleryWrapHeight
				galleryWrapHeight = $galleryWrap.height();

			}, 200);


		}
		Closure.prototype.setPosition = function(state){
			if(state == 'fixed'){
				$this.removeClass('absolute').addClass('fixed');
			}else if(state == 'absolute'){
				$this.removeClass('fixed').addClass('absolute').css('bottom', 460);
			}
		}
		Closure.prototype.getGalleryHeight = function(){
			return galleryWrapHeight;
		}
		Closure.prototype.getGalleryWrapHeight = function(){
			return galleryWrapHeight;
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_thumbnail_mk'] = {
		constructor:ThumbNail,
		attrName:'data-component-thumbnail-mk'
	};
})(Core);
