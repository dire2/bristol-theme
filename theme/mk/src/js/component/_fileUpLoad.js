(function(Core){
	var FileUpLoad = function(){
		'use strict';

		var $this, $form, $inputFiles, $progress, $uploadBtn, currentIndex=0;
		var setting = {
			selector:'[data-component-file]',
			form:'#fileupload-form',
			inputFiles:'#input-file',
			uploadBtn:'[data-upload-btn]',
			maxLenth:5
		}

		var setImgPreview = function(target){
			var _self = target;
			var _this = this;

			if($(this).val() === '') return false;

			$form.ajaxSubmit({
				success:function(data){
					upImageResult.call(_self, data);
				},
				error:function(data){
					_self.fireEvent('error', [data]);
				}
			});
		}

		var upImageResult = function(data){
			if(data.result){
				this.fireEvent('upload', this, [data.fileName, data.fullUrl]);
			}else if(data.result === 'error'){
				this.fireEvent('error', this, [data.errorMessage]);
			}else{
				this.fireEvent('error', this, ['이미지 전송을 실패하였습니다.']);
			}
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				var cUrl = Core.Utils.url.getCurrentUrl();

				$this = $(setting.selector);
				$uploadBtn = $this.find(setting.uploadBtn);
				$inputFiles = $(setting.inputFiles);
				$form = $(setting.form);

				$uploadBtn.click(function(e){
					e.preventDefault();

					if(currentIndex >= setting.maxLenth){
						_self.fireEvent('error', this, ['최대'+setting.maxLenth+'장 까지만 업로드 가능합니다.']);
						return false;
					}

					//appView일때 toapp 호출
					if(Core.Utils.appViewChk()){
						location.href='toapp://attach?uploadUrl='+ location.origin + $form.attr('action') +'&mediatype=image&callback=Core.getModule("module_review_write").moduleConnect&imagecount=1';
					}else{
						$inputFiles.trigger('click');
					}
				});

				$inputFiles.change(function(e){
					setImgPreview.call(this, _self);
				});

				return this;
			},
			setCurrentIndex:function(index){
				currentIndex = index;
			},
			setToappUploadImage:function(data){
				upImageResult.call(this, data);
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_file'] = {
		constructor:FileUpLoad,
		attrName:'data-component-file'
	}
})(Core);