(function(Core){
	var SearchField = function(){
		'use strict';

		var $this, $btn, $input, $resultWrap, opt, searchTxt = '', _self, validateIS = false, isAction = true;
		var setting = {
			selector:'[data-component-searchfield]',
			resultWrap:'.result-wrap',
			btn:'.btn_search',
			input:'.input-textfield',
			attrName:'data-component-searchfield',
			resultTemplate:''
		}


		//dummy data
		var dummyJson = '{"result":"success","query":"척테일러","results":[{"keywordCnt":0,"keyword":"척테일러"},{"keywordCnt":0,"keyword":"척테일러올스타`70"},{"keywordCnt":0,"keyword":"척테일러올스타"},{"keywordCnt":0,"keyword":"척테일러70"},{"keywordCnt":0,"keyword":"척테일러올스타투"},{"keywordCnt":0,"keyword":"척테일러레더"},{"keywordCnt":0,"keyword":"척테일러올스타클래식"},{"keywordCnt":0,"keyword":"척테일러2"},{"keywordCnt":0,"keyword":"척테일러올스타투부츠레더"},{"keywordCnt":0,"keyword":"척테일러올스타레더"}]}';


		var resultFunc = function(data){
			var json = (typeof data === Object) ? data : Core.Utils.strToJson(data.responseText || data, true);
			if(json.results.length > 0){
				addTemplate(json.results);
			}else{
				if(opt.complete !== 'auto'){
					UIkit.modal.alert('검색결과가 없습니다.');
					$('.uk-modal-close').text('Ok');

				}
			}

			isAction = true;
		}

		var addTemplate = function(data){
			if(setting.resultTemplate === ''){
				UIkit.notify('template is not defined', {timeout:3000,pos:'top-center',status:'warning'});
				return;
			}

			var template = Handlebars.compile($(setting.resultTemplate).html())(data);
			$resultWrap.empty().append(template);
		}

		var action = function(){
			if(searchTxt !== ''){
				_self.fireEvent('beforeSubmit', this, [searchTxt]);

				if(opt.hasOwnProperty('api')){
					Core.Utils.ajax(opt.api, 'GET', {'q':searchTxt}, resultFunc);
				}else if(opt.hasOwnProperty('submit')){
					_self.fireEvent('submit', this, [$(opt.submit), searchTxt]);
					$(opt.submit).submit();
				}
			}else{
				//UIkit.modal.alert(opt.errMsg);
				$input.setErrorLabel(opt.errMsg);
			}
		}

		//return prototype
		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				_self = this;
				opt = arguments[0];
				$this = $(setting.selector);
				$resultWrap = $this.find(setting.resultWrap);
				$btn = $this.find(setting.btn);

				$input = Core.getComponents('component_textfield', {context:$this, selector:'.input-textfield'}, function(){
					this.addEvent('focusin', function(e){
						$resultWrap.addClass('active');
					});

					this.addEvent('focusout', function(e){
						searchTxt = $(this).val();
					});

					this.addEvent('enter', function(e){
						searchTxt = $(this).val();

						if(isAction && searchTxt !== ''){
							isAction = false;
							action();
						}
					});

					if(opt.hasOwnProperty('autoComplete')){
						this.addEvent('keyup', function(e){
							// 비동기 호출 resultFunc callback 함수 넘김
							Core.Utils.ajax(opt.autoComplete, 'POST', {'q':$(this).val()}, resultFunc);
						});
					}
				});

				$this.off('mouseleave').on('mouseleave', function(){
					$resultWrap.removeClass('active');
				});

				$this.off('mouseenter').on('mouseenter', function(){
					$resultWrap.addClass('active');
				});
				
				// 상품 주문시 시군구를 검색을 통해 입력하지 않고 직접 입력하는 경우가 있어 재검색 버튼을 추가 검색을 통한 주소는 disable처리
				$('.re_search').on('click', function(e){
					e.preventDefault();
					var $zipCodeDisplay = $('.tit').find('[data-postalCode]');
					$zipCodeDisplay.parent().addClass("uk-hidden");
					$('input[name="address.addressLine1"]').attr("readonly",false).css('background','');
					$('input[name="address.addressLine2"]').val('');
					$('input[name="address.postalCode"]').val('');
					$('result-wrap li').remove();
					$('input[name="address.addressLine1"]').select();
					$('input[name="address.addressLine1"]').focus();
					action();
				});

				$btn.on('click', function(e){
					e.preventDefault();
					action();
				});

				// result list click event
				$resultWrap.on('click', '.list a', function(e){
					e.preventDefault();

					validateIS = true;
					//$input.setValue($(this).text());
					_self.fireEvent('resultSelect', _self, [this]);

					/*if(!opt.hasOwnProperty('api')){
						$btn.trigger('click');
					}*/

					$resultWrap.removeClass('active');
				});

				searchTxt = $input.getValue();

				return this;
			},
			getValidateChk:function(){
				if(opt.required === 'false' || setting.isModify === 'true'){
					return true;
				}else if(opt.required === 'true'){
					return validateIS;
				}
			},
			setErrorLabel:function(message){
				$input.setErrorLabel(message||opt.errMsg);
			},
			getInputComponent:function(){
				return $input;
			},
			setResultAppend:function(appendContainer, template, data){
				if(appendContainer === 'this'){
					$resultWrap.append(Handlebars.compile($(template).html())(data));
				}else{
					$(appendContainer).append(Handlebars.compile($(template).html())(data));
				}

			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_searchfield'] = {
		constructor:SearchField,
		attrName:'data-component-searchfield'
	};
})(Core);