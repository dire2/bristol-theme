(function(Core){
	'use strict';

	Core.register('module_map', function(sandbox){

		var storeList = null,
			$this,
			$infoViewContainer = null,
			map = null,
			markers = [],
			infoWindows = [],
			currentStoreIndex = 0;
		/*
			store-info-view 에 추가되는 dom object

			<h2 class="tit">명동</h2>
			<span class="address">서울 영등포구 여의도동</span>
			<span class="phonenum">070-0000-0000</span>
			<dl class="info">
				<dt class="key">운영시간</dt>
				<dd class="value">평일 10:00, 주말 11:00</dd>
				<dt class="key">매장정보</dt>
				<dd class="value">네이버 지도 API v3는 JavaScript 형태로 제공되는 NAVER 지도 플랫폼으로써</dd>
			</dl>
			<button class="close-btn"><i class="icon-delete_thin"></i></button>
		*/

		var Method = {
			moduleInit:function(){
				var args = arguments[0];
				$this = $(this);
				$infoViewContainer = $(this).find(args['data-module-map'].infoview);
				storeList = args['data-store-list'];

				//map 초기화
				var firstLatitude = (storeList[0]) ? storeList[0].latitude:37.3595953;
				var firstLongitude = (storeList[0]) ? storeList[0].longitude:127.1053971;

				map = new naver.maps.Map(args['data-module-map'].target, {
					center:new naver.maps.LatLng(firstLatitude, firstLongitude),
					zoom:5
				});

				var searchField = sandbox.getComponents('component_searchfield', {context:$this}, function(){
					this.addEvent('beforeSubmit', function(){
						var val = arguments[0];
						$('input[name=_find]').val(val);
					});
				});


				$(this).find('a').click(function(e){
					e.preventDefault();
					Method.mapEvent($(this).parent().index());

					$this.find('.search-result').removeAttr('style');
					if($('body').attr('data-device') !== 'pc') $this.find('.search-result').css('display', 'none');
				});

				$(this).on('click', '.close-btn', function(e){
					Method.mapEvent(currentStoreIndex);
				});

				Method.mapInit();
			},
			getStoreList:function(id){
				for(var key in storeList){
					if(storeList[key].id == id){
						return {'info':storeList[key], 'index':key}
					}
				}
			},
			showInfoDetail:function(data){
				var template = '<h2 class="tit">{{name}}</h2><span class="address">{{address1}}</span><span class="address">{{address2}}</span><span class="phonenum">{{phone}}</span><dl class="info">{{additionalAttributes}}</dl><button class="close-btn"><i class="icon-delete_thin"></i></button>';
				var subTemplate = '<dt class="key">{{key}}</dt><dd class="value">{{value}}</dd>';
				var rtnTxt = '';

				var appendTxt = template.replace(/{{\/*\w*}}/g, function(pattern){
					if(pattern == '{{additionalAttributes}}'){
						for(var key in data['additionalAttributes']){
							rtnTxt += subTemplate.replace(/{{\/*\w*}}/g, function(pattern){
								switch(pattern){
									case '{{key}}' :
										return key;
										break;
									case '{{value}}' :
										return data['additionalAttributes'][key];
										break;
								}
							});
						}

						return rtnTxt;
					}else{
						return (data[pattern.replace(/{{|}}/g, '')] == null) ? '' : data[pattern.replace(/{{|}}/g, '')];
					}
				});

				$infoViewContainer.empty().append(appendTxt);
				$infoViewContainer.stop().animate({'left':0}, 300);
			},
			mapInit:function(){

				//store 위도, 경도 값으로 지도 마커 찍어내기
				for (var i=0; i<storeList.length; i++) {
					var position = new naver.maps.LatLng(storeList[i].latitude, storeList[i].longitude);
					var imgDomain = _GLOBAL.SITE.IMAGE_DOMAIN;
					var marker = new naver.maps.Marker({
						map:map,
						position:position,
						title:storeList[i].name,
						icon:{
							url: imgDomain+'/cmsstatic/sp_pins_spot_v3.png',
							size: new naver.maps.Size(24, 37),
							anchor: new naver.maps.Point(12, 37),
							origin: new naver.maps.Point(0,0)
						},
						zIndex: 100
					});

					var infoWindow = new naver.maps.InfoWindow({
						content: '<div style="width:150px;text-align:center;padding:10px;">'+ storeList[i].name +'</div>'
					});

					markers.push(marker);
					infoWindows.push(infoWindow);
				}

				for (var i=0, ii=markers.length; i<ii; i++) {
					naver.maps.Event.addListener(markers[i], 'click', getClickHandler(i));
				}

				function getClickHandler(seq){
					return function(){
						Method.mapEvent(seq);
					}
				}
			},
			mapEvent:function(seq){
				var marker = markers[seq], infoWindow = infoWindows[seq];

				if (infoWindow.getMap()) {
					infoWindow.close();
					$infoViewContainer.stop().animate({'left':$infoViewContainer.outerWidth(true)}, 300, function(){
						$infoViewContainer.removeAttr('style');
						$this.find('.search-result').removeAttr('style');
					});

				} else {
					infoWindow.open(map, marker);

					var objStoreInfo = Method.getStoreList($(this).attr('data-store-id'));
					Method.showInfoDetail(storeList[seq]);
					currentStoreIndex = seq;

					if($('body').attr('data-device') !== 'pc') $this.find('.search-result').css('display','none');

					//선택한 store 좌표로 이동
					map.setCenter(new naver.maps.LatLng(storeList[seq].latitude, storeList[seq].longitude));
					map.setZoom(8);
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-map]',
					attrName:['data-module-map', 'data-store-list'],
					moduleName:'module_map',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);