(function(Core){
	'use strict';
	Core.register('module_wishlist', function(sandbox){

		var $this, modal;
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var miniCartModule = sandbox.getModule('module_minicart');
				modal = UIkit.modal('#common-modal', {center:true});

				$this.on('click', '.wish-delete_btn', function(e){
					e.preventDefault();
					var $that = $(this);
					UIkit.modal.confirm('삭제 하시겠습니까?', function(){
						location.href = $that.attr('href');
					});
				});

				$this.find('.btn-link').each(function(i){
					var target = $(this).attr('data-target');
					var url = $(this).attr('data-href');

					$(this).click(function(e){
						e.preventDefault();

						Core.Utils.ajax(url, 'GET', {quickview:true}, function(data){
							var domObject = $(data.responseText).find('#quickview-wrap');
							$(target).find('.contents').empty().append(domObject[0].outerHTML);
							$(target).addClass('quickview');
							Core.moduleEventInjection(domObject[0].outerHTML);
							modal.show();
						});

						/*sandbox.utils.ajax(url, 'POST', data, function(data){
							var jsonData = sandbox.rtnJson(data.responseText);
							if(jsonData.hasOwnProperty('error')){
								UIkit.notify(jsonData.error, {timeout:3000,pos:'top-center',status:'warning'});
							}else{
								//UIkit.notify('쇼핑백에 상품이 담겼습니다.', {timeout:3000,pos:'top-center',status:'success'});
								miniCartModule.update();
							}
						});*/
					});
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-wishlist]',
					attrName:'data-module-wishlist',
					moduleName:'module_wishlist',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);