(function(Core){
	Core.register('module_gnb', function(sandbox){

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var $oneDepth = $('.onedepth-list');
				var $twoDepth = $('.header-submenu-wrap');
				var $header = $('header');
				var $fixedWrapper = $this.closest('.fixed-wrapper');
				var $logoWarapper = $('.header-logo');
				var $logo = $('.header-item');
				var args = arguments[0];
				var setTime;
				var isOver = true;
				var positionFlag = 'static';
				var offsetTop = $('.header-gnb').offset().top;

				$oneDepth.on({
					'mouseenter.lnb':function(){
						$(this).find('a').addClass('active').parent().siblings().find('a').addClass('active').removeClass('active');
						if(isOver){
							$(this).find('.header-submenu-wrap').css({'display':'block', 'opacity':0}).stop().animate({opacity:1, marginTop:0}, 200);
						}else{
							$(this).find('.header-submenu-wrap').css({'display':'block', 'marginTop':'0'});
						}

						$(this).siblings().find('.header-submenu-wrap').removeAttr('style');
						clearTimeout(setTime);
						isOver = false;
					},
					'mouseleave.lnb':function(){
						$(this).find('a').removeClass('active');
						$(this).find('.header-submenu-wrap').removeAttr('style');
					},
					'click.lnb':function(e){
						var href = $(this).attr("href");
						if( href == "#" || href == "javascript:;" ){
							e.preventDefault();
							$(this).find('>').addClass('active');
						}
					}
				});

				$twoDepth.on('mouseleave', function(){
					var _self = this;
					setTime = setTimeout(function(){
						$(_self).closest('.onedepth-list').find('a').removeClass('active');
						$(_self).removeAttr('style');
						isOver = true;
					}, 500);
				});


				/* search */

				$('.gnb-search-field a').click(function(e){
					e.preventDefault();
					$('.search-panel').css('display', 'block');
					if(!$('.gnb-search').hasClass('active')){
						$('.search-field').find('input[type=search]').focus();
					}
				});

				$('.btn-search-close').click(function(e){
					e.preventDefault();
					$('.search-panel').removeAttr('style');
				});

				sandbox.scrollController(window, document, function(per){
					var offsetPer = offsetTop / ($(document).height() - $(window).height()) * 100;

					if(per > offsetPer && positionFlag !== 'fixed'){
						positionFlag = 'fixed';
						$fixedWrapper.css({'position':'fixed', 'width':'100%', 'marginTop':-offsetTop});
						$header.height($fixedWrapper.height());
						if($('body').attr('data-device') === 'pc'){
							$logoWarapper.stop().animate({marginTop:-37}, 200);
							$logo.stop().animate({maxWidth:200}, 200);
						}else{
							$logo.removeAttr('style');
						}
					}else if(per <= offsetPer && positionFlag !== 'static'){
						positionFlag = 'static';
						$fixedWrapper.removeAttr('style');
						$header.removeAttr('style');
						if($('body').attr('data-device') === 'pc'){
							$logoWarapper.stop().animate({marginTop:0}, 200);
							$logo.stop().animate({maxWidth:265}, 200);
						}else{
							$logo.removeAttr('style');
						}
					}
				}, 'gnb');
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-gnb]',
					attrName:'data-module-gnb',
					moduleName:'module_gnb',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
