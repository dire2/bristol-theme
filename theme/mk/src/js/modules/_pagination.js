(function(Core){
	Core.register('module_pagination', function(sandbox){
		var $this, args, currentPage, totalPageNum, totalPageCount, lineSize, pageSize, isHistoryBack = false, endPoint;

		var setSessionPaging = function(){
			sessionStorage.setItem('categoryPagingType', args.type);
			sessionStorage.setItem('categoryCurrentPage', currentPage + 1);
		}

		var Method = {
			moduleInit:function(){
				var sessionCurrentPage = sessionStorage.getItem('categoryCurrentPage');
				var sessionLineSize = sessionStorage.getItem('categoryLineSize');
				endPoint = Core.getComponents('component_endpoint');

				$this = $(this);
				args = arguments[0];
				currentPage = (sessionCurrentPage) ? sessionCurrentPage : args.currentPage;
				pageSize = Number(args.pageSize);
				totalPageNum = Math.ceil(args.totalCount / pageSize);
				lineSize = (sessionLineSize !== null) ? sessionLineSize : args.lineSize;

				switch(args.type){
					case 'more' :
						Method.typeMore();
						break;
					case 'scroll' :
						Method.typeScroll();
						break;
				}
			},
			getPaging:function(){
				return (args.totalCount > pageSize * currentPage && totalPageNum > currentPage) ? currentPage++ : null;
			},
			typeMore:function(){
				if(currentPage >= totalPageNum){
					$this.find('button, a').remove();
					return;
				}

				$this.find('button, a').click(function(e){
					e.preventDefault();

					var _self = this;
					if(Method.getPaging()){
						sandbox.utils.ajax(args.api, 'GET', {'page':currentPage, 'pageSize':pageSize, 'lineSize':lineSize}, function(data){
							endPoint.call('loadMoreProducts', {page : currentPage, pageSize: pageSize })
							$(args.target).append($(data.responseText).find(args.target)[0].innerHTML);
							sandbox.moduleEventInjection($(data.responseText).find(args.target)[0].innerHTML);

							if(currentPage >= totalPageNum){
								$(_self).off('click');
								$(_self).remove();
							}

							setSessionPaging();
						});
					}
				});
			},
			typeScroll:function(){
				if(currentPage >= totalPageNum) return;
				var _self = this;
				var isFirst = true;
				var isLoaded = true;
				var prevScrollTop = 0;
				var contentsHeightPer = 0;
				var scrollController = sandbox.scrollController(window, document, function(percent){
					contentsHeightPer = this.getScrollTop($(args.target).offset().top + $(args.target).height());

					if(percent > contentsHeightPer && isLoaded && !isHistoryBack && this.getScrollPer() < percent && !isFirst){
						isLoaded = false;
						if(Method.getPaging()){
							sandbox.utils.ajax(args.api, 'GET', {'page':currentPage, 'pageSize':pageSize, 'lineSize':lineSize}, function(data){
								endPoint.call('loadMoreProducts', {page : currentPage, pageSize: pageSize })
								$(args.target).append($(data.responseText).find(args.target)[0].innerHTML);
								sandbox.moduleEventInjection($(data.responseText).find(args.target)[0].innerHTML);

								if(currentPage >= totalPageNum){
									scrollController.destroy();
								}else{
									isLoaded = true;
									setSessionPaging();
								}
							});
						}
					}

					//새로고침, 히스토리백을 했을경우 돔오브젝트가 생성되지 못한 상황에서 스크롤의 위치가 최 하단으로 이동 하기 때문에
					//처음 로드 시점에서는 무조건 scroll 이벤트를 막는다.
					isFirst = false;

				}, 'pagination');
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-pagination]',
					attrName:'data-module-pagination',
					moduleName:'module_pagination',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			setLineSize:function(size){
				lineSize = size;
				sessionStorage.setItem('categoryLineSize', lineSize);
			},
			getPagingType:function(){
				return args.type;
			},
			destroy:function(){
				if(args.type === 'scroll' && args.scrollContainer !== 'document') scrollArea.destroy();
			}
		}
	});
})(Core);