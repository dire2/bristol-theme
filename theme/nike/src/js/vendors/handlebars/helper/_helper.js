Handlebars.registerHelper('isHiddenInfo', function(key, option) {	
	if((key !== 'storeType') && (key !== '지번')){
		return option.fn(this);
	}
});

Handlebars.registerHelper('isStoreType', function(key, option) {	
	if((key === 'storeType')){
		return option.fn(this);
	}
});


Handlebars.registerHelper('isNRC', function(key, option) {
	if((key === 'storeType') ){
		if(this && this.includes('nrc')){
			return option.fn(this);
		}
	}
});

Handlebars.registerHelper('isNTC', function(key, option) {
	if((key === 'storeType') ){
		if(this && this.includes('ntc')){
			return option.fn(this);
		}
	}
});

Handlebars.registerHelper('isReservation', function(key, option) {
	if((key === 'storeType') ){
		if(this && this.includes('reservation')){
			return option.fn(this);
		}
	}
});

//handlebars helper
Handlebars.registerHelper('addClass', function(index, option){
   if(index == 0){
	  return 'active';
   }else{
	  return ''
   }
});
