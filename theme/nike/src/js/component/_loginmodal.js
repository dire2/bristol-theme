(function(Core){
	var LoginModal = function(){
		'use strict';

		var $this, args, endPoint;
		var setting = {
			selector:'[data-component-loginmodal]',
			activeClass:'active'
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				// var _self = this;
				args = arguments[0];
				$this = $(setting.selector);
				//endPoint = Core.getComponents('component_endpoint');

				/* login */
				$this.click(function(e){
					e.preventDefault();

					// var _self = $(this);
					Core.getModule('module_header').setModalHide(true).setLogin(function(data){
						// console.log('callback');
						location.reload();
					});
				});

				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_loginmodal'] = {
		constructor:LoginModal,
		reInit:true,
		attrName:'data-component-loginmodal'
	}
})(Core);
