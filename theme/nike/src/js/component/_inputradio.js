(function(Core){
	var InputRadio = function(){
		'use strict';
		var setting = {
			selector:'[data-component-radio]',
			attrName:'data-component-radio',
			container:'.input-radio',
			label:'label',
			radio:'input[type=radio]',
			unlock:false
		}

		var rtnOption = function(container, key, data){
			data.forEach(function(data, i){
				if(data.inventoryType != 'UNAVAILABLE'){
					if(data.inventoryType === 'ALWAYS_AVAILABLE' || null){
						enableItem(container, key, data);
					}else if(data.inventoryType === 'CHECK_QUANTITY'){
						if(opt && opt.uiType === 'pdp'){
							if(data.quantity === 0 && opt.quantityOption === 'restock'){
								enableItem(container, key, data);
							} else if(opt.quantityOption !== 'restock' &&(data.quantity > 0 || data.quantity == null)){
								enableItem(container, key, data);
							}
						}
					}
				}
			});
		}

		var $this, $label, $radio, $container, eventID, firstInit = false, opt, isValidate = false;

		function enableItem(container, key, data){
			container.find(setting.radio).each(function(i){
				if($(this).val() == data[key]){
					$(this).removeAttr('disabled').parent().removeAttr('disabled').removeClass('disabled');
					if(opt && opt.uiType === 'pdp'){
						$(this).parent().find(setting.label).removeClass('sd-out');
					}
				}
			});
		}


		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				//console.log(arguments);
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				opt = arguments[0]||{};

				$this = $(setting.selector);
				$container = $this.find(setting.container);
				$label = $container.find(setting.label);
				$radio = $container.find(setting.radio);
				
				$label.on('click', function(e){		
					e.preventDefault();
					console.log('label on click');					
					_self.fireEvent('click', this, [$(this).siblings()]);
					if(!$(this).parent().hasClass('disabled')){
						if($(this).siblings().prop('checked') && setting.unlock){
							$(this).siblings().prop('checked', false);
							$(this).parent().removeClass('checked');
							return;
						}

						$(this).siblings().trigger('click');
					}
				});

				$container.on('click', function(){		
					//PDP SIZE (optionGridType)
					if(opt && opt.uiType === 'pdp'){
					    _self.fireEvent('click', this, [$(this).siblings()]);						
						if(!$(this).attr('disabled')){
							$(this).parent().find(setting.label, setting.radio).each(function(){
								//기존에 선택된 사이즈 해지
								$(this).removeClass('selected');
								$(this).prop('checked', false);
							});
							$(this).find(setting.label).addClass('selected');
							$(this).find(setting.radio).prop('checked', true);
							$(this).find(setting.radio).trigger('change');
						}
					}
				})

				$radio.on('change', function(){			
					if($(this).prop('checked')){						
						isValidate = true;
						$(this).parent().addClass('checked').siblings().removeClass('checked');
						$(this).siblings().attr('checked');

						_self.fireEvent('change', this, [$(this).attr('data-value'), $(this).val(), $(this).attr('data-id'), $(this).attr('data-friendly-name')]);
					}
				});

				// 기본 선택값 처리
				$radio.each(function(i){
					var $this = $(this);
					if($this.prop('checked')){
						setTimeout(function(){
							$this.trigger('change');
							_self.fireEvent('init', $this);
						});
					}
				});

				return this;
			},
			receiveToData:function(option, skuData){
				isValidate = false;
				rtnOption($container, option.type, skuData);
			},
			reInit:function(){
				$container.each(function(i){
					$(this).removeClass('checked').find('input[type=radio]').removeAttr('checked');
				});
			},
			disabled:function(){
				$container.each(function(i){
					$(this).removeClass('checked').addClass('disabled').find('input[type=radio]').removeAttr('checked').attr('disabled', 'disabled');
				});
			},
			trigger:function(value, valueId){
				$radio.each(function(){
					if($(this).val() == valueId){
						$(this).trigger('click');
						return false;
					}
				});
			},
			getValidateChk:function(){
				if(opt.required){
					return isValidate;
				}else{
					return true;
				}
			},
			getThis:function(){
				return $this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_radio'] = {
		constructor:InputRadio,
		attrName:'data-component-radio',
		reInit:true
	}
})(Core);
