(function(Core){
    var Gallery = function(){

       var $this, $galleryWrap, $zoomWrap, args, arrGalleryList=[],arrColorList=[], sliderComponent,sliderComponent2, endPoint;
       var setting = {
          selector:'[data-component-gallery]',
          galleryWrapper:'#product-gallery',
          zoomWrapper:'.pdp-gallery-fullview',
          zoomAppender:'.gallery-images'
       }

       var Closure = function(){}
       Closure.prototype.setting = function(){
          var opt = Array.prototype.slice.call(arguments).pop();
          $.extend(setting, opt);
          return this;
       }

       Closure.prototype.init = function(){
          var _self = this;

          args = arguments[0];
		  $this = $(setting.selector);
          $galleryWrap = $this.find(setting.galleryWrapper);
          $zoomWrap = $this.find(setting.zoomWrapper);
          endPoint = Core.getComponents('component_endpoint');
          
          //pc일때
          var arrList = [];
          $galleryWrap.find('.image-list').each(function(i){       
             var data = Core.Utils.strToJson($(this).attr('data-thumb'), true);
             var imgUrl = $(this).find('img').attr('src').replace(/\?[a-z]+/g, '');
             var pushIS = true;
             data.thumbUrl = imgUrl;

             /* 중복 이미지 처리 */
             for(var i=0; i < arrList.length; i++){
                if(arrList[i].thumbSort === data.thumbSort && arrList[i].thumbUrl === data.thumbUrl){
                   pushIS = false;
                   return;
                }
             }

             if(pushIS){
                arrList.push(data);
                arrGalleryList.push(data);
             }
		  });
		
        //모바일일때
		var arrList2 = [];
        $("#product-option_color").find("a").each(function(){ 
             var data = {};
             var linkUrl = $(this).attr('href');
             var imgUrl = $(this).find('img').attr('src').replace(/\?[a-z]+/g, '');
             var pushIS = true;
             data.thumbUrl = imgUrl;
             data.linkUrl = linkUrl;

             if(pushIS){
                arrList2.push(data);
                arrColorList.push(data);
             }
          });

          $galleryWrap.click(function(){
           $('html').addClass('uk-modal-page');
           $('body').css('paddingRight', 15);
           $zoomWrap.addClass('show');
        });

          $zoomWrap.click(function(){
            //  if($('#quickview-wrap').length <= 0){
                $('html').removeClass('uk-modal-page');
                $('body').removeAttr('style');
            //  }
             $(this).removeClass('show');
          });
        
        //진입시 바로 모바일 pc,사이즈 체크
		var widthMatch = matchMedia("all and (max-width: 767px)");
		if (Core.Utils.mobileChk || widthMatch.matches) {
			this.setThumb(args.sort);
			$("#product-option_color").hide();
		} else {
			this.setZoom(args.sort);
			$("#product-option_color").show();
		}

		//리사이징될때 실행 (오동작 체크 필요)
		$(window).resize(function() {
			if (Core.Utils.mobileChk || widthMatch.matches) {
			  Closure.prototype.setThumb(args.sort);
			  $("#product-option_color").hide();
			} else {
			  Closure.prototype.setZoom(args.sort);
			  $("#product-option_color").show();
			}
		});

          return this;
       }
       
       //pc인경우
       Closure.prototype.setZoom = function(sort){
          var _self = this;
          var appendTxt = '';
          var count = 0;
          var sortType = sort || args.sort;

          //console.log('arrGalleryList:' , arrGalleryList);  
          var arrGalleryData = arrGalleryList.filter(function(item, index, array){         
             if(item.thumbSort === sortType || item.thumbSort === 'null'){
                count++;
                return item;
             }
          });
		
          var thumbTemplate;
          if(arrGalleryData.length > 3){
            thumbTemplate = Handlebars.compile($("#product-gallery-nike").html())(arrGalleryData);
          } else {
            thumbTemplate = Handlebars.compile($("#product-gallery-nike-large").html())(arrGalleryData);
          }
          
		  var zoomTemplate = Handlebars.compile($('#product-gallery-zoom').html())(arrGalleryData);

		  $galleryWrap.empty().append(thumbTemplate);
		  $zoomWrap.find(setting.zoomAppender).empty().append(zoomTemplate);
		  $("#color-swipe").empty();
	   }
	 
      //모바일인경우
	  Closure.prototype.setThumb = function(sort){
		var _self = this;
		var appendTxt = '';
		var count = 0;
		var sortType = sort || args.sort;
		var arrThumbData = arrGalleryList.filter(function(item, index, array){
			if(item.thumbSort === sortType || item.thumbSort === 'null'){
				count++;
				return item;
			}
		});

		var colorgalleryTemplate = Handlebars.compile($("#product-gallery-color-swipe").html())(arrColorList);
		var galleryTemplate = Handlebars.compile($("#product-gallery-swipe").html())(arrThumbData);
		var zoomTemplate = Handlebars.compile($('#product-gallery-zoom').html())(arrThumbData);

		$("#color-swipe").empty().append(colorgalleryTemplate);
		$galleryWrap.empty().append(galleryTemplate);
		$zoomWrap.find(setting.zoomAppender).empty().append(zoomTemplate);
		
		//사이즈 조정
	    $galleryWrap.find(".swipe-wrapper").css('width','100%');
	    $galleryWrap.find(".swipe-wrapper").find('li').css('min-width','auto');
	    $("#color-swipe").find(".swipe-wrapper").find('li').css('min-width','auto');

		if(sliderComponent) sliderComponent.destroySlider();
		sliderComponent = Core.getComponents('component_slider', {context:$this, selector:'#product-gallery>div>.swipe-container'}, function(){
			this.addEvent('slideAfter', function($slideElement, oldIndex, newIndex){
				$galleryWrap.find('li').eq(newIndex).addClass('active').siblings().removeClass('active');
			});
		});
    
		if(sliderComponent2) sliderComponent2.destroySlider();
		sliderComponent2 = Core.getComponents('component_slider', {context:$this, selector:'#color-swipe>div>.swipe-container'}, function(){
			this.addEvent('slideAfter', function($slideElement, oldIndex, newIndex){
				$galleryWrap.find('li').eq(newIndex).addClass('active').siblings().removeClass('active');
			});
		});
	}

       Core.Observer.applyObserver(Closure);
       return new Closure();
    }

    Core.Components['component_gallery'] = {
       constructor:Gallery,
       attrName:'data-component-gallery'
    }
 })(Core);
