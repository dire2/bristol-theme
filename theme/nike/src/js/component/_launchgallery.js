(function(Core){
	Core.register('module_launchgallery', function(sandbox){
        var $that;
        var zoomWrapper = $('.pdp-gallery-fullview');
		var Method = {
			moduleInit:function(){
				$this = $(this);
                //상품 이미지 클릭
                $('.lc-prd-images li').click(function() {
                    var index = $(this).index();
                    console.log('index:', index);
                    $('html').addClass('uk-modal-page');
                    $('body').css('paddingRight', 15);
                    zoomWrapper.addClass('show');
                });
                
                //줌 화면의 X 버튼 
                $('.button-wrapper').click(function(){
                    $('html').removeClass('uk-modal-page');
                    $('body').removeAttr('style');
                    zoomWrapper.removeClass('show');
                 });
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-launchgallery]',
					attrName:'data-module-launchgallery',
					moduleName:'module_launchgallery',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
