(function(Core){
	'use strict';

	Core.register('module_map', function(sandbox){
		var storeList = null,
			$this,
			$infoViewContainer = null,
			map = null,
			markers = [],
			infoWindows = [],
			filterQuery = [],
			currentStoreIndex = 0,
			searchQueryString = '';
		/*
			store-info-view 에 추가되는 dom object

			<h2 class="tit">명동</h2>
			<span class="address">서울 영등포구 여의도동</span>
			<span class="phonenum">070-0000-0000</span>
			<dl class="info">
				<dt class="key">운영시간</dt>
				<dd class="value">평일 10:00, 주말 11:00</dd>
				<dt class="key">매장정보</dt>
				<dd class="value">네이버 지도 API v3는 JavaScript 형태로 제공되는 NAVER 지도 플랫폼으로써</dd>
			</dl>
			<button class="close-btn"><i class="icon-delete_thin"></i></button>
		*/

		/**************************

			매장검색
			매장이름, 지역검색 : _find=지하철&_search=name&_condition=like
			필터 : _find={{매장타입}}&_search=storeType&_condition=or||and

			admin에서 재고위치/대리점의 추가속성에 storeType, icon은 각각, 검색필터와 마커의 아이콘의 클래스를 입력하는 속성이다.

			membership: 나이키 멤버십 및 티켓을 사용 할 수 있는 매장
			nrc: nike running club
			ntc: nike traingin club
			reservation: 매장 상품 예약 서비스 제공 하는 매장

		***************************/

		var makeMarkerIcon = function(storeData){
			var icon =  {
				size: new naver.maps.Size(20, 31),
				origin: new naver.maps.Point(0, 0),
				anchor: new naver.maps.Point(10, 16)
			}

			if(storeData.additionalAttributes && storeData.additionalAttributes.icon && storeData.additionalAttributes.icon !== ''){
				icon.content = '<i class="icon_map_marker '+storeData.additionalAttributes.icon+'"></i>';
			} else if(undefined !== storeData.additionalAttributes.storeType && storeData.additionalAttributes.storeType.includes('membership')){
				icon.url ='/assets/images/g_ico_mapFlag01.png';
			} else{
				icon.url ='/assets/images/g_ico_mapFlag02.png';
			}
			return icon;
		}

		var submitSearchMap = function(){
			//_find=서울&_search=name&_condition=like&_find=손목시계,지하철&_search=storeType&_condition=or

			var filterParams = location.pathname + '?';
			if(searchQueryString !== '') filterParams += searchQueryString + '&';
			if(filterQuery.length > 0){
				filterParams += '_find='+filterQuery.join(',')+'&_search=storeType&_condition=or';
			}
			location.href = filterParams.replace(/[?|&]$/, '');
		}

		var Method = {
			moduleInit:function(){
				var args = arguments[0];
				$this = $(this);
				$infoViewContainer = $(this).find(args['data-module-map'].infoview);
				storeList = args['data-store-list'];

				//map 초기화
				var firstLatitude = (storeList[0]) ? storeList[0].latitude:37.5024836;
				var firstLongitude = (storeList[0]) ? storeList[0].longitude:127.0260331;
				var currentParams = sandbox.utils.getQueryParams(location.href);
				var arrCurrentFilters = [];
				if(currentParams.hasOwnProperty('_find') && currentParams['_find'] instanceof Array){
					arrCurrentFilters = currentParams['_find'][1].split(',');
				}else{
					currentParams = null;
					arrCurrentFilters = null;
				}

				map = new naver.maps.Map(args['data-module-map'].target, {
					center:new naver.maps.LatLng(firstLatitude, firstLongitude),
					zoom:9
				});

				var searchField = sandbox.getComponents('component_searchfield', {context:$this}, function(){
					/*this.addEvent('beforeSubmit', function(){
						var val = arguments[0];
						$('input[name=_find]').val(val);
					});*/
					this.addEvent('searchEmpty', function($form){
						searchQueryString = $form.serialize();
						submitSearchMap();
					});

					this.addEvent('searchKeyword', function($form, value){
						searchQueryString = $form.serialize();
						submitSearchMap();
					});
				});

				var searchCheckBox = sandbox.getComponents('component_checkbox', {context:$this}, function(i, dom){
					this.addEvent('change', function(val){
						var index = filterQuery.indexOf(val);
						if(index === -1){
							// filterQuery.push(val);
							//value가 '매장 상품 예약 서비스'로 들어와서 search안됨.name ='reservation'을 넣어 준다.
							filterQuery.push(this.name);
						}else{
							filterQuery.splice(index, 1);
						}
					});

					if(arrCurrentFilters !== null && arrCurrentFilters.indexOf(encodeURI(this.getThis().val())) > -1){
						this.getThis().prop('checked', true);
						this.getThis().trigger('change');
					}
				});

				$(this).find('.filter-btn').click(function(e){
					e.preventDefault();

					/*if(filterQuery.length > 0){
						var filterQueryParams = sandbox.utils.getQueryParams($(this).closest('form').serialize());
						filterQueryString = '_find='+filterQuery.join(',');
						if(filterQueryParams._search) filterQueryString += '&_search='+filterQueryParams._search;
						if(filterQueryParams._condition) filterQueryString += '&_condition='+filterQueryParams._condition;
					}else{
						filterQueryString = '';
					}*/

					searchField.externalAction();
				});

				$(this).find('.search-result li #search-list-a').click(function(e){
					e.preventDefault();
					Method.mapEvent($(this).parent().index());

					$this.find('.search-result').removeAttr('style');
					if($('body').attr('data-device') !== 'pc') {
						$this.find('.search-result').css('display', 'none');
					}
				});

				$(this).on('click', '.close-btn', function(e){
					Method.mapEvent(currentStoreIndex);
				});

				//service filter area show and hide
				$(this).on('click', '#service-filter-btn', function(e){
					var target = '#service-filter-area';
					if($(target).is(":visible")){
						$(target).hide();
						$(target).addClass('close');
					} else {
						$(target).show();
						$(target).removeClass('close');
					}
				});
				
				
				Method.updateCheckAll();
				
				// 매장찾기 전체 체크시
				$(this).find('input#check-all-store[type="checkbox"]').on('change', Method.changeAllCheck);
				// 아이템 체크박스 선택시
				$(this).find('input#check-item-store[type="checkbox"]').on("change", Method.changeItemCheck);


				Method.mapInit();
			},
			// 매장찾기 전체 체크 처리
			changeAllCheck:function(e){
				e.preventDefault();			
				var isCheck = $(this).prop('checked');				
				$('input#check-item-store[type="checkbox"]').each( function(){
					if(isCheck == true && !$(this).prop('checked')){
						$(this).prop('checked', isCheck ).trigger('change');
					}
					if(isCheck == false && $(this).prop('checked')){
						$(this).prop('checked', isCheck ).trigger('change');
					}					
				});			
			},
			// 아이템 체크박스 선택시
			changeItemCheck:function(e){
				var isCheck = $(this).prop('checked');
				if( isCheck ){
					$(this).parent().addClass('checked');
				}else{
					$(this).parent().removeClass('checked');
				}
				Method.updateCheckAll();
			},			
			// 아이템 체크박스 변경시 전체 선택 체크박스 상태처리
			updateCheckAll:function(){
				
				if($('input#check-item-store[type="checkbox"]').length == $('input#check-item-store[type="checkbox"]:checked').length ){
					$('input#check-all-store[type="checkbox"]').prop( 'checked', true);
				}else{
					$('input#check-all-store[type="checkbox"]').prop( 'checked', false);
				}
			},
			getStoreList:function(id){
				for(var key in storeList){
					if(storeList[key].id == id){
						return {'info':storeList[key], 'index':key}
					}
				}
			},
			//Store 선택 시 상세 정보 만드는 스크립트 실행
			showInfoDetail:function(data){				
				var template = Handlebars.compile($('#map-store-info').html())(data);

				$infoViewContainer.empty().append(template);
				$infoViewContainer.stop().animate({'left':0}, 300);
			},
			mapInit:function(){

				//store 위도, 경도 값으로 지도 마커 찍어내기
				//store type에 따라 2개의 마커icon 필요함
				for (var i=0; i<storeList.length; i++) {
					var position = new naver.maps.LatLng(storeList[i].latitude, storeList[i].longitude);
					var marker = new naver.maps.Marker({
						map:map,
						position:position,
						title:storeList[i].name,
						icon:makeMarkerIcon(storeList[i]),
						zIndex:100
					});

					var infoWindow = new naver.maps.InfoWindow({
						// content: '<div style="width:150px;text-align:center;padding:10px;">'+ storeList[i].name +'</div>'
						content: '<div id="map_store_info_layer" style="width:320px;text-align:center;padding:20px 24px 20px 30px">'+Handlebars.compile($('#map-window-store-info').html())(storeList[i])+'</div>'
					});

					markers.push(marker);
					infoWindows.push(infoWindow);
				}

				for (var i=0, ii=markers.length; i<ii; i++) {
					naver.maps.Event.addListener(markers[i], 'click', getClickHandler(i));
				}

				function getClickHandler(seq){
					return function(){
						Method.mapEvent(seq);
					}
				}

				// function closeMapStoreInfo(){
				// 	/* 레이어 닫기 */
				// 	$("#global_store_layer").hide();
				// }
			},
			mapEvent:function(seq){
				var marker = markers[seq], infoWindow = infoWindows[seq];

				if (infoWindow.getMap()) {
					infoWindow.close();
					$infoViewContainer.stop().animate({'left':$infoViewContainer.outerWidth(true)}, 300, function(){
						$infoViewContainer.removeAttr('style');
						$this.find('.search-result').removeAttr('style');
					});

				} else {
					infoWindow.open(map, marker);

					var objStoreInfo = Method.getStoreList($(this).attr('data-store-id'));
					Method.showInfoDetail(storeList[seq]);
					currentStoreIndex = seq;

					if($('body').attr('data-device') !== 'pc') $this.find('.search-result').css('display','none');

					//선택한 store 좌표로 이동
					map.setCenter(new naver.maps.LatLng(storeList[seq].latitude, storeList[seq].longitude));
					map.setZoom(10);
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-map]',
					attrName:['data-module-map', 'data-store-list'],
					moduleName:'module_map',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
