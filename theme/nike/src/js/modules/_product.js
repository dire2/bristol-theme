(function(Core){
	Core.register('module_product', function(sandbox){
		var currentFirstOptValue = '';
		var currentQuantity = 1;
		var itemAttributes = '';
		var miniOptionIS = false;
		var objProductOption = {};
		var minOffsetTop = 30;
		var maxOffsetTop = 0;
		var args = null;
		var $this;
		var imgCurrentIndex;
		var productId = '';
		var skuId = '';
		var isQuickView = false;
		var isQuantity = true;
		var productOption;
		var quantity;
		var endPoint;
		var privateId;
		var $optionWrap;
		var $galleryWrap;
		var optionWrapOffsetTop;
		var previousScrollTop = 0;
		var $descriptionWrap;
		var longDescription;
		
		var quantityCheck = function(inventoryType, maxQty){
			var obj = {isQuantity:false, maxQty:0}
			if(inventoryType !== 'UNAVAILABLE'){
				if(inventoryType === 'CHECK_QUANTITY'){
					obj.isQuantity = (maxQty > 0) ? true : false;
					obj.maxQty = maxQty;
				}else if(inventoryType === 'ALWAYS_AVAILABLE'){
					obj.isQuantity = true;
					obj.maxQty = null;
				}
			}else{
				obj.isQuantity = false;
				obj.maxQty = 0;
			}

			return obj;
		}

		var defaultSkuSetup = function(productOptComponents){
			var skuData, quantityState;
			if(!productOptComponents) return;
			if(quantity){
				if(Array.isArray(productOptComponents)){
					$.each(productOptComponents, function(i){
						skuData = this.getDefaultSkuData()[0];
						quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
						quantity[i].setMaxQuantity(quantityState.maxQty);
						isQuantity = quantityState.isQuantity;
					});
				}else{
					skuData = productOptComponents.getDefaultSkuData()[0];
					quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
					quantity.setMaxQuantity(quantityState.maxQty);
					isQuantity = quantityState.isQuantity;
				}
			}
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				productId = args.productId;
				privateId = args.privateId;
				endPoint = Core.getComponents('component_endpoint');

				var $dim = $('[data-miniOptionDim]');
				var guideModal = UIkit.modal('#guide', {modal:false});
				var miniCartModule = sandbox.getModule('module_minicart');
				var gallery = sandbox.getComponents('component_gallery', {context:$this});
                var $infoHeightWrap = $('[data-info-height]');

				$galleryWrap = $this.find('.product-gallery-wrap');
				$optionWrap = $this.find('.product-option-container');
				optionWrapOffsetTop = $optionWrap.offset().top;

				quantity = sandbox.getComponents('component_quantity', {context:$(document)}, function(i){
					var INDEX = i;
					this.addEvent('change', function(qty){
						for(var i=0;i<quantity.length;i++){
							if(i !== INDEX){
								quantity[i].setQuantity(qty);
							}
						}
					});
				});

				productOption = sandbox.getComponents('component_product_option', {context:$(document)}, function(i){ //product Option select components
					var CURRENT_INDEX = i;
					var INDEX = 0;
					var _self = this;
					var currentOptValueId = '';
					var key = this.getProductId();
					if(!objProductOption.hasOwnProperty(key)){
						objProductOption[key] = [];
						INDEX = 0;
					}else{
						INDEX++;
					}
					objProductOption[key].push(this);

					this.addEvent('changeFirstOpt', function(firstOptName, optionName, productId, value, valueId, id){
						if(currentOptValueId != valueId){
							currentOptValueId = valueId;

							for(var i=0; i<objProductOption[productId].length; i++){
								if(i != INDEX){
									skuId = '';
									objProductOption[productId][i].setTrigger(optionName, value, valueId);
								}

								if(optionName !== 'COLOR'){
									objProductOption[productId][i].getValidateChk();
								}
							}

							if(optionName === 'COLOR'){
								gallery.setThumb(value);
							}
						}

						//console.log( "changeFirstOpt");
						/*
						if( _.isFunction(marketingUpdateProductInfo)){
							marketingUpdateProductInfo();
						}
						*/
					});

					this.addEvent('skuComplete', function(skuOpt){
						//args.isDefaultSku
						if(quantity){
							var quantityState = quantityCheck(skuOpt.inventoryType, skuOpt.maxQty);
							isQuantity = quantityState.isQuantity;
							skuId = skuOpt.id;

							if(args.isDefaultSku !== 'true'){
								if(Array.isArray(quantity)){
									quantity[CURRENT_INDEX].setQuantity(1);
									quantity[CURRENT_INDEX].setMaxQuantity(quantityState.maxQty);
								}else{
									quantity.setQuantity(1);
									quantity.setMaxQuantity(quantityState.maxQty);
								}
							}
						}

						//console.log( "skuComplete");
						/*
						if( _.isFunction(marketingUpdateProductInfo)){
							marketingUpdateProductInfo();
						}
						*/
					});
				});


				/* isDefaultSku - true  ( option이 없는 경우 )  */
				if(args.isDefaultSku === 'true') defaultSkuSetup(productOption);

				/* cart Update */
				$('[data-add-item]').each(function(i){
					var INDEX = i;
					$(this).find('.btn-link').click(function(e){
						e.preventDefault();

						var validateChk = (args.isDefaultSku === 'true') ? true : false;
						var qty = 0;

						if(args.isDefaultSku === 'false'){
							if(Array.isArray(productOption)){
								$.each(productOption, function(i){
									validateChk = this.getValidateChk('옵션을 선택해 주세요.');
								});
							}else{
								validateChk = productOption.getValidateChk('옵션을 선택해 주세요.');
							}
						}

						if(Array.isArray(quantity)){
							qty = quantity[INDEX].getQuantity();
						}else{
							qty = quantity.getQuantity();
						}

						if(validateChk && isQuantity && qty != 0){
							var $form = $(this).closest('form');
							var actionType = $(this).attr('action-type');
							var url = $(this).attr('href');
							var itemRequest = BLC.serializeObject($form);
							itemRequest['productId'] = productId;
							itemRequest['quantity'] = qty;


							/*****************************************************************
								유입 channel sessionStorage
								 - channel : 유입된 매체 식별 이름
								 - pid : 상품 식별 code ( productId, style Code, UPC.... )

								사이트 진입시 URL에 channel, pid 가 있을때 매출을 체크 한다.
								channel 만 있을경우에는 모든 상품을 channel 매출로 인지하고
								channel과 pid 둘다 있을경우 해당 상품만 channel 매출로 인지한다.
							*****************************************************************/

							if(sandbox.sessionHistory.getHistory('channel')){
								if(sandbox.sessionHistory.getHistory('pid')){
									if(sandbox.sessionHistory.getHistory('pid') === privateId){
										itemRequest['itemAttributes[channel]'] = sandbox.sessionHistory.getHistory('channel');
									}
								}else{
									itemRequest['itemAttributes[channel]'] = sandbox.sessionHistory.getHistory('channel');
								}
							}

							switch(actionType){
								case 'externalLink' :
									//외부링크
									window.location.href = url;
									break;
								default :
									BLC.ajax({
										url:url,
										type:"POST",
										dataType:"json",
										data:itemRequest
									}, function(data, extraData){
										if(data.error){
											UIkit.modal.alert(data.error);
										}else{
											/*
											if( _.isFunction( marketingAddCart )){
												marketingAddCart();
											}
											*/
											var cartData = $.extend( data, {productId : productId, quantity : qty, skuId : skuId });

											if(actionType === 'add'){
												endPoint.call('addToCart', cartData );
												miniCartModule.update();
											}else if(actionType === 'modify'){
												var url = Core.Utils.url.removeParamFromURL( Core.Utils.url.getCurrentUrl(), $(this).attr('name') );
												Core.Loading.show();
												endPoint.call( 'cartAddQuantity', cartData );
												_.delay(function(){
													window.location.assign( url );
												}, 1000);

												//window.location.assign( url );
											}else if('redirect'){
												Core.Loading.show();
												endPoint.call( 'buyNow', cartData );
												_.delay(function(){
													window.location.assign( data.redirectUrl );
												}, 1000);
											}
										}
									});
									break;
							}
						}else if(!isQuantity || qty == 0){
							UIkit.notify(args.errMsg, {timeout:3000,pos:'top-center',status:'warning'});
						}

					});
				});


				//scrollController
				var scrollArea = sandbox.scrollController(window, document, function(percent){
					var maxOffsetTop = this.getScrollTop($('footer').offset().top);
					var maxHeight = this.setScrollPer(maxOffsetTop);
					if(percent < minOffsetTop && miniOptionIS){
						miniOptionIS = false;
						$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
						$('.mini-option-wrap').find('.info-wrap_product_n').removeClass('active');
						$dim.removeClass('active');
					}else if(percent >= minOffsetTop && percent <= maxOffsetTop && !miniOptionIS){
						miniOptionIS = true;
						$('.mini-option-wrap').stop().animate({bottom:0}, 200);
					}else if(percent > maxOffsetTop && miniOptionIS){
						miniOptionIS = false;
						$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
						$('.mini-option-wrap').find('.info-wrap_product_n').removeClass('active');
						$dim.removeClass('active');
					}
				}, 'miniOption');

				//PDP 상품 설명 영역 스크롤 : 갤러리 영역내 위치 고정
				var summaryScrollController = sandbox.scrollController(window, document, function(per, scrollTop){
					if(sandbox.reSize.getState() === 'pc'){
						if($galleryWrap.height() > $optionWrap.height() && $optionWrap.height() > $(window).height()){
							var galleryPosY = $galleryWrap.height() + optionWrapOffsetTop - $(window).height();
							var optionPosY = $optionWrap.height() + optionWrapOffsetTop -$(window).height();
							var scrollupFixPosY = $galleryWrap.height() - $optionWrap.height()/*+ optionWrapOffsetTop*/;

							// console.log('scrollTop:%d, optionPosY:%d, galleryPosY:%d, optionWrapOffsetTop:%d, scrollupFixPosY:%d, option top:%d' , scrollTop, optionPosY, galleryPosY, optionWrapOffsetTop,  scrollupFixPosY, $optionWrap.offset().top);
							
							if(scrollTop > optionPosY && galleryPosY > scrollTop){
								//스크롤 시 옵션 정보를 갤러리 영역 내에 고정 
								// console.log('fix');
								if(scrollTop > previousScrollTop){
									//scroll down
									if(!$optionWrap.hasClass('fixed')){
										$optionWrap.removeClass('absolute', 'bottom').addClass('fixed bottom');
								    }
								} else if(scrollTop < previousScrollTop && scrollupFixPosY >= scrollTop ) {
									//scroll up
									if($optionWrap.hasClass('absolute')){
										$optionWrap.removeClass('absolute bottom').addClass('fixed');
									}
								}
							} else {
								//스크롤시 옵션 정보 움직임 
								// console.log('move');
								if(scrollTop > previousScrollTop){
									//scroll down
									if($optionWrap.hasClass('fixed')){
										$optionWrap.removeClass('fixed','bottom','top').addClass('absolute bottom');
									}
								} else if(scrollTop < previousScrollTop) {
									//scroll up
									if(0 === scrollTop & $optionWrap.hasClass('fixed')){
										//스크롤 다운으로 내려갔다가 다시 맨 위로 올라오는 경우 
										$optionWrap.removeClass('fixed bottom');
									} else if($optionWrap.hasClass('absolute') && scrollupFixPosY >= scrollTop){
										 //스크롤 다운으로 내려갔다가 슬슬 위로 올라오는 경우
										//  console.log('need to fix before reach top');
										 $optionWrap.removeClass('absolute bottom').addClass('fixed');

									} else if($optionWrap.hasClass('fixed bottom')){
										$optionWrap.removeClass('fixed bottom');
									}
								}	
							}
						} else {
							//아코디언 정보를 펼친 경우 갤러리 길이 보다 상품옵션이 더 길어 질수 있다
							if($optionWrap.hasClass('fixed', 'absolute')){
								$optionWrap.removeClass('fixed', 'absolute', 'bottom', 'top');
							}
						}
					} else {
						if($optionWrap.hasClass('fixed')){
							$optionWrap.removeClass('fixed top bottom');
						}
						if($optionWrap.hasClass('absolute')){
							$optionWrap.removeClass('absolute top bottom');
						}
					}
					//스크롤 업/다운 구분을 위해 이전 스크롤 위치 기억
					previousScrollTop = scrollTop;

				}, 'product');

				$('.minioptbtn').click(function(e){
					console.log('minioptbtn');
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product_n').addClass('active');
					$dim.addClass('active');
				});

				$('.mini-option-wrap').on('click', '.close-btn', function(e){
					console.log('mini-option-wrap');					
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product_n').removeClass('active');
					$dim.removeClass('active');
				});


				//guide option modal
				$this.find('.option-guide').on('click', function(e){
					e.preventDefault();
					guideModal.show();
				});


				$('.uk-quickview-close').click(function(e){
					guideModal.hide();
					isQuickView = true;
				});

				guideModal.off('.uk.modal.product').on({
					'hide.uk.modal.product':function(){
						if(isQuickView){
							setTimeout(function(){
								$('html').addClass('uk-modal-page');
								$('body').css('paddingRight',15);
							});
						}
					}
				});

				//PDP summary에서 상품 설명의 이미지 제거한 내용을 영역에 붙임.
				if($this.find('[data-long-description]').attr('data-long-description')){
					var html = $.parseHTML($this.find('[data-long-description]').attr('data-long-description'));
					$(html).find('div.imgArea').remove().find('script').remove();
					$this.find('#pdp-description-summary').empty().append(html);
				}

				//상품 정보 영역의 높이 줄임처리 (상품정보, 유의사항)
				$infoHeightWrap.each(function(e){
					// e.preventDefault();
					var argmts = Core.Utils.strToJson($(this).attr('data-info-height'), true) || {};
					var pdpInfoSubjectHeight=78;
					var readMoreHeight=65;
					var infoType = argmts.infoType;
					var outerHeight = parseInt(argmts.outerHeight);
					var shortenHeight =  outerHeight-readMoreHeight;
					var contentsHeight = shortenHeight - pdpInfoSubjectHeight;
					var $descriptionWrap;
					if(infoType === 'attention-guide'){
						$descriptionWrap = $(this);
					} else if(infoType === 'product-detail'){
						$descriptionWrap = $(this).closest('.pop-detail-content');
					}

					if(argmts && ($descriptionWrap.outerHeight() > outerHeight || $descriptionWrap.outerHeight() === 0)){
						if(infoType === 'attention-guide'){
							$descriptionWrap.outerHeight(shortenHeight);
							$descriptionWrap.find('.guide-area').height(contentsHeight).css({'overflow':'hidden'});
							$descriptionWrap.find('#read-more').show();
						} else if(infoType === 'product-detail'){
							if($descriptionWrap.find('.conArea').length > 0){
								$descriptionWrap.find('.conArea').height(shortenHeight).width('100%').css({'overflow':'hidden'});
							} else if($descriptionWrap.find('.sectionR').length  > 0){
								$descriptionWrap.find('.sectionR').height(shortenHeight).css({'overflow':'hidden'});
							}
						}
					}
				});

				//1 on 1 이미지 외 상품 설명 제거. 어드민 상품 속성에 porduct1on1이 true인 경우에만 PDP 화면 아래쪽에 표시됨.
				if($this.find('[data-1on1-description]').length > 0){
					var html = $.parseHTML($this.find('[data-long-description]').attr('data-long-description'));
					$(html).find('div.proInfo').remove().find('script').remove();
					$this.find('[data-1on1-description]').empty().append(html);
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-product]',
					attrName:'data-module-product',
					moduleName:'module_product',
					handler:{context:this, method:Method.moduleInit}
				});
				//console.log( "product init");
				/*
				if( _.isFunction(marketingUpdateProductInfo)){
					marketingUpdateProductInfo();
				}
				*/
			},
			destroy:function(){
				$this = null;
				args = [];
				productOption = null;
				quantity = null;
			}
		}
	});
})(Core);
