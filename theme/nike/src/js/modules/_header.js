(function(Core){
	Core.register('module_header', function(sandbox){
		var $this, $headerMenuWrap, args, isSignIn = false, modal, isModalHide = false, isRefresh = false, reDirectUrl = '';
		var Method = {
			moduleInit:function(){
				$this = $(this);
				$headerMenuWrap = $this.find('.header-mymenu');
				args = arguments[0];
				isSignIn = (args.isSignIn === 'true') ? true : false;
				modal = UIkit.modal('#common-modal', {center:true});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-header]',
					attrName:'data-module-header',
					moduleName:'module_header',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			setLogin:function(callBackFunc){
				var _self = this;

				if(!isSignIn){
					//로그인 전
					sandbox.utils.promise({
						url:'/dynamicformpage',
						type:'GET',
						data:{'name':'login', 'dataType':'model'}
					}).then(function(data){
						var defer = $.Deferred();
						var appendTxt = $(data).find('.content-area').html();
						$('#common-modal').find('.contents').empty().append(appendTxt);
						sandbox.moduleEventInjection(appendTxt, defer);
						modal.show();
						return defer.promise();
					}).then(function(data){
						$headerMenuWrap.find('li').eq(0).empty().append($(args.template).html());
						isSignIn = true;
						if(isModalHide) modal.hide();
						if(!isRefresh) callBackFunc({isSignIn:true});
						else if(isRefresh) location.href = reDirectUrl;
					}).fail(function(msg){
						defer = null;
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});

						//로그인 실패시 재귀호출
						_self.setLogin(callBackFunc);
					});
				}else{
					//로그인 후
					callBackFunc({isSignIn:true});
				}
			},
			getIsSignIn:function(){
				return isSignIn;
			},
			setModalHide:function(isHide){
				isModalHide = isHide;
				return this;
			},
			reDirect:function(url){
				isRefresh = true;
				reDirectUrl = (url) ? url : location.href;
				return this;
			},
			popRegister:function(callBackFunc){
				sandbox.utils.promise({
					// url:'/register',
					url:'/dynamicformpage',
					type:'GET',
					data:{'name':'register', 'dataType':'model'}
				}).then(function(data){
					var defer = $.Deferred();
					var appendTxt = $(data).find('.content-area').html();
					$('#common-modal').find('.contents').empty().append(appendTxt);
					sandbox.moduleEventInjection(appendTxt, defer);
					modal.show();
					return defer.promise();
				}).fail(function(msg){
					defer = null;
					console.log('msg:' , msg);
					UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
					//회원 가입 실패시 재귀호출
					_self.popRegister(callBackFunc);
				});
			}
		}
	});
})(Core);
