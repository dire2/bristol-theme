(function(Core){
	Core.register('module_thedrawfaq', function(sandbox){
		var $that;
		var closingMSTime = 0, drawStatus, discountTimer, before60Mins=60*60*1000, before1Min=60*1000 ;
		var Method = {
			moduleInit:function(){
				
				$('.page-scroll').bind('click', function(event) {	
					if ( $(window).width() < 870 ){
						var $anchor = $(this);
						$('html, body').stop().animate({
							scrollTop: $($anchor.attr('href')).offset().top - 98
						}, 1000/*, 'easeInOutExpo'*/);
						event.preventDefault();
					}else {
						var $anchor = $(this);
						$('html, body').stop().animate({
							scrollTop: $($anchor.attr('href')).offset().top -98
						}, 1000/*, 'easeInOutExpo'*/);
						event.preventDefault();	
					}
				});

			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-thedrawfaq]',
					attrName:'data-module-thedrawfaq',
					moduleName:'module_thedrawfaq',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
