(function(Core){
	Core.register('module_search', function(sandbox){
		var $this, args, clickIS, endPoint, latestKeywordList, arrLatestKeywordList = [], isSaveLatest;

		//초기값 설정, 쿠키에 저장 
		isSaveLatest = $.cookie('isSaveLatest') 
		if(isSaveLatest == undefined){
			$.cookie('isSaveLatest', 'true');
			isSaveLatest = $.cookie('isSaveLatest');
		}
		var setSaveLatestText = function(){
			if(isSaveLatest == 'true'){
				$('#toggle-save-latest').text("검색어저장 끄기");
			} else {
				$('#toggle-save-latest').text("검색어저장 켜기");
			}
		}
		setSaveLatestText();

		var setSearchKeyword = function(keyword){
			var pattern = new RegExp(keyword, 'g');
			arrLatestKeywordList = sandbox.utils.rtnMatchComma(latestKeywordList.replace(pattern, ''));
			arrLatestKeywordList.unshift(keyword);

			if(arrLatestKeywordList.length >= args.keywordMaxLen){
				arrLatestKeywordList = arrLatestKeywordList.slice(0, -1);
			}

			if(isSaveLatest == 'true'){
				$.cookie('latestSearchKeyword', arrLatestKeywordList.join(','));
			}
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				clickIS = false;

				latestKeywordList = $.cookie('latestSearchKeyword') || '';
				arrLatestKeywordList = sandbox.utils.rtnMatchComma(latestKeywordList || '');
				endPoint = Core.getComponents('component_endpoint');

				sandbox.getComponents('component_searchfield', {context:$this, resultTemplate:'#search-list', resultWrap:'.etc-search-wrap'}, function(){
					this.addEvent('resultSelect', function(data){
                        var text = $(data).text();

                        //nike는 인기검색어 앞에 순번이 있어 아이템 선택시 순번 제거 필요. 
                        if(text.startsWith('10')){
                            text = text.substring(4);
                        } else if(text.match(/^\d/)){
                            text = text.substring(3);
                        }     

						var endPointData = {
							key : text,
							text : text
                        }
                        
						endPoint.call( 'searchSuggestionClick', endPointData );
						this.getInputComponent().setValue(text);
						setSearchKeyword(text);
						location.href='/search?q='+ text;
					});

					this.addEvent('beforeSubmit', function(data){
						setSearchKeyword(data);
					});

					// this.addEvent('removeList', function(keyword){
					// 	// 삭제버튼 이벤트를 받는 부분
					// 	// keyword 가 all 일때 쿠키의 최근 검색어를 모두 삭제
					// 	if(keyword === 'all'){
					// 		$.cookie('latestSearchKeyword', '');
					// 	}else{
					// 		latestKeywordList = latestKeywordList.replace(keyword, '');
					// 		$.cookie('latestSearchKeyword', latestKeywordList);
					// 	}
					// });

					/* 최근검색어 */
					if(args.isLatestKeyword === 'true'){
						this.setResultPrepend('#keyword-container', '#latest-search-keyword', {
							label:'최근 검색어',
							keyword:arrLatestKeywordList
						});
					}
				});

				// search btn (search field show&hide)
				$('.gnb-search-btn').click(function(e){
					e.preventDefault();

					if(clickIS){
						clickIS = false;
						$this.removeClass('active');
					}else{
						clickIS = true;
						$this.addClass('active');
					}
				});
				
				//최근 검색어 삭제 버튼 
				$('#delete-all-latest').click(function(e){
					// $.removeCookie('latestSearchKeyword', { path: '/' });
					$.cookie('latestSearchKeyword', '');
					//최근검색어 리스트 삭제
					$('#latest-keyword > li').remove();
                   
					//검색입력창 최근 검색어 삭제 
					// $this.find($('#search')).val("");
				});

				//검색어 저장 토글 
				$('#toggle-save-latest').click(function(e){	
					// $.cookie('isSaveLatest', !isSaveLatest);
					if(isSaveLatest == 'true'){
						isSaveLatest = 'false';
					} else {
						isSaveLatest = 'true';
					}
					$.cookie('isSaveLatest', isSaveLatest);
					setSaveLatestText();
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-search]',
					attrName:'data-module-search',
					moduleName:'module_search',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			searchTrigger:function(){
				$('.gnb-search-btn').trigger('click');
			}
		}
	});
})(Core);
