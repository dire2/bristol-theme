(function(Core){
	'use strict';
	Core.register('module_review_write', function(sandbox){
		var $deferred = null;
		var imgCount = 0;
		var removeId = null;
		var maxCount = 5;
		var currentStarCount = 0;
		var starCountIS = false;
		var fileLoad = null;
		var arrDescription = ['별로에요.', '그저 그래요.', '나쁘지 않아요.', '마음에 들어요.', '좋아요! 추천합니다.'];
		var imgTemplate = '<span class="preview-up-img"><a href="javascript:;" class="file-remove_btn"></a><img src="{{imgUrl}}" alt="{{imgAlt}}" /></span>';
		var inputHiddenTemplate = '<input type="hidden" name="fileList[{{id}}].fullUrl" class="file-{{id}}" value={{fullUrl}} /><input type="hidden" name="fileList[{{id}}].fileName" class="file-{{id}}" value={{fileName}} />';
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var $form = $this.find('#review-write-form');
				var $imgContainer = $this.find('.uplode-preview-list');
				var $thumbNailWrap = $this.find('.thumbnail-wrap');
				var $textArea = sandbox.getComponents('component_textarea', {context:$this});
				var $submitArea = $this.find('input[type=submit]');

				//작성완료 버튼 활성화 
				function enableSumitButton(){
					if( starCountIS && $textArea.getValue().length > 0){
						if($submitArea.hasClass('disabled')){
							$submitArea.removeClass('disabled')
						}	
					}
				}
				//textArea 포커스 이벤트
				 $this.find('#comment').change(function(){
					enableSumitButton();
				 });

				fileLoad = sandbox.getComponents('component_file', {context:$this}, function(){
					var _self = this;
					this.addEvent('error', function(msg){
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'warning'});
					});

					this.addEvent('upload', function(fileName, fileUrl){
						console.log(fileName, fileUrl);
						$thumbNailWrap.append(sandbox.utils.replaceTemplate(imgTemplate, function(pattern){
							switch(pattern){
								case 'imgUrl' :
									return fileUrl;
									break;
								case 'imgAlt' :
									return fileName;
									break;
							}
						}));

						imgCount++;
						_self.setCurrentIndex(imgCount);
					});
				});

				imgCount = $thumbNailWrap.children().size();
				fileLoad.setCurrentIndex(imgCount);

				currentStarCount = $this.find('.rating-star a').filter('.active').length - 1;

				$this.find('.rating-star a').click(function(e) {
					e.preventDefault();
					var index = $(this).index() + 1;
					$(this).parent().children('a').removeClass('active');
					$(this).addClass('active').prevAll('a').addClass('active');

					$this.find('input[name=rating]').val(index*20);
					$this.find('input[name=starCount]').val(index);
					$this.find('.rating-description').text(arrDescription[index-1]);

					starCountIS = true;

					enableSumitButton();
				});

				if(currentStarCount >= 0){
					$this.find('.rating-star a').eq(currentStarCount).trigger('click');
				}

				$this.find('.uplode-preview-list').on('click', '.file-remove_btn', function(e){
					e.preventDefault();
					var index = $(this).attr('href');
					$(this).parent().remove();

					imgCount--;
					fileLoad.setCurrentIndex(imgCount);
				});

				$this.find('input[type=submit]').click(function(e){
					e.preventDefault();

					if(!starCountIS || !$textArea.getValidateChk()){
						return;
					}

					$thumbNailWrap.children().each(function(i){
						var $this = $(this);
						$form.append(sandbox.utils.replaceTemplate(inputHiddenTemplate, function(pattern){
							switch(pattern){
								case 'id' :
									return i;
									break;
								case 'fileName' :
									return $this.find('img').attr('alt');
									break;
								case 'fullUrl' :
									return $this.find('img').attr('src');
									break;
							}
						}));
					});

					var itemRequest = BLC.serializeObject($form);
					sandbox.utils.ajax($form.attr('action'), $form.attr('method'), itemRequest, function(res){
						var data = sandbox.rtnJson(res.responseText);

						if(data.hasOwnProperty('errorMessage') || !data){
							if($deferred) $deferred.reject(data.errorMesage);
							else UIkit.notify(data.errorMesage, {timeout:3000,pos:'top-center',status:'danger'});
						}else{
							/*리뷰 작성 시 상품 화면으로 되돌아 가면서 reflesh 되지 않아 우선 막음*/
							/*if($deferred) $deferred.resolve(data);
							else */location.href = data.redirectUrl;
							Core.ga.action('review','write');
						}
					}, true);
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-review-write]',
					attrName:'data-module-review-write',
					moduleName:'module_review_write',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$deferred = null;
				console.log('destroy review-write module');
			},
			setDeferred:function(defer){
				$deferred = defer;
			},
			moduleConnect:function(){
				fileLoad.setToappUploadImage({
					fileName:arguments[0],
					fullUrl:arguments[1],
					result:(arguments[2] === '1') ? true : false
				});
			}
		}
	});
})(Core);
