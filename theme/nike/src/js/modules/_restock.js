(function(Core){
	Core.register('module_restock', function(sandbox){
		var $this = $(this);
        
		var Method = {
			moduleInit:function(){
				//SKU 정보에서 재고수량을 확인 하여 입고 알림 가능한 사이즈만 활성화 
				var args = arguments[0];
				Method.checkQuantity(args);
				
				//입고알림 문자받기 show or hide
				if(document.getElementById("set-restock-alarm") && args.skudata){
					for(var index = 0; args.skudata.length > index; index++){
						if(0==args.skudata[index].quantity){
							//enable 입고알림문자받기
							$('#set-restock-alarm').show();
							break;
						}
					}
				}
					
				//사이즈 선택시 css 변경
				$(this).find('#size-grid li').on('click', function(e){
					if(!$(this).attr('disabled')){
						$(this).parent().find('li a').each(function(){
							//기존에 선택된 사이즈 해지
							$(this).removeClass('selected');
						});
						$(this).find('a').addClass('selected');
						//사이즈 영역에 선택한 사이즈 값 표시
						document.getElementById('size-value').innerHTML= $(this).text();
						//sku id 저장
						$('#size-value').attr('data-sku-id', args.skudata[$(this).index()].skuId);
						//사이즈 선택 후 사이즈 선택역역을 숨긴다
						Method.sizeTableOpen();
					}
				});

				//사이즈 표시 영역 선택 
				$(this).find('#size-value-area').on('click', function(){
					console.log('size-value-area click');
					//사이즈가 선택된 경우에만 사이즈 리스트를 닫는다.
					if($('#size-value').attr('data-sku-id')){
						Method.sizeTableOpen();
					} else {
						UIkit.modal.alert("사이즈를 선택하세요.");
					}
					
				});

                //개인정보 취급 방침 팝업 열기
                $(this).find('#privacyPolicyLink').on('click', function(e){
                    $('#layerPrivacyPolicy').show();
                });
                
				//개인정보 취급 방침 팝업 닫기
				$('[id^="closePolicy"]').each(function(){
					$(this).click(function(){
						$('#layerPrivacyPolicy').hide();
					});
				});
				//입고알림 서비스 신청 하기 
				$(this).find('#request-restock-alarm').on('click', function(e){
					//사이즈 선택 확인
					if(!$('#size-value').attr('data-sku-id')){
						UIkit.modal.alert("사이즈를 선택하세요.");
						return;
					}
					//휴대전화 번호 입력 확인 targetValue
					var phoneNum = document.getElementById("targetValue").value;
					if(10 > phoneNum.length){
						UIkit.modal.alert('휴대폰번호를 정확하게 입력 하셔야<br/>입고 알림 서비스를 이용 하실 수 있습니다.');
						return;
					} else {
						var pattern =  new RegExp('^[0-9]*$', 'g');
						if(!pattern.test(phoneNum)){
							UIkit.modal.alert('휴대폰번호를 정확하게 입력 하셔야<br/>입고 알림 서비스를 이용 하실 수 있습니다.');
							return;
						}
					}
					//체크박스 확인
					if(!$('#check-privacy-policy-agree').hasClass('checked')){
						UIkit.modal.alert('개인정보 취급방침 이용에 동의 하셔야<br/>입고 알림 서비스를 이용 하실 수 있습니다.');
						return;
					}
					
					//phone number format
					var tempPhone, formatPhone;
					if(phoneNum.length > 10){
						tempPhone = phoneNum.match(/^(\d{3})(\d{4})(\d{4})$/);
					} else {
						tempPhone = phoneNum.match(/^(\d{3})(\d{3})(\d{4})$/);
					}
					formatPhone = tempPhone[1] + '-' + tempPhone[2] + '-' + tempPhone[3];
					var notify = '고객님께서 수신 동의 하신<br/>' + formatPhone  +'(으)로<br/>입고 알림 문자가 1회 발송됩니다.'
					UIkit.modal.confirm(notify, function(){
						Method.add();
					});	
				});
				
                
			},
			checkQuantity:function(args){
				// console.log('check arg:', args);
				if(args && undefined != args.skudata){
					$('#size-grid').find("li").each( function(index){
						if(args.skudata.length > index){
							if(0==args.skudata[index].quantity){
								//입고알림에서는 재고가 없는 상품을 활성화
								$(this).removeAttr('disabled');
								$(this).find("a").removeClass('sd-out');
							}
							// console.log('sku quantity:', args.skudata[index].quantity);
						}
					});
				}
			},
			add:function(){
				var customerId = $('#request-restock-alarm').attr('data-customer-id');
				var obj = {
					'id' : customerId?customerId:'', //계정
					'skuId' : $('#size-value').attr('data-sku-id'), 
					'messageType' : 'SMS', //SMS/EMAIL
					'target' : document.getElementById("targetValue").value       //SMS인 경우 번호, EMAIL인 경우 이메일
				}	
				// console.log('obj:', obj);
					
				Core.Utils.ajax('/restock/add', 'GET',obj, function(data) {
					var data = $.parseJSON( data.responseText );
					if( data.result ){
						$('.uk-modal-close').click();
						// $(".sms-complete").show();
						var sucessMsg = '입고 알림 신청이 완료되었습니다.'
						UIkit.notify(sucessMsg, {timeout:3000,pos:'top-center',status:'success'});
					}else{
						UIkit.notify(data.errorMsg, {timeout:3000,pos:'top-center',status:'danger'});
					}
				},true);
			},
			sizeTableOpen:function(){
				console.log('sizeTableOpen');
				if($('#size-value-area').hasClass('open')){
					$('#size-value-area').removeClass('open')
					$('#size-list-area').slideUp();
					console.log('hasClass');
				} else {
					$('#size-value-area').addClass('open')
					$('#size-list-area').show('bind');
					console.log('else hasClass');
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-restock]',
					attrName:'data-module-restock',
					moduleName:'module_restock',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
