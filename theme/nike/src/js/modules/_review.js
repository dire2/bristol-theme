(function(Core){
	Core.register('module_review', function(sandbox){
		var $deferred, $this, modal, args, arrQueryString = [], currentProductId, isSignIn;
		var Method = {
			moduleInit:function(){
				args = arguments[0];
				$this = $(this);
				isSignIn = (args.isSignIn === 'true') ? true : false;

				//필터조건 초기화 ( 최신순, 도움순 )
				arrQueryString[2] = sandbox.utils.getQueryParams($('.sort-tabs').find('.sort-item').filter('.active').attr('href'), 'array').join('&');

				//modal init
				// modal = UIkit.modal('#common-modal', {center:true});
				modal = UIkit.modal('#common-modal-large', {center:true});
				modal.off('.uk.modal.review').on({
					'hide.uk.modal.review':function(){
						console.log('review modal hide');
						$this.find('.contents').empty();
						if(isSignIn != sandbox.getModule('module_header').getIsSignIn()){
							if(currentProductId) Method.reviewProcessorController(currentProductId);
						}
					}
				});

				// product detail 상품 리뷰 쓰기
				$('.review-write-btn').off('click').on('click', function(e){
					e.preventDefault();

					var target = $(this).attr('href');
					var productId = $(this).attr('data-productid');

					if(!productId){
						UIkit.notify('productID 가 없습니다.', {timeout:3000,pos:'top-center',status:'warning'});
						return;
					}

					Method.reviewTask(target, productId);
				});


				//review feedback
				var feedBack = sandbox.getComponents('component_like', {context:$this}, function(){
					this.addEvent('likeFeedBack', function(data){
						if(data.hasOwnProperty('HELPFUL')){
							// $(this).parent().siblings().find('.like-num').text(data.HELPFUL);
							$(this).find('.num').text(data.HELPFUL);
						}else if(data.hasOwnProperty('NOTHELPFUL')){
							$(this).find('.num').text(data.NOTHELPFUL);
							// console.log(data.NOTHELPFUL);
						}
					});
				});


				// 상품 리뷰 수정
				$('.review-modify').off('click').on('click', function(e){
					e.preventDefault();

					var target = $(this).attr('href');
					var url = $(this).attr('data-link');
					var productId = $(this).attr('data-productid');
					var defer = $.Deferred();
					var successMsg = $(this).attr('data-successmsg');

					//review 모달 css 추가
					$(target).addClass('review-write');

					sandbox.utils.promise({
						url:url,
						type:'GET',
						data:{'redirectUrl':location.pathname}
					}).then(function(data){
						$(target).find('.contents').empty().append(data);
						sandbox.moduleEventInjection(data, defer);
						return defer.promise();
					}).then(function(data){
						//arrQueryString = [];
						UIkit.notify(successMsg, {timeout:3000,pos:'top-center',status:'success'});
						Method.reviewProcessorController(productId);
						modal.hide();
					}).fail(function(msg){
						defer = null;
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
						if(modal.isActive()) modal.hide();
					});
				});



				// 상품 리뷰 삭제
				$('.review-remove').off('click').on('click', function(e){
					e.preventDefault();
					var _self = this;
					var url = $(this).attr('href');
					var productId = $(this).attr('data-productid');
					var reviewId = $(this).attr('data-reviewId');
					var successMsg = $(this).attr('data-successmsg');

					UIkit.modal.confirm("삭제 할까요?", function(){
						sandbox.utils.ajax(url, 'GET', {}, function(data){
							var data = sandbox.rtnJson(data.responseText);
							if(data.result){
								UIkit.notify(successMsg, {timeout:3000,pos:'top-center',status:'success'});
								Method.reviewProcessorController(productId);
							}else{
								UIkit.notify(data.errorMessage, {timeout:3000,pos:'top-center',status:'danger'});
							}
						});
					});
				});


				//review filter
				$('.review-filter').click(function(e){
					e.preventDefault();
					var query = sandbox.utils.getQueryParams($(this).attr('href'), 'array').join('&');
					var productId = $(this).attr('data-productid');

					if($(this).hasClass('star')){
						arrQueryString[0] = query;
					}else if($(this).hasClass('hash')){
						arrQueryString[1] = query;
					}else if($(this).hasClass('other')){
						arrQueryString[2] = query;
					}

					Method.reviewProcessorController(productId);
				});

				$('.review-filter-delete').click(function(e){
					e.preventDefault();

					var query = sandbox.utils.getQueryParams($(this).attr('href'), 'array').join('&');
					var productId = $(this).attr('data-productid');

					arrQueryString = [];
					arrQueryString[2] = query;
					Method.reviewProcessorController(productId);
				});



				/* browse history back */
				if(sandbox.utils.mobileChk) {
					$(window).on('popstate', function(e) {
						var data = e.originalEvent.state;
						if(modal && modal.active){
							modal.hide();
						}
					});
				}
			},
			reviewTask:function(target, productId){
				var defer = $.Deferred();
				currentProductId = productId;

				sandbox.getModule('module_header').setLogin(function(data){
					//console.log('review : ', data);
					var isSignIn = data.isSignIn;
					sandbox.utils.promise({
						url:'/review/reviewWriteCheck',
						type:'GET',
						data:{'productId':productId}
					}).then(function(data){
						//data.expect 기대평
						//data.review 구매평
						if(data.expect || data.review){
							/* review history */
							if(sandbox.utils.mobileChk) history.pushState({page:'review'}, "review", location.href);

							isSignIn = isSignIn;
							modal.show();
							return sandbox.utils.promise({
								url:'/review/write',
								type:'GET',
								data:{'productId':productId, 'redirectUrl':location.pathname, 'isPurchased':data.review}
							});
						}else{
							defer.reject('리뷰를 작성할 수 없습니다.');
						}
					}).then(function(data){
						$(target).addClass('review-write');
						$(target).find('.contents').empty().append(data);
						sandbox.moduleEventInjection(data, defer);
						return defer.promise();
					}).then(function(data){
						Method.reviewProcessorController(productId);
						modal.hide();
					}).fail(function(msg){
						defer = null;
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
						if(modal.isActive()) modal.hide();
					});
				});
			},
			reviewProcessorController:function(productId){
				var arrData = [];
				var obj = {
					/*'mode':'template',
					'templatePath':'/modules/productListReview',
					'resultVar':'review',*/
					'productId':productId
				}

				for(var key in obj){
					arrData.push(key+'='+obj[key]);
				}

				///processor/execute/review      /review/list, /account/reviewlist
				//console.log(arrQueryString.join('&'));

				//템플릿 캐시로 인해 추가된 로딩바 상태
				sandbox.setLoadingBarState(true);
				sandbox.utils.ajax(args.api, 'GET', arrData.join('&') + arrQueryString.join('&') + '&mode=template&resultVar=reviewSummaryDto', function(data){
                    // 탭 선택시 페이지 초기화
                    sessionStorage.setItem('categoryCurrentPage', 1);

                    //li 부분만 서버에서 받은 템플릿으로 교체 한다.
                    var ulTag = $(args.target).find('#review-list');
                    ulTag.empty();
                    $(data.responseText).find('li').each(function(index){
                        ulTag.append(this);
                    });
                    //다른 텝에서 '더 보기'를 눌러 전체 리뷰를 로드한 경우 버튼이 없으므로 다시 설정
                    if($this.find('button#load-more').css("display") == "none"){
                            $('button#load-more').show();
                    }
                    // $(args.target).empty().append(data.responseText);
					sandbox.moduleEventInjection(data.responseText);
				}, false, false);
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-review]',
					attrName:'data-module-review',
					moduleName:'module_review',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$deferred = null;
				$this = null;
				args = null;
				modal = null;

				console.log('destroy reveiw module');
			},
			setDeferred:function(defer){
				$deferred = defer;
			},
			history:function(){

			}
		}
	});
})(Core);
