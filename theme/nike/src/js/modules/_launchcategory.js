(function(Core){
	Core.register('module_launchcategory', function(sandbox){
		var $that, category;
		// var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];
		var Method = {
			moduleInit:function(){
				$this = $(this);

                if(arguments[0] && undefined != arguments[0].category){
					category = arguments[0].category;
				}
				// console.log('category:', category);

                //마우스 클릭시 탭 하단 선택 표시
				$('.launch-menu').on('click', 'li', function(){
					$('.launch-menu li.on').removeClass('on');
					$(this).addClass('on');
				});
                //우측 뷰 변경 아이콘
				$this.find('.toggle-box a').on('click', function(){
					//갤러리뷰 <->그리드뷰
					var items = document.querySelectorAll('.launch-category > .uk-grid > .uk-width-large-1-3');
					// console.log('items:', items);
					/* 
						class 정의
						feed: (gallery)uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-3 feedItem 
						      (grid)uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-6 feedItem
						in stock: (gallery)uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-3 instockItem 
						          (grid)uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-6 instockItem
						upcoming: (gallery)uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-3
						          (grid)uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-6
						*/
					var appendClass = '';
					if(category == 'feedItem' || category == 'instockItem'){
						appendClass = category;
					}
					if(items != null && items.length > 0){
						//gallery to grid
						$('.toggle-box a span').removeClass('ns-grid').addClass('ns-feed');
						$('.toggle-box a').removeClass('view-grid').addClass('view-list');
						for(var i = items.length - 1; i >= 0; --i){
							items[i].className = "uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-6 " + appendClass;
						}
					} else {
						//grid to gallery
						$('.toggle-box a span').removeClass('ns-feed').addClass('ns-grid');
						$('.toggle-box a').removeClass('view-list').addClass('view-grid');

						items = document.querySelectorAll('.launch-category > .uk-grid > .uk-width-large-1-6');
						for(var i = items.length - 1; i >= 0; --i){
							items[i].className = "uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-3 " + appendClass;
						}
					}
				});
				//카테고리에서 상픔 클릭시 상단 탭 선택표시 off 및 우측 아이콘 숨김
				$('.launch-category .uk-grid div a').on('click', function(){
					$('.launch-menu li.on').removeClass('on');
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-launchcategory]',
					attrName:'data-module-launchcategory',
					moduleName:'module_launchcategory',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
