(function(Core){
	Core.register('module_thedraw', function(sandbox){
		var $that;
		var closingMSTime = 0, openingMSTime = 0, drawStatus, discountTimer, before60Mins=60*60*1000, before1Min=60*1000, waitingForOpening ;
		var Method = {
			moduleInit:function(){
				$this = $(this);
				if(null != arguments && arguments[0]){
					if(undefined!=arguments[0].closingMSTime){
						closingMSTime = arguments[0].closingMSTime;
					}
					if(undefined!=arguments[0].openingMSTime){
						openingMSTime = arguments[0].openingMSTime;
					}
					if(undefined!=arguments[0].drawStatus){
						drawStatus = arguments[0].drawStatus;
						//drawStatus : ready 신청시작전 / opening 디스카운트 진행 중 / closed 신청종료
					}
					// console.log('drawStatus: %s, closingMSTime: %s', drawStatus, closingMSTime);
				}

				function prependZero(num, len) {
					while(num.toString().length < len) {
						num = "0" + num;
					}
					return num;
				}
				
				function TimeDiscount(){
					var currentMSTime = new Date().getTime();
					
					if(closingMSTime >= currentMSTime){
						var remainingTime = closingMSTime - currentMSTime;
						var days, hours, minuntes, seconds;
						// console.log('openingMSTime:' + openingMSTime + ', currentMSTime:' + currentMSTime);

						if(openingMSTime > currentMSTime){
							//count 시작 전
							waitingForOpening = true;
							// console.log('기다리는중');
						} else {
							//ready > opening이 되면 화면 reflash
							if(waitingForOpening == true){
								CloseCount();
								location.reload();
								// console.log('reload!!!, waitingForOpening:' , waitingForOpening);	
							}
							//count 진행 
							if(before1Min >= remainingTime){
								//1분전 붉은 글씨 표시
								if(!$('#remainTime').hasClass("c_ff0015")){        		
								 $('#remainTime').addClass("c_ff0015");
								}
							}
							
							seconds = Math.floor(remainingTime / 1000);
							minuntes = Math.floor(seconds / 60);
							seconds = seconds % 60;
							hours = Math.floor(minuntes / 60);
							minuntes = minuntes  % 60;
							hours = hours % 24;

							document.getElementById('remainingHours').innerHTML = prependZero(hours, 2) + '<span class="timeDot">:</span>';
							document.getElementById('remainingMinutes').innerHTML = prependZero(minuntes, 2)+ '<span class="timeDot">:</span>';
							document.getElementById('remainingSeconds').innerHTML = prependZero(seconds, 2);
							
							// console.log('%s:%s:%s', prependZero(hours, 2), prependZero(minuntes, 2), prependZero(seconds, 2));
						}
					} else {
						 //종료 시 타이머 종료 후 화면 리로드
						 CloseCount();
						 location.reload();
					}
					
				}

                function OpenCount(){
					// console.log('OpenCount');					
					if(drawStatus !== 'closed'){
					    discountTimer = setInterval(TimeDiscount, 500);
					}
				}

				function CloseCount(){
					if(undefined !== 'closed'){
						clearInterval(discountTimer);
					}
					
				}

				OpenCount();
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-thedraw]',
					attrName:'data-module-thedraw',
					moduleName:'module_thedraw',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
