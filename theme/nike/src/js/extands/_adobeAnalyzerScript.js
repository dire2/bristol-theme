(function(Core){
	// 전역으로 사용될 기본 변수명
	var md = null;
	dl = {};	

	function init(){
		md = _GLOBAL.MARKETING_DATA();
		$.extend( dl, getPageData());

		// 기본 정보 이외의 추가 정보를 처리해야 하는 타입들
		switch( md.pageType ){
			case "category" : 
				$.extend( dl, getCategoryData());
			break;

			case "search" : 
				$.extend( dl, getSearchData());
			break;

			case "product" : 
				$.extend( dl, getProductData());
			break;

			case "cart" : 
				$.extend( dl, getCartData());
			break;

			case "checkout" : 
				
			break;

			case "confirmation" : 
				$.extend( dl, getOrderConfirmationData());
			break;

			case "register":
				$.extend( dl, getRegisterStartData());
			break;

			case "registerSuccess":
				$.extend( dl, getRegisterComplateData());
			break;

		}

		console.log( dl );	
		window._dl = dl;
	}

	function getPageData(){
		var data = {};
		data.site_app_name = "nikestorekr"; // 고정
		data.page_division = "Commerce"; // 고정
		data.country = "kr";
		data.language = "ko-KR";

		data.page_name = ["section1", "section2"];
		data.site_section = getSectionL1Data(); // gender : man, women, boy, girls 
		data.site_section_l2 = ""; // category 2depth 정보

		// 최상위 카테고리 4개중에 하나를 눌렀을때만 서브 카테고리 정보인 L2 필요함
		if( data.site_section  != "" ){
			data.site_section_l2 = getSectionL2Data();
		}

		data.page_type = getPageTypeData(); //goods, grid wall/ grid wall:PWH  prop17

		data.login_status = _GLOBAL.CUSTOMER.ISSIGNIN ? "logged in" : "not logged in";    //logged in,  not logged in

		// 로그인 후 첫 페이지 이면 
		var isFirstLogin = true;
		if( isFirstLogin == "true" ){
			data.page_event = {
				login : true 
			}
		}

		return data;
	}

	function getPageTypeData(){
		// goods
		// grid wall -> categoryData 에서 처리 

		// homepage
		if( md.pathName == '/' ){
			return "homepage";
		}
		// snkrs 
		// search -> searchData에서 처리 

		// men/sport/running
		// sports landing
		if( md.pathName.indexOf( "/sport/" ) > -1 ){
			return "sport landing";
		}
		// brand landing 
		if( md.pathName.indexOf( "/brand/" ) > -1 ){
			return "brand landing";
		}
		// gender landing
		if( getRegexTestResult( /^(\/men|\/women|\/boys|\/girls)$/g, md.pathName ) ){
			return "gender landing";
		}

		// memeber 
		// my page
		// cart -> cartData에서 처리 
		// order -> checkout 에서 처리
		// cscenter -> 이부분은 확인 해야함 zendesk에서 던져줘야 하는 정보가 될수있음
		// the draw 
		// mylocker 
		// etc

		return "etc";

	}

	function getSectionL1Data(){
		var patten = "";

		if( getRegexTestResult( /^\/category\/men/g, md.pathName )) {
			return "men";
		}

		if( getRegexTestResult( /^\/category\/women/g, md.pathName )) {
			return "women";
		}		

		if( getRegexTestResult( /^\/category\/boys/g, md.pathName )) {
			return "boys";
		}		

		if( getRegexTestResult( /^\/category\/girls/g, md.pathName )) {
			return "girls";
		}		
		return "";
	}

	function getSectionL2Data(){
		var subCategory = md.pathName.split("/");

		if( subCategory.length >= 5 ){
			// 첫번째 / 때문에 length가 1이 더 잡히기 때문에 4로 리턴
			return subCategory[4];
		}

		return "";
	}

	// 카테고리 정보
	function getCategoryData(){
		var data = {};
		data.page_type = "grid wall";

		if( md.categoryInfo != null ){
			var categoryInfo = md.categoryInfo;

			if( categoryInfo.hasHeaderContent == true ){
				data.page_type += ":PWH";
			}

			// todo
			// 필터 적용하는 부분에서 url에 lf 값이 있으면 제거해줘야 함 

			// 검색 필터를 사용자가 선택해서 넘어온경우가 아닌 링크로 만들어놓은 url에 필터가 걸려있으면 facet을 처리하지 않는다.

			if( categoryInfo.facet != null ){
				if( String(categoryInfo.lf).toUpperCase() != "Y" ){
					data.search_facet = JSON.stringify(categoryInfo.facet);		
					// facet를 사용한 경우
					data.page_event = {
						endeca_filter_applied :true
					}
				}
			}
		}
		return data;
	}

	// 상품정보
	function getProductData(){
		var data = {};
		data.page_type = "goods";

		if( md.productInfo != null && md.customProductInfo != null ){
			var isSale = ( md.productInfo.price < md.productInfo.retailPrice );
			var priceState = "clearance";
			if( isSale ){
				if( "임시세일 조건" == true ){
					priceState = "reduced";
				}
				// 세일이면서 아무 값이 없으면 시즌오프 clearance
			}
			data.proudcts = [
				{
					product_category : md.customProductInfo.productCategory, 	// products, prop1, eVar12, prop20
					product_name : md.productInfo.name, 			// products, prop1, eVar12, prop20
					product_quantity : "PRODUCT_QUANTITY", 	// 수량 정보는 sku 단위이기 때문에 불가능 
					product_unit_price : md.productInfo.price,						

					product_inventory_status : "PRODUCT_INVENTORY_STATUS", // 재고 상태
					avg_product_rating : "AVG_PRODUCT_RATING", // 평균 review 평점
					price_status : priceState, // 가격 정책
					number_of_product_review : "#_PRODUCT_REVIEW", // review 갯수
					product_finding_method : "PRODUCT_FINDING_METHOD", // 상품 페이지 방문 경로 		
					//onsite search(검색으로 바로 이동), browser(일반 plp에서), internal promotion(내부 프로모션 링크), external camopaign( cp코드 있으면 ), referring nike site(?), cross-sell(다른상품에서)	
					
				}
			];

			// 링크 param 
			data.page_event = {
				product_view : true, // product detail views
			}
			
			if( "cross-sell" == true ){
				data.page_event.cross_sell_click_through = true // 추천상품 링크를 통해 PDP 페이지를 들어온 경우
				data.proudcts[0].cross_sell_source = "CROSS_SELL_SOURCE"; // 추천 링크를 통한 PDP 페이지 방문 경로                       evar14=pdp:AA1128-200
			}

			//TODO 
			//재고 확인 스크립트가 모두 적용된 후에나 처리 가능한 정보임 
			//data.page_event.value_out_of_stock_item = "VALUE_OUT_OF_STOCK_ITEM"; // VALUE_OUT_OF_STOCK_ITEM (Number) PDP out-of-stock의 경우
		}

		return data;
	}

	// 카트 정보
	function getCartData(){
		var data = {};
		data.page_type = "cart";

		data.page_event = {
			cart_view : true, // 장바구니 보기 (장바구니 페이지 열기)
		}		

		return data;
	}

	// 주문완료 정보
	function getOrderConfirmationData(){
		var data = {};
		data.page_type = "order";

		/* 구매확정시 필요 속성 영역 */
		data.purchase_id = "PURCHASE_iD", // 구매 (확정) 번호
		data.payment_method = "PAYMENT_METHOD", // 결제 수단		

		data.products = 
		[ 
			{ 
				product_category : "PRODUCT_CATEGORY", 	// products, prop1, eVar12, prop20
				product_name : "PRODUCT_NAME", 			// products, prop1, eVar12, prop20
				product_quantity : "PRODUCT_QUANTITY",
				product_unit_price : "PRODUCT_UNIT_PRICE",
				
			}, 
			{ 
				product_category : "PRODUCT_CATEGORY",
				product_name : "PRODUCT_NAME",
				product_quantity : "PRODUCT_QUANTITY",
				product_unit_price : "PRODUCT_UNIT_PRICE",
				
			}, 
		]
		
		data.page_event = {
			purchase : true,  // 구매 확정 
			shipping_amount : "SHIPPING_AMOUNT" // (Number) 배송비
		}

		return data;
	}

 	// 검색 정보
	function getSearchData(){
		var data = {};
		data.page_type = "search";

		data.search_facet = "SEARCH_FACET",
		data.onsite_search_phrase = "ONSITE_SEARCH_PHRASE",
		data.onsite_search_result_page_type = "ONSITE_SEARCH_RESULT_PAGE_TYPE", 
		
		data.page_event = {
			onsite_search : true	// 내부 검색결과가 있는 경우 없으면 null, 
		}

		return data;
	}

	// 가입 시작 정보
	function getRegisterStartData(){
		var data = {};
		data.page_event = {
			registration_start : true, // 사용자 등록 시작 
		}
		return data;		
	}

	// 가입 완료 정보
	function getRegisterComplateData(){
		var data = {};
		data.page_event = {
			registration_complete : true, // 사용자 등록 완료

			// todo 
			email_signup_success : true // 이메일 수신 동의를 사용자 등록시에 한 경우
		}
		return data;
	}


	function getRegexTestResult( patten, str ){
		return patten.test( str );
	}

	function callTrackEvent( data ){
		trackEvent( $.extend( data,  dl ) );
	}

	function trackEvent( data ){
		debug( data );
	}

	function addEvent(){

		var endPoint = Core.getComponents('component_endpoint');
		var data = {};
		endPoint.addEvent('clickEvent', function( param ){
			debug( "clickEvent" );
			data = {};

			data.click_name = param.name;

			if( param.area == "slider"){
				data.click_area = String(data.click_name).split("_")[0];
			}else{
				data.click_area = param.area;
			}
			data.page_event = {
				link_click : true
			}
			callTrackEvent( data );
		});	
	}

	function debug( data, alert ){
		console.log( data );
		if( alert == true ){
			alert( data );
		}
	}
	Core.aa = {
		// 함수를 구분짓는것이 큰 의미는 없지만 추후 형태의 변화가 있을것을 대비해서 구분
		init : function(){
			init();
			addEvent();
		}
	}

})(Core);