(function(Core){
	'use strict';
	Core.register('module_minicart', function(sandbox){
		var args;
		var endPoint;
		var Method = {
			$that:null,
			$closeBtn:null,

			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				args = arguments[0];
				endPoint = Core.getComponents('component_endpoint');

				$this.on('click', '[data-remove-item]',  function(e){
					e.preventDefault();
					Method.removeItem( $(this).attr('href'));
				});
			},
			show:function(){
				//UIkit.offcanvas arguments type : selector:string, option:object
				UIkit.offcanvas.show('#minicart', {target:'#minicart', mode:'slide'});
			},
			hide:function(){
				//uikit 사용으로 hide는 필요없는 상황
				UIkit.offcanvas.hide('#minicart');
			},
			update:function(){
				var obj = {
					'mode':'template',
					'templatePath':'/modules/miniCart',
					'resultVar':'cart',
					'cache':new Date().getTime()
				}

				sandbox.utils.ajax('/processor/execute/cart_state', 'GET', obj, function(data){
					Method.$that.empty().append(data.responseText);
					var miniCartItem = $(args.miniCartCnt);
					var itemSize = $($(data.responseText)[0]).val();
					miniCartItem.attr('icon-text-attr', itemSize);


					if( itemSize == 0 ){
						miniCartItem.addClass('empty');
					}else{
						miniCartItem.removeClass('empty');
					}
					Method.show();
				});
			},
			removeItem:function( url ){
				// error 체크와 ajax 로딩 처리 추가 되야 함
				UIkit.modal.confirm("상품을 삭제 할까요?", function(){
					endPoint.call( 'removeFromCart', sandbox.utils.url.getQueryStringParams( url ));
					sandbox.utils.ajax(url, 'GET', {}, function(data){
						Method.update();
					});
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-minicart]',
					attrName:'data-module-minicart',
					moduleName:'module_minicart',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			show:Method.show,
			hide:Method.hide,
			update:Method.update
		}
	});
})(Core);