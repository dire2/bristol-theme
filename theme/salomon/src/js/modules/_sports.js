(function(Core){
	'use strict';
	Core.register('module_sports', function(sandbox){
		var args;
		var endPoint;
		var isAllValidate = false;
		var isInputRadioVal = false;
		var isInputTextVal = false;
		var isInputCheckVal = false;
		var Method = {
			$that:null,
			$closeBtn:null,

			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				args = arguments[0];
				endPoint = Core.getComponents('component_endpoint');
				$this.on('click', '#mediaModal',  function(e){
					e.preventDefault();
				    var $target = $('#sportsPopup').find('iframe');
				    setTimeout(function() {
			        $target.attr('src', '');
			        $('.uk-modal .loading-msg').show();
		        }, 1000);
				});
			}
		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-sports]',
					attrName:'data-module-sports',
					moduleName:'module_sports',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			show:Method.show,
			hide:Method.hide,
			update:Method.update
		}
	});
})(Core);

$('#sportsPopup').on('click', function() {
	if($(this).attr('aria-hidden') == 'false'){
		setTimeout(function() {
	    $('#sportsPopup iframe').attr('src', '');
	    $('.uk-modal .loading-msg').show();
		}, 1000);
	}
});