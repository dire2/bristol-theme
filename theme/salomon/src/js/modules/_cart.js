(function(Core){
	Core.register('module_cart', function(sandbox){
		var endPoint;
		var Method = {
			moduleInit:function(){
				// modal layer UIkit 사용
				var modal = UIkit.modal('#common-modal');
				endPoint = Core.getComponents('component_endpoint');

				//옵션 변경
				$(this).on('click', '.optchange-btn', function(e){
					e.preventDefault();

					var target = $(this).attr('href');
					var $parent = $(this).parent().parent().parent();
					var id = $parent.find('input[name=productId]').attr('value');
					var quantity = $parent.find('input[name=quantity]').attr('value');
					var url = $parent.find('input[name=producturl]').attr('value');
					var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
					var obj = {'qty':quantity, 'orderitemid':orderItemId, 'quickview':true}

					$parent.find('[data-opt]').each(function(i){
						var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
						for(var key in opt){
							obj[key] = opt[key];
						}
					});

					sandbox.utils.ajax(url, 'GET', obj, function(data){
						var domObject = $(data.responseText).find('#quickview-wrap');
						$(target).find('.contents').empty().append(domObject[0].outerHTML)
						$(target).addClass('cart_opt_chg');
						sandbox.moduleEventInjection(domObject[0].outerHTML);
						modal.show();
					});
				});


				//나중에 구매하기
				$(this).on('click', '.later-btn', function(e){
					e.preventDefault();

					$.cookie('pageMsg', $(this).attr('data-msg'));
					Method.addItem(this, {type:'later'});
				});

				//카트에 추가
				$(this).on('click', '.addcart-btn', function(e){
					e.preventDefault();

					$.cookie('pageMsg', $(this).attr('data-msg'));
					Method.addItem(this, {type:'addcart'});
				});

				//카트 삭제
				$(this).on('click', '.delete-btn .btn-delete', Method.removeItem );



				//페이지 상태 스크립트
				var pageMsg = $.cookie('pageMsg');
				if(pageMsg && pageMsg !== '' && pageMsg !== 'null'){
					$.cookie('pageMsg', null);
					UIkit.notify(pageMsg, {timeout:3000,pos:'top-center',status:'success'});
				}
			},
			addItem:function(target, opt){
				var $parent = $(target).parent().parent().parent().parent();
				var id = $parent.find('input[name=productId]').attr('value');
				var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
				var quantity = $parent.find('input[name=quantity]').attr('value');
				var sessionId = $(target).siblings().filter('input[name=csrfToken]').val()
				var obj = {'productId':id, 'orderItemId':orderItemId ,'quantity':quantity, 'csrfToken':sessionId}
				var url = $(target).parent().attr('action');
				var method = $(target).parent().attr('method');

				$parent.find('[data-opt]').each(function(i){
					var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
					for(var key in opt){
						obj['itemAttributes['+ $(this).attr('data-attribute-name') +']'] = opt[key];
					}
				});

				sandbox.utils.ajax(url, method, obj, function(data){
					var jsonData = sandbox.rtnJson(data.responseText, true) || {};
					var url = sandbox.utils.url.removeParamFromURL( sandbox.utils.url.getCurrentUrl(), $(this).attr('name') );

					if(jsonData.hasOwnProperty('error')){
						$.cookie('pageMsg', jsonData.error);
					}
					window.location.assign(url);
				});
			},
			removeItem:function(e){
				e.preventDefault();
				var url = $(this).attr('href');
				UIkit.modal.confirm('삭제하시겠습니까?', function(){
					Core.Loading.show();
					endPoint.call( 'removeFromCart', sandbox.utils.url.getQueryStringParams( url ));
					_.delay(function(){
						window.location.href = url;
					}, 1000);
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-cart]',
					attrName:'data-module-cart',
					moduleName:'module_cart',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
