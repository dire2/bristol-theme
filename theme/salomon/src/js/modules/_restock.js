(function(Core){
	'use strict';
	Core.register('module_restock', function(sandbox){
		var args;
		var endPoint;
		var isAllValidate = false;
		var isInputRadioVal = false;
		var isInputTextVal = false;
		var isInputCheckVal = false;
		var Method = {
			$that:null,
			$closeBtn:null,

			moduleInit:function(){
				var $this = $(this);
				Method.$that = $this;
				args = arguments[0];
				endPoint = Core.getComponents('component_endpoint');

				$this.on('click', '#restockAdd',  function(e){
					e.preventDefault();
					if(Method.getValidateAllChk()){
						Method.add();
					}
				});
				$this.on('click', '#btn-restock-delete',  function(e){
					e.preventDefault();
					var alarm_id = $(this).parent().parent().find("input[id='hidden-restock-id']").val();
					if(typeof(alarm_id) != "undefined" && alarm_id != "" && isNaN(alarm_id) == false){
						UIkit.modal.confirm('삭제 하시겠습니까?', function(){
							Core.Utils.ajax('/restock/remove', 'GET',{'id':alarm_id}, function(data) {
								var data = $.parseJSON( data.responseText );
								if( data.result ){
									UIkit.modal.alert( "정상적으로 삭제 되었습니다." ).on('hide.uk.modal', function() {
										location.reload();
									});
								}else{
									UIkit.notify(data.errorMsg, {timeout:3000,pos:'top-center',status:'danger'});
								}
							},true);
						});												
					}else{
						UIkit.modal.alert( "삭제 실패하였습니다. 짐시 후 다시 시도 해주세요." ).on('hide.uk.modal',null);
					}
				});
				$this.on('change', 'input',  function(e){
					Method.getValidateAllChk();
				});
				$this.on('keyup','#phone', function(e){
					Method.getValidateAllChk();
				});	
			},
			add:function(){
				
				var obj = {
						'id' : '',
						'skuId' : '',
						'messageType' : '',
						'target' : ''
					}
				
					obj.messageType = 'SMS';
					obj.skuId = $("#restockSkuId").val();
					obj.target = $("#phone").val();					
				
					Core.Utils.ajax('/restock/add', 'GET',obj, function(data) {
						var data = $.parseJSON( data.responseText );
						if( data.result ){
							$(".sms-apply").hide();
							$(".sms-complete").show();
						}else{
							UIkit.notify(data.errorMsg, {timeout:3000,pos:'top-center',status:'danger'});
						}
					},true);
			},
			getValidateChk:function(val){
				var isValidate = false;
				isValidate = $(val).parsley().validate();
				return isValidate;
			},
			getValidateAllChk:function(){
				var isColorCheck = false;
				var isSizeCheck = false;
				var isSizeEmpty = false;
				var isPhoneCheck = false;
				var isLicenseCheck = false;
				var regExp = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
				
				//상품 색상 check
				$(".restock-option-page").find("input[type='radio'][name='COLOR']").each(function(){
					if($(this).attr("checked")){
						isColorCheck = true;
					}
				});
				if(!isColorCheck){
					$(".restock-option-error-msg:first").text("옵션을 선택해 주세요");
				}else{
					$(".restock-option-error-msg:first").text("");
				}
				
				//상품 사이즈 check
				$(".restock-option-page").find("input[type='radio'][name='SIZE']").each(function(){
					if($(this).attr("checked")){
						isSizeCheck = true;
					}
				});
				
				//상품에 사이즈가 없을 경우 check
				if($(".restock-option-page").find("input[type='radio'][name='SIZE']").length == 0){  
					isSizeEmpty = true;
				}
				
				$(".restock-option-page").find("input[type='radio'][name='SIZE']").each(function(){
					if(!isSizeCheck ){
						$(".restock-option-error-msg:last").text("옵션을 선택해 주세요");
					}else{
						$(".restock-option-error-msg:last").text("");
					}
				});
				
				//휴대폰 번호 check
				if($("input[name='phone']").val() != ""){
					if(regExp.test($("input[name='phone']").val())){
						isPhoneCheck = true;
						$(".restock-sms-error-msg").text("");
					}else{
						$(".restock-sms-error-msg").text("잘못된 휴대폰 번호입니다. 올바른 휴대폰 번호를 입력해주세요.");
					}
				}else{
					$(".restock-sms-error-msg").text("휴대폰 번호를 입력해 주세요.");
				}			
				//개인정보취급방침 check
				if($("#chb-restock-agreement").attr("checked")){
					isLicenseCheck = true;
					$(".restock-agreement-error-msg").text("");
				}else{
					$(".restock-agreement-error-msg").text("개인정보취급방침을 체크해주세요.");
				}
				
				//입고알림 신청 버튼 
				if(isColorCheck && isSizeCheck && isPhoneCheck && isLicenseCheck){
					$('#restockAdd').css('background','#57b7bc');
				}else if(isColorCheck && isSizeEmpty && isPhoneCheck && isLicenseCheck){
					$('#restockAdd').css('background','#57b7bc');
				}
				
				if(isSizeCheck){
					return (isColorCheck  && isSizeCheck && isPhoneCheck && isLicenseCheck);
				}else{
					return (isColorCheck  && isSizeEmpty && isPhoneCheck && isLicenseCheck);
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-restock]',
					attrName:'data-module-restock',
					moduleName:'module_restock',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			show:Method.show,
			hide:Method.hide,
			update:Method.update
		}
	});
})(Core);