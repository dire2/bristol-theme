(function(Core){
   var Gallery = function(){

      var $this, $thumbWrap, $galleryWrap, $zoomWrap, args, currentIndex=0, arrThumbList=[], iScroll, sliderComponent, endPoint;
      var setting = {
         selector:'[data-component-gallery]',
         galleryModal:'#gallery-photo-swipe',
         galleryBtn:'[data-gallery-zoom]',
         thumbWrapper:'.product-thumb',
         galleryWrapper:'.product-gallery',
         zoomWrapper:'.pdp-gallery-fullview',
         zoomAppender:'.gallery-images',
         thumbContainer:'.thumb-wrap',
         thumbList:'.thumb-list',
         zoombtn:'.zoom-btn'
      }

      var galleryOptions = {
         history:false,
         focus:false,
         showAnimationDuration:0,
         hideAnimationDuration:0,
         index:0
      };


      var Closure = function(){}
      Closure.prototype.setting = function(){
         var opt = Array.prototype.slice.call(arguments).pop();
         $.extend(setting, opt);
         return this;
      }

      Closure.prototype.init = function(){
         var _self = this;

         args = arguments[0];
         $this = $(setting.selector);
         $thumbWrap = $this.find(setting.thumbWrapper);
         $galleryWrap = $this.find(setting.galleryWrapper);
         $zoomWrap = $this.find(setting.zoomWrapper);
         endPoint = Core.getComponents('component_endpoint');

         var arrList = [];
         $thumbWrap.find('.thumb-list').each(function(i){
            var data = Core.Utils.strToJson($(this).attr('data-thumb'), true);
            var imgUrl = $(this).find('img').attr('src').replace(/\?[a-z]+/g, '');
            var pushIS = true;

            data.thumbUrl = imgUrl;

            /* 중복 이미지 처리 */
            for(var i=0; i < arrList.length; i++){
               if(arrList[i].thumbSort === data.thumbSort && arrList[i].thumbUrl === data.thumbUrl){
                  pushIS = false;
                  return;
               }
            }

            if(pushIS){
               arrList.push(data);
               arrThumbList.push(data);
            }
         });

         $thumbWrap.on('click', 'li', function(e){
            e.preventDefault();

            var index = $(this).index();
            $(this).addClass('active').siblings().removeClass('active');
            sliderComponent.goToSlide(index);
            galleryOptions.index = index;
                endPoint.call('pdpImageClick', {index : index});
         });

         $galleryWrap.on('click', setting.zoombtn, function(){
            $('html').addClass('uk-modal-page');
            $('body').css('paddingRight', 15);
            $zoomWrap.addClass('show');
         });

         $zoomWrap.click(function(){
            if($('#quickview-wrap').length <= 0){
               $('html').removeClass('uk-modal-page');
               $('body').removeAttr('style');
            }
            $(this).removeClass('show');
         });

         $("#btn_prev_thumb").click(function(){
                var currPos = Number($("#slider_curr_pos").val());
                var currPage = Number($("#slider_curr_page").val());
                var pageCnt = Number($("#slider_page_cnt").val());
                var lastImgCnt = Number($("#slider_lastimg_cnt").val());
                var goSliderPos = currPos;
                if(currPage == 1){
                    //console.log("Warning, first page !!!");
                    return false;
                }
                if(pageCnt == 2){
                    goSliderPos=0;
                }else{
                    if(pageCnt == currPage){
                        if(lastImgCnt == 0)
                            goSliderPos = currPos - 450;
                        else
                            goSliderPos = currPos - 80*lastImgCnt;
                    }else{
                        goSliderPos = currPos - 450;
                    }
                }
                currPage--;
                $("#slider_curr_page").val(currPage);
                $("#slider_curr_pos").val(goSliderPos);
                //console.log("go this position ::: " + goSliderPos);
                $("ul.thumb-wrap").css("transform","translate(-"+goSliderPos+"px, 0px) translateZ(0px)");
                $("ul.thumb-wrap").css("transition-duration","0.5s");
                if(currPage == 1){
                    //console.log("Warning, first page !!!");
                    $("#btn_prev_thumb").attr("disabled", true);
                    $("#btn_next_thumb").attr("disabled", false);
                }
         })

         $("#btn_next_thumb").click(function(){
                var currPos = Number($("#slider_curr_pos").val());
                var currPage = Number($("#slider_curr_page").val());
                var pageCnt = Number($("#slider_page_cnt").val());
                var lastImgCnt = Number($("#slider_lastimg_cnt").val());
                var goSliderPos = currPos;
                if(currPage == pageCnt){
                    //console.log("Warning, last page !!!");
                    return false;
                }
                if((pageCnt-currPage) > 1 || ((pageCnt-currPage) == 1 && lastImgCnt == 0)){
                    goSliderPos=currPos+450;
                }
                if((pageCnt-currPage) == 1 && lastImgCnt != 0){
                    goSliderPos = currPos + 92*lastImgCnt;
                }
                currPage++;
                $("#slider_curr_page").val(currPage);
                $("#slider_curr_pos").val(goSliderPos);
                //console.log("next button, go this position ::: " + goSliderPos);
                $("ul.thumb-wrap").css("transform","translate(-"+goSliderPos+"px, 0px) translateZ(0px)");
                $("ul.thumb-wrap").css("transition-duration","0.5s");
                if(currPage == pageCnt){
                  //console.log("Warning, last page !!!");
                  $("#btn_prev_thumb").attr("disabled", false);
                  $("#btn_next_thumb").attr("disabled", true);
                }
         });

         this.setThumb(args.sort);
         return this;
      }

      Closure.prototype.setThumb = function(sort){
         var _self = this;
         var appendTxt = '';
         var count = 0;
         var sortType = sort || args.sort;
         var arrThumbData = arrThumbList.filter(function(item, index, array){
            if(item.thumbSort === sortType || item.thumbSort === 'null'){
               count++;
               return item;
            }
         });

         var thumbTemplate = Handlebars.compile($("#product-gallery-thumb").html())(arrThumbData);
         var galleryTemplate = Handlebars.compile($("#product-gallery-swipe").html())(arrThumbData);
         var zoomTemplate = Handlebars.compile($('#product-gallery-zoom').html())(arrThumbData);

         $thumbWrap.find('.thumb-list').eq(0).addClass('active');
         $galleryWrap.empty().append(galleryTemplate);
         $zoomWrap.find(setting.zoomAppender).empty().append(zoomTemplate);

         /*if(args.thumbType === 'left'){
            $thumbWrap.empty().append(thumbTemplate).css({
               'minHeight':'100%'
               //'height':($thumbWrap.find(setting.thumbList).eq(0).height() + parseInt(args.between)) * count
               //'height':(($thumbWrap.find(setting.thumbList).eq(0).height()>0 ? $thumbWrap.find(setting.thumbList).eq(0).height() : 83) + parseInt(args.between)) * count
            });
         }*/

         $thumbWrap.empty().append(thumbTemplate);

         if(args.thumbType === 'bottom'){
            $thumbWrap.find(setting.thumbContainer).css({
               'width':($thumbWrap.find(setting.thumbList).eq(0).outerWidth(true) + parseInt(args.between)) * count
            });
         }


         if(sliderComponent) sliderComponent.destroySlider();
         sliderComponent = Core.getComponents('component_slider', {context:$this, selector:'.swipe-container'}, function(){
            this.addEvent('slideAfter', function($slideElement, oldIndex, newIndex){
               $thumbWrap.find('li').eq(newIndex).addClass('active').siblings().removeClass('active');
            });

            /*this.addEvent('onInit', function(){
               if(!Core.Utils.mobileChk){
                  iScroll = new IScroll($thumbWrap[0], {
                     scrollX:(args.thumbType === 'bottom') ? true : false,
                     scrollY:(args.thumbType === 'bottom') ? false : true
                  });
               }
            });*/
            });
         if(count <= 5){
            $("#btn_prev_thumb").hide();
            $("#btn_next_thumb").hide();
         }else{

                $("#slider_curr_pos").val(0);
                $("#slider_curr_page").val(1);
                $("#btn_prev_thumb").show();
                $("#btn_next_thumb").show();
                $("#slider_curr_pos").val(0);
                $("#slider_curr_page").val(1);
            var pageCnt = Math.trunc(count / 5);
            if(count % 5 != 0){
                    $("#slider_lastimg_cnt").val(count-(pageCnt*5));
               pageCnt++;
                }
                $("#slider_page_cnt").val(pageCnt);
            }
         //console.log("Thumbnail position reload ... thumbnail count ::: " + count);
      }

      Core.Observer.applyObserver(Closure);
      return new Closure();
   }

   Core.Components['component_gallery'] = {
      constructor:Gallery,
      attrName:'data-component-gallery'
   }
})(Core);
