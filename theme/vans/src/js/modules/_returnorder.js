(function(Core){
	'use strict';
 
	Core.register('module_returnorder', function(sandbox){
	   var Method = {
		  $that:null,
		  $popModal:null,
		  $returnBtn:null,
		  $returnItemList:null,
		  $popSubmitBtn:null,
		  $beforeAddress:null,
		  $newAddress:null,
		  isPartialReturn:null,
		  deliverySearch:null,
		  isNewAddress:false,
		  $btnReturn:null,
 
		  moduleInit:function(){
			 var args = Array.prototype.slice.call(arguments).pop();
			 $.extend(Method, args);
 
			 var $this = $(this);
			 Method.$that = $this;
			 Method.$popModal = UIkit.modal("#popup-return");
			 Method.$returnBtn = $this.find('[data-return-btn]');
			 Method.$popSubmitBtn = Method.$popModal.find('[data-return-submit]');
			 Method.$popAddressModal = UIkit.modal("#popup-customer-address", {modal: false});
			 Method.$beforeAddress = Method.$popModal.dialog.find('[data-before-return-address]');
			 Method.$newAddress = Method.$popModal.dialog.find('[data-new-return-address]');
 
			 // 주문별 전체 체크시
			 $this.find('[data-return-order]').find('.input-checkbox:not(.disabled)').find('input[name="check-all"]').on('change', Method.changeAllCheck);
 
			 // 아이템 체크박스 선택시
			 $this.find('[data-return-order-list]').find('.input-checkbox:not(.disabled)').find('input[type="checkbox"]').on("change", Method.changeItemCheck );
 
			 // 반품 사유 변경시
			 $this.find('[data-return-reason-type]').on("change", function(){
				Method.updateStatus();
			 });
 
			 // 부분 반품
			 Method.$returnBtn.on("click", function(e){
				e.preventDefault();
				Method.isPartialReturn = true;
				if( !$(this).hasClass('disabled')){
				   //Method.openReturnOrderPopup( $('[data-return-order] [data-return-order-item].checked') );
				   Method.openReturnOrderPopup( $(this).closest('[data-return-order]').find('[data-return-order-item].checked') );
				}
			 });
 
			 // 주문별 전체 반품
			 $this.find('[data-return-order-btn]').on('click', function(e){
				e.preventDefault();
				Method.isPartialReturn = false;
				Method.openReturnOrderPopup( $(this).closest('[data-return-order]').find('[data-return-order-item]') );
			 });
 
 
			 // 주소 입력 처리
			 var $zipCodeInput = $(this).find('[name="addressPostalCode"]');
			 var $cityInput = $(this).find('[name="addressCity"]');
 
			 Method.deliverySearch = sandbox.getComponents('component_searchfield', {context:$this, selector:'.search-field', resultTemplate:"#address-find-list"}, function(){
				// 검색된 내용 선택시 zipcode 처리
				this.addEvent('resultSelect', function(data){
				   var zipcode = $(data).data('zip-code5');
				   var city = $(data).data('city');
				   var doro = $(data).data('doro');
 
				   var $input = this.getInputComponent().setValue( '(' + zipcode + ')' + city + doro );
 
				   $zipCodeInput.val( zipcode );
				   $cityInput.val( city.split(' ')[0] );
				});
			 });
 
 
			 // 배송지 선택 버튼
			 $this.find('[data-customer-address-btn]').on('click', function(e){
				e.preventDefault();
				Method.$popAddressModal.show();
			 });
 
			 // 배송지 선택 모듈 select 이벤트 호출( 배송지 선택했을때 호출됨 )
			 Core.getComponents('component_customer_address', {context:$this}, function(){
				this.addEvent('select', function(data){
				   Method.updateCustomerAddress( data );
				   if( Method.$popAddressModal.isActive()){
					  Method.$popAddressModal.hide();
				   }
				})
			 });
 
			 var addressList = Method.$popAddressModal.find('[data-customer-address-select-btn]');
			 // 등록되어있는 배송지가 없다면
			 // TODO customer_address compnent에 size 추가 하자
			 if( addressList.length == 0 ){
				$this.find('[data-return-address-type] a').removeClass('uk-active').eq(1).addClass('uk-active');
				Method.updateAddressInput();
				$this.find('[data-return-address-type]').hide();
				$this.find('#return-address').removeClass('uk-margin-top');
			 }else{
				// 첫번째 주소 선택
				$this.find('[data-return-address-type]').show();
				$this.find('#return-address').addClass('uk-margin-top');
				addressList.eq(0).trigger('click');
			 }
 
			 // 배송지 입력 타입 버튼 선택시
			 $this.find('[data-return-address-type]').on('show.uk.switcher', function(){
				Method.updateAddressInput();
			 });
 
			 // 수거메모 선택시
			 $this.find('[data-personal-message-select]').on('change', Method.updatePersonalMessage )
 
			 Method.$popSubmitBtn.on('click', Method.returnOrderSubmit );
 
		  },
		  updatePersonalMessage:function(e){
			 e.preventDefault();
			 var $msgContainer = Method.$popModal.dialog.find('[data-personal-message]');
			 var $personalMsg = $msgContainer.find('[name="personalMessageText"]');
 
			 var value = $(this).val();
			 if(value == ''){
				$personalMsg.val('');
				$msgContainer.addClass('uk-hidden');
			 }else if(value == 'dt_1'){
				// 직접입력일 경우
				$personalMsg.val('');
				$msgContainer.removeClass('uk-hidden');
			 }else{
				//$personalMsg.val( $(this).find("option:selected").val() + "||" + $(this).find("option:selected").text() );
				$personalMsg.val( $(this).find("option:selected").text());
				$msgContainer.addClass('uk-hidden');
			 }
		  },
		  // 반품 신청 팝업
		  openReturnOrderPopup:function( $items ){
 
			 var $modal = Method.$popModal;
 
			 var $modalForm = $modal.dialog.find('form');
			 Method.$returnItemList = $modal.dialog.find('[data-return-reason-list]>ul');
 
			 var $reasonItem = $modal.dialog.find('.uk-hidden[data-return-reason-item]');
			 var $returnAddress = $modal.dialog.find('input[name="return-address"]');
 
			 Method.$returnItemList.empty();
 
			 // 복사된 아이템이 들어가게 되는 리스트
			 //Method.$itemList = $modal.dialog.find('[data-return-reason-list]>ul');
			 //var $baseItem = $modal.dialog.find('.uk-hidden[data-return-reason-item]');
			 $items.each(function(){
				 var item = $(this),
					 orderItemId = item.find('[name="orderItemId"]').val(),
					 childItem = item.siblings('[data-parentOrderItemId="'+orderItemId+'"]');
				 childItem.length && ($items = $items.add(childItem));
			 });
 
			 $items.each( function(){
				// 아이템 dom 복사
				var $newItem = $reasonItem.clone().removeClass("uk-hidden");
 
				// 정보 수정
				var $info = $newItem.find('[return-reason-info]');
				var $thumb = $newItem.find('[return-reason-thumb]');
				var parentOrderItemId = $(this).data('parentorderitemid');
				if (parentOrderItemId){
					$newItem.attr('data-parentOrderItemId', parentOrderItemId);
						 $newItem.find('[return-reason-partials-quantity] .select-box').hide().after('<span data-returnQuantity>1</span>');
				}
				$info.empty();
				$thumb.empty();
 
				$(this).find('input[name="orderItemId"]').clone().appendTo( $info );
				$(this).find('[name="orderId"]').clone().appendTo( $info );
				$(this).find('[name="orderItemSize"]').clone().appendTo( $info );
				$(this).find('[name="ableEntireReturn"]').clone().appendTo( $info );
				$(this).find('[name="returnableQuantity"]').clone().appendTo( $info );
				$(this).find('[name="returnedQuantity"]').clone().appendTo( $info );
 
 
				$(this).find('.info-wrap').clone().appendTo( $info );
				$(this).find('.img-wrap').clone().appendTo( $thumb );
				// 반품 가능 수량
				var returnableQuantity = $(this).find('input[name="returnableQuantity"]').val();
				// 반품 된 수량
				var returnedQuantity = $(this).find('input[name="returnedQuantity"]').val();
 
				// 복사된 정보중 수량은 삭제
				$info.find('.opt.quantity').remove();
 
				if( Method.isPartialReturn ){
				   var $quantity = $newItem.find('[return-reason-partials-quantity]');
				   for( var i=1; i<=returnableQuantity; i++){
					  $quantity.find("select").removeAttr('disabled').append( '<option value="' + i + '">' + i +'</option>');
				   }
				}else{
				   var $quantity = $newItem.find('[return-reason-order-quantity]');
				   $quantity.find('input[name="quantity"]').removeAttr('disabled').val(returnableQuantity);
				}
 
				$quantity.removeClass('uk-hidden').find('[data-quantity]').text( returnableQuantity );
				$newItem.appendTo( Method.$returnItemList );
 
				Core.moduleEventInjection(Method.$returnItemList.html());
			 })
 
			 Method.$returnItemList.find('select').on("change", Method.updateStatus );
 
			 Method.updatePaymentInfo( false );
			 Method.updateSubmitBtn( false );
			 Method.updateStatus();
 
			 $modal.show();
			 sandbox.validation.reset( $modal.dialog.find('form') );
		  },
 
 
		  // 주문별 전체 체크 처리
		  changeAllCheck:function(e){
			 e.preventDefault();
			 var isCheck = $(this).prop('checked');
			 var $itemList = Method.getOrderElementByChecked( $(this) ).itemList;
 
			 $itemList.find('li').each( function(){
				$(this).find('input[type="checkbox"]').prop( 'checked', isCheck ).trigger('change');
			 });
 
			 Method.updateRetunBtnStatus();
		  },
 
		  // 아이템 체크박스 변경시 전체 선택 체크박스 상태처리
		  updateCheckAll:function($checkbox){
			 var $orderInfo = Method.getOrderElementByChecked( $checkbox );
			 var $itemList = $orderInfo.itemList;
			 var $allCheck = $orderInfo.allCheckbox;
 
			 if( $itemList.find('li').not('[data-parentOrderItemId]').length == $itemList.find('li').find('input[type="checkbox"]:checked').length ){
				$allCheck.prop( 'checked', true );
			 }else{
				$allCheck.prop( 'checked', false );
			 }
		  },
 
		  // 체크 여부에 따른 리턴 버튼 활성화 처리
		  updateRetunBtnStatus:function(){
			 var isChecked =  Method.$btnReturn.closest('[data-return-order]').find('[data-return-order-list]').find('input[type="checkbox"]').is(':checked');
			 
			 if( isChecked ){
                //Method.$returnBtn.removeAttr('disabled').removeClass('disabled');
                //Method.$returnBtn.text('부분 반품하기');
				Method.$btnReturn.show();
			 }else{
                //Method.$returnBtn.attr('disabled','true').addClass('disabled');
                //Method.$returnBtn.text('부분 반품할 상품을 선택해주세요');
				Method.$btnReturn.hide();
			 }
		  },
 
		  // 취소 가격 및 추가 정보 입력 여부 처리
		  updateStatus:function(){
			 //console.log('updateStatus');
 
			 if (this.value){
			 var item = $(this).closest('[data-return-reason-item]'),
				 itemOrderId = item.find('[name="orderItemId"]').val(),
				 childItem = item.siblings('[data-parentorderitemid="'+itemOrderId+'"]');
				 if (childItem.length) {
					 childItem.find('[name="quantity"]').val(this.value);
					 childItem.find('[data-returnquantity]').html(this.value);
				 }
			 }
 
			 var $modal = Method.$popModal;
			 var $form = $modal.dialog.find('form');
			 var action = $form.attr('action') + '/calculator';
			 var param = $form.serialize();
 
 
 
			 if( Method.isPartialReturn ){
				if( !Method.getIsAvailablePartialReturn()){
				   UIkit.modal.alert('전체 반품을 원하실 경우 전체반품 버튼을 통해 진행해 주시기 바랍니다.');
				   Method.$returnItemList.find('select').each( function(){
					  $(this).val(1).trigger('update');
					  //$(this).find('option:eq(0)').attr('selected', 'selected');
				   });
				   return;
				}
			 }else{
				param += '&entireReturn=true';
			 }
 
			 //console.log(param);
 
			 Core.Utils.ajax( action, 'POST', param, function(data){
				var data = sandbox.rtnJson(data.responseText, true);
				var $paymentList = $modal.find('[data-payment-list]');
				var $paymentItem = $modal.find('.uk-hidden[data-payment-item]');
				$paymentList.empty();
 
				if( !data ){
				   var $newItem = Method.getReplacePaymentItem( $paymentItem, '서버 통신 오류' );
				   $newItem.appendTo( $paymentList );
				   //UIkit.notify( 'PAYMENTS 정보 오류' , {timeout:3000,pos:'top-center',status:'danger'});
				   return;
				}
 
				var result = data['result'];
 
				if( result == true ){
				   //console.log( data );
 
				   var parentFgChargeFeeTotal = data['ro']['parentFgChargeFeeTotal'];
				   var refundAmountTotal = data['ro']['refundAmountTotal'];
				   var refundPayments = data['ro']['refundPayments'];
				   var returnFgChargeFeeTotal = data['ro']['returnFgChargeFeeTotal'];
 
				   /*
				   for( var str in data['ro']){
					  //console.log( String(str ));
				   }
				   refundAmountTotal 총 환불 예정 금액
				   returnFgChargeFeeTotal 반품 배송비
				   parentFgChargeFeeTotal 추가배송비
				   refundPayments 결제수단별 환불금액
				   returnFgChargeFeeTotal
				   */
 
				   // 결제 정보
				   if( refundPayments != null ){
					  $.each( refundPayments, function( index, payment ){
						 var type = _GLOBAL.PAYMENT_TYPE[ payment.paymentType.type ] + '(원결제금액: ' + sandbox.utils.price(payment.orgPaymentAmount.amount) + ')'
						 var $newItem = Method.getReplacePaymentItem( $paymentItem, type, payment.totalAmount.amount );
						 $newItem.appendTo( $paymentList );
					  });
 
					  // 반품 배송비
					  if( returnFgChargeFeeTotal != null && returnFgChargeFeeTotal.amount > 0 ){
						 var $newItem = Method.getReplacePaymentItem( $paymentItem, "반품 배송비 : ", returnFgChargeFeeTotal.amount );
						 $newItem.appendTo( $paymentList );
					  }
					  // 추가배송비
					  if( parentFgChargeFeeTotal != null && parentFgChargeFeeTotal.amount > 0 ){
						 var $newItem = Method.getReplacePaymentItem( $paymentItem, "추가 배송비 : ", parentFgChargeFeeTotal.amount );
						 $newItem.appendTo( $paymentList );
					  }
 
					  //총 환불 예정 금액
					   if( refundAmountTotal != null ){
						 var $newItem = Method.getReplacePaymentItem( $paymentItem, "총 환불 예정 금액 : ", refundAmountTotal.amount );
						 $newItem.appendTo( $paymentList );
						  var totalAmount =  refundAmountTotal.amount;
								 if(totalAmount < 0){
									 UIkit.modal.alert('환불 가능 금액이 없어 반품 신청이 불가능합니다. 자세한 사항은 고객센터로 문의 바랍니다.');
									 $('.button.width-max').hide();
						  }else{
							  $('.button.width-max').show();
						  }
					  }
 
					  Method.updateSubmitBtn( true );
				   }else{
					  var $newItem = Method.getReplacePaymentItem( $paymentItem, 'PAYMENTS 정보 오류' );
					  $newItem.appendTo( $paymentList );
				   }
				}else{
				   //UIkit.notify( data.errorMsg , {timeout:3000,pos:'top-center',status:'danger'});
				   var $newItem = Method.getReplacePaymentItem( $paymentItem, data.errorMsg );
				   $newItem.appendTo( $paymentList );
				}
				// 가격정보에 오류가 있어도 정보를 노출해야하기 때문에 true
				Method.updatePaymentInfo( true );
				//Method.updateSubmitBtn( true );
			 },true);
 
 
		  },
 
		  // 계산 결과 dom 생성
		  getReplacePaymentItem:function( $base, type, amount ){
			 var $newItem = $base.clone().removeClass("uk-hidden");
			 $newItem.find('[data-type]').text( type );
			 if( amount ){
				$newItem.find('[data-amount]').text( sandbox.utils.price(amount) );
			 }else{
				$newItem.find('[data-amount]').text( sandbox.utils.price(0) );
			 }
			 return $newItem;
		  },
 
		  // 아이템 체크박스 선택시
		  changeItemCheck:function(e){
			 var isCheck = $(this).prop('checked');
			 Method.$btnReturn = $(this).closest('.item-info').siblings().children().find('[data-return-btn]');
			 if( isCheck ){
				$(this).closest('[data-return-order-item]').addClass('checked');
			 }else{
				$(this).closest('[data-return-order-item]').removeClass('checked');
			 }
			 Method.updateCheckAll($(this));
			 Method.updateRetunBtnStatus();
		  },
 
		  // 선택된 아이템의 order dom
		  getOrderElementByChecked:function($checkbox){
			 var $order = $checkbox.closest('[data-return-order]');
			 return {
				order : $order,
				allCheckbox : $order.find('input[name="check-all"]'),
				itemList : $order.find('[data-return-order-list]')
			 }
		  },
 
		  // 리턴 상황에 따른 가격정보와
		  updatePaymentInfo:function( $bool ){
			 var $paymentContainer = Method.$popModal.find('[data-payment-conatiner]');
 
			 if( $bool ){
				$paymentContainer.show();
			 }else{
				$paymentContainer.hide();
			 }
		  },
 
		  // 버튼 활성화/비활성화 처리
		  updateSubmitBtn:function( $bool ){
			 if( $bool ){
				Method.$popSubmitBtn.removeAttr('disabled').removeClass('disabled');
			 }else{
				Method.$popSubmitBtn.attr('disabled','true').addClass('disabled');
			 }
		  },
 
		  // 배송지 선택으로 주소 입력시
		  updateCustomerAddress:function( data ){
			 var $target = Method.$popModal.dialog.find('[data-before-return-address]');
			 if( $target.find('[data-user-name]').length > 0 ){
				$target.find('[data-user-name]').html($.trim(data.fullName));
			 }
 
			 if( $target.find('[data-phone]').length > 0 ){
				$target.find('[data-phone]').html($.trim(data.phoneNumber));
			 }
 
			 if( $target.find('[data-postalCode]').length > 0 ){
				$target.find('[data-postalCode]').html($.trim(data.postalCode));
			 }
 
			 if( $target.find('[data-address1]').length > 0 ){
				$target.find('[data-address1]').html($.trim(data.addressLine1));
			 }
 
			 if( $target.find('[data-address2]').length > 0 ){
				$target.find('[data-address2]').html($.trim(data.addressLine2));
			 }
 
			 // 변경된 값 input 에 적용
			 $target.find('input[name="addressFirstName"]').val($.trim(data.fullName));
			 $target.find('input[name="addressPhone"]').val($.trim(data.phoneNumber));
			 $target.find('input[name="addressLine1"]').val($.trim(data.addressLine1));
			 $target.find('input[name="addressLine2"]').val($.trim(data.addressLine2));
			 $target.find('input[name="addressPostalCode"]').val($.trim(data.postalCode));
			 $target.find('input[name="addressCity"]').val($.trim(data.city));
		  },
 
		  // 실제 전송될 주소 정보를 설정하기 위해 불필요 정보 disabled
		  updateAddressInput:function(){
			 if( Method.$beforeAddress.hasClass('uk-active')){
				Method.isNewAddress = false;
				Method.$beforeAddress.find('input').attr('disabled', false );
				Method.$newAddress.find('input').attr('disabled', true );
			 }else{
				Method.isNewAddress = true;
				Method.$beforeAddress.find('input').attr('disabled', true );
				Method.$newAddress.find('input').attr('disabled', false );
			 }
		  },
 
		  returnOrderSubmit:function(e){
			 e.preventDefault();
 
			 var $modalForm = Method.$popModal.dialog.find('form');
			 sandbox.validation.validate( $modalForm );
 
			 if( sandbox.validation.isValid( $modalForm )){
 
				if( Method.isNewAddress ){
				   if( !Method.deliverySearch.getValidateChk() ){
					  UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
					  return false;
				   }
				}
 
				//console.log( $modalForm.attr('action'))
				UIkit.modal.confirm('반품 하시겠습니까?', function(){
				   // 주소에 노출된 우편번호 제거
				   if( Method.isNewAddress ){
					  var $addressLine1 = $modalForm.find('[name="addressLine1"]:visible');
					  if( $addressLine1 ){
						 var address1 = $addressLine1.val().split(')');
						 if( address1.length > 1 ){
							$addressLine1.val( $.trim( address1[1]) );
						 }
					  }
				   }
 
				   var param = $modalForm.serialize();
				   if( !Method.isPartialReturn ){
					  param += '&entireReturn=true';
				   }
 
				   Method.updateSubmitBtn( false );
 
				   //console.log( param );
				   Core.Utils.ajax( $modalForm.attr('action'), 'POST', param, function(data){
					  var data = sandbox.rtnJson(data.responseText, true);
					  var result = data['result'];
					  if( result == true ){
						 if( _GLOBAL.MARKETING_DATA().useGa == true ){
							var marketingOption = {
							   orderType : 'RETURN',
							   orderId : data.ro.returnOrderId
							};
							Core.ga.processor( marketingOption );
						 }
						 UIkit.modal.alert("반품이 완료 되었습니다.").on('hide.uk.modal', function() {
							window.location.href = "/account/orders/returned";
						 });
					  }else{
						 UIkit.modal.alert(data['errorMsg']).on('hide.uk.modal', function() {
							window.location.reload();
						 });
					  }
					  //$('.customer-contents').replaceWith($(data.responseText).find('.customer-contents'));
 
				   },true)
 
				}, function(){},
				{
				   labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			 }
 
		  },
 
		  // 부분반품 가능 여부 판단
		  getIsAvailablePartialReturn:function(){
 
			 var itemList = {};
			 Method.$returnItemList.find('li').each( function(){
				isAvailable = false;
				var orderItemSize = $(this).find('input[name="orderItemSize"]').val();
				var ableEntireReturn = $(this).find('input[name="ableEntireReturn"]').val();
				var returnableQuantity = $(this).find('input[name="returnableQuantity"]').val();
				var returnedQuantity = $(this).find('input[name="returnedQuantity"]').val();
				var orderId = $(this).find('input[name="orderId"]').val();
				var quantity = $(this).find('[name="quantity"]:not(:disabled)').val();
 
				if( itemList[orderId] == undefined ){
				   itemList[orderId] = {};
				   itemList[orderId].ableEntireReturn = ableEntireReturn;
				   itemList[orderId].orderItemSize = orderItemSize;
				   itemList[orderId].items = [];
				}
 
				var itemObj = {
				   returnedQuantity : Number(returnedQuantity),
				   returnableQuantity : Number(returnableQuantity),
				   quantity : Number(quantity)
				}
 
				itemList[orderId].items.push( itemObj );
			 })
 
			 var isAvailable = false;
			 $.each( itemList, function(){
				// 전체 반품이 불가능하거나 현재 주문의 전체 아이템 사이즈와 선택된 아이템 사이즈가 같지 않다면
				if( this.ableEntireReturn == "false" || this.orderItemSize != this.items.length ){
				   isAvailable = true;
				}else{
				   $.each( this.items, function(){
					  //console.log( this.returnableQuantity + ' : ' + this.quantity);
					  if( this.returnedQuantity != 0 || this.returnableQuantity != this.quantity ){
						 //console.log('반품된 수량이 있거나 전체 수량이 아닌것 있음');
						 isAvailable = true;
						 return;
					  }
				   })
				}
 
				if( isAvailable == false ){
				   //console.log('부분반품 불가능한 order가 있음');
				   return false;
				}
			 })
			 return isAvailable;
		  }
	   }
 
	   return {
		  init:function(){
			 sandbox.uiInit({
				selector:'[data-module-returnorder]',
				attrName:'data-module-returnorder',
				moduleName:'module_returnorder',
				handler:{context:this, method:Method.moduleInit}
			 });
		  }
	   }
	});
 })(Core);
