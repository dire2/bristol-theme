(function(Core){
	Core.register('module_cart', function(sandbox){
		var endPoint;
		var Method = {
			moduleInit:function(){
				// modal layer UIkit 사용
				var modal = UIkit.modal('#common-modal');
				var cartCustomProduct = $("#cartCustomProduct").val();
				endPoint = Core.getComponents('component_endpoint');
				
				if(cartCustomProduct == 'true'){
					var customGuide = "<div style='text-align: center;'>※※※ 커스텀 슈즈 주문 유의사항 ※※※  </div><br/><br/><배송 안내>" +
							"<br/> 주문일로부터 3-5주 소요  <br/><br/><교환 및 반품 안내><br/> " +
							"①  결제 완료 후에는 주문 취소 및 주문시 입력한 정보 (디자인 수정, 사이즈 변경 등) 변경이 불가합니다.<br/> " +
							"②  직접 디자인하신 정보를 바탕으로 해외 주문/제작이 이루어지므로,<br/> 단순 변심 및 사이즈 교환 등의 사유로 반품하실 수 없습니다.<br/> " +
							"③ 제품 하자(불량, 제품손상 등)이 발생하였을 경우에만 고객센터 문의를 통해 환불 절차가 진행됩니다. <br/> " +
							"④ 타 정상제품과 묶음 주문 및 배송 불가하며, 커스텀 상품은 배송비 1만원이 부과 됩니다. <br/><br/> <이미지 및 텍스트 심사 안내> <br/> " +
							"① 이미지 또는 텍스트를 추가한 경우에는,컨텐츠 적합성 심사 후에 제작 가능여부를 안내드립니다.<br/> " +
							"② 부적합한 경우에는 자동 주문취소 및 환불처리됩니다. <br/><br/>" +
							"<고객센터><br/> 1522-1882";
				    UIkit.modal.alert(customGuide);
				}

				//옵션 변경
				$(this).on('click', '.optchange-btn', function(e){
					e.preventDefault();

					var target = $(this).attr('href');
					var $parent = $(this).closest('.product-opt_cart');
					var id = $parent.find('input[name=productId]').attr('value');
					var quantity = $parent.find('input[name=quantity]').attr('value');
					var url = $parent.find('input[name=producturl]').attr('value');
					var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
					var obj = {'qty':quantity, 'orderitemid':orderItemId, 'quickview':true}

					$parent.find('[data-opt]').each(function(i){
						var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
						for(var key in opt){
							obj[key] = opt[key];
						}
					});

					sandbox.utils.ajax(url, 'GET', obj, function(data){
						var domObject = $(data.responseText).find('#quickview-wrap');
						$(target).find('.contents').empty().append(domObject[0].outerHTML)
						$(target).addClass('quickview');
						sandbox.moduleEventInjection(domObject[0].outerHTML);
						modal.show();
					});
				});


				//나중에 구매하기
				$(this).on('click', '.later-btn', function(e){
					e.preventDefault();

					$.cookie('pageMsg', $(this).attr('data-msg'));
					Method.addItem.call(this, {type:'later'});
				});

				//카트에 추가
				$(this).on('click', '.addcart-btn', function(e){
					e.preventDefault();

					$.cookie('pageMsg', $(this).attr('data-msg'));
					Method.addItem.call(this, {type:'addcart'});
				});

				//카트 삭제
				$(this).on('click', '.delete-btn .btn-delete', Method.removeItem );



				//페이지 상태 스크립트
				var pageMsg = $.cookie('pageMsg');
				if(pageMsg && pageMsg !== '' && pageMsg !== 'null'){
					$.cookie('pageMsg', null);
					UIkit.notify(pageMsg, {timeout:3000,pos:'top-center',status:'success'});
				}
			},
			addItem:function(opt){
				var $parent = $(this).closest('.product-opt_cart');
				var id = $parent.find('input[name=productId]').attr('value');
				var orderItemId = $parent.find('input[name=orderItemId]').attr('value');
				var quantity = $parent.find('input[name=quantity]').attr('value');
				var sessionId = $(this).siblings().filter('input[name=csrfToken]').val()
				var obj = {'productId':id, 'orderItemId':orderItemId ,'quantity':quantity, 'csrfToken':sessionId}
				var url = $(this).closest('form').attr('action');
				var method = $(this).closest('form').attr('method');

				$parent.find('[data-opt]').each(function(i){
					var opt = sandbox.rtnJson($(this).attr('data-opt'), true);
					for(var key in opt){
						obj['itemAttributes['+ $(this).attr('data-attribute-name') +']'] = opt[key];
					}
				});

				sandbox.utils.ajax(url, method, obj, function(data){
					var jsonData = sandbox.rtnJson(data.responseText, true) || {};
					var url = sandbox.utils.url.removeParamFromURL( sandbox.utils.url.getCurrentUrl(), $(this).attr('name') );

					if(jsonData.hasOwnProperty('error')){
						$.cookie('pageMsg', jsonData.error);
					}
					window.location.assign(url);
				});
			},
			removeItem:function(e){
				e.preventDefault();
				var url = $(this).attr('href');
				UIkit.modal.confirm('삭제하시겠습니까?', function(){
					Core.Loading.show();
					endPoint.call( 'removeFromCart', sandbox.utils.url.getQueryStringParams( url ));
					_.delay(function(){
						window.location.href = url;
					}, 1000);
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-cart]',
					attrName:'data-module-cart',
					moduleName:'module_cart',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);

