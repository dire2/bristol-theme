(function(Core){
	Core.register('module_customproduct', function(sandbox){
		var $that;
		// var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];
		var Method = {
			moduleInit:function(){
				$this = $(this);
				var args = arguments[0];

				$this.find('.custom-top-banner > .scroll > a').click(function(e){
					e.preventDefault();
					var selector = $(this).attr('href').replace('#', '');
					var scrollTop = $this.find('[name='+selector+']').offset().top;


					$("html, body").animate({scrollTop:scrollTop}, 500);
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-customproduct]',
					attrName:'data-module-customproduct',
					moduleName:'module_customproduct',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);