(function(Core){
	Core.register('module_order_payment', function(sandbox){
		var Method = {
			$that:null,
			$usafeContent:null,
			$agreeForm:null,
			moduleInit:function(){
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);
				var $this = $(this);
				
				// 커스텀 상품일경우 무통장 버튼 비노출
				var isCustomProduct = $("#paymentCustom").val();
				var $obj = $('.payment-method-item');
			
                $obj.each(function(){
                    if($(this).attr('data-method') == 'vbank' && isCustomProduct == 'true'){
                        $(this).find('h6').hide();
                    }
                });
				Method.$that = $this;

				$this.find('[data-checkout-btn]').on("click", Method.checkout );
				$this.find('[data-payment-method]').find('.payment-method-item-title').on('click', Method.changePaymentMethod);

				/*
				Method.$agreeForm = $this.find('form[name="checkout-agree-form"]');
				sandbox.validation.init( Method.$agreeForm );
				*/

				Method.$usafeContent = $this.find('[data-usafe-content]');

				sandbox.getComponents('component_radio', {context:$this}, function(i){
					this.addEvent('change', function(target, val){
						if( $(this).attr('name') == 'usafeIsAgree'){
							Method.toggleUsafeContent( val == "true" );
						}
					});
				});
			},

			toggleUsafeContent:function( $bool ){
				if( $bool ){
					Method.$usafeContent.show();
				}else{
					Method.$usafeContent.hide();
				}
			},

			// payment 정보 선택시
			changePaymentMethod:function(e){
				e.preventDefault();
				var $item = $(this).closest('.payment-method-item');
			    if($($item).attr('data-pausable') == 'true'){    
					if(!$item.hasClass('active')){
						$item.siblings().removeClass('active');
						$item.addClass('active');
						$('[name="paymentButton"]').css('background','#cccccc').css({'cursor':'default'});
					}
			    }else{
		    		$item.siblings().removeClass('active');
					$item.addClass('active');
					$('[name="paymentButton"]').css('background-color','').css({'cursor':''});
					$('[name="paymentButton"]').removeClass('button-color');
			    }
			},
			checkout:function(e){
				e.preventDefault();
				var isCheckoutAgree = Method.$that.find('[name="isCheckoutAgree"]').is(':checked');
				var _self = $(this);
				/*
				sandbox.validation.validate( Method.$agreeForm );
				if(sandbox.validation.isValid( Method.$agreeForm )){

				}
				*/


				// 결제 방법에 따른 처리
				var $activeItem = Method.$that.find("[data-payment-method]").find(".payment-method-item.active");
				// 무통장일때
				if( $activeItem.data("type") == 'WIRE' ){
					var $form = $activeItem.find('form[name="checkout-useInsureGarantee-form"]');

					// 보증보험 사용할 때
					if( $form.length > 0 ){
						var usafeIsAgree = $activeItem.find('[name="usafeIsAgree"]:checked').val();

						if( usafeIsAgree == 'true'){
							sandbox.validation.init( $form );
							sandbox.validation.reset( $form );
							sandbox.validation.validate( $form );

							// 모두 선택 체크하고
							if( sandbox.validation.isValid( $form )){
								// 동의 여부 체크
								if( $activeItem.find('[name="usafeInfoAgree"]:checked').val() == 'false'){
									UIkit.modal.alert("개인정보 이용동의에 동의해주세요");
									return;
								}
							}else{
								return;
							}
						}
					}
				}

				if( !isCheckoutAgree ){
					UIkit.modal.alert("상품, 가격, 할인, 배송정보에 동의해주세요");
					return;
				}
				
				
				if( _self.data('checkout-btn') == 'complete'){
					_self.closest('form').submit();
					return;
				}

				//iamport 모듈
				if( $activeItem.data('provider') == 'IAMPORT' && $activeItem.data('pausable') == false){
					BLC.ajax({
						url : '/cart/check',
						type : 'POST',
						dataType:'json',
						error : function (checkError) {
							if (checkError.status == 404) { //배포전
								Method.checkoutIamport( $activeItem );
							} else {
								if (confirm('장바구니 정보를 찾을 수 없어 메인 페이지로 이동합니다.')) {
								}
								location.href = '/';
							}
						}
					}, function(checkResult) {
						if (checkResult['resultCode'] == '000') {
							Method.checkoutIamport( $activeItem );
						} else if (checkResult['resultCode'] == '901') {
                            location.href = '/checkout?edit-shipping=true';
						} else if (checkResult['resultCode'] == '999'){
							if (confirm('장바구니에 오류가 발생하여 주문이 완료되지 않았습니다. 장바구니를 비우고 다시 담아 주문 부탁 드립니다')) {
							}
							location.href = '/cart';                            
						} else {
							if (confirm('장바구니 정보를 찾을 수 없어 메인 페이지로 이동합니다.')) {
							}
							location.href = '/';
						}
					});
				}
			},
			checkoutIamport:function( $activeItem ){
				// 결제 전일때
				var $orderinfo = $("#orderinfo-review");
				var $shippingInfo = $("#shipping-review");
				var $priceInfo = $("#order-summary");


				var IMP = window.IMP;
				//var in_app = $(frm.in_app).is(':checked');

				//IMP.init('imp29019801');
				//결제 처리 전에 이미 전달해놓은 상품가격 값과 비교해야함
				//$paymentInfo.find("input[name='cartId']").val()

				var paymentMethod = $activeItem.data("method"); // 결제 수단
				var mRedirectUrl = $activeItem.data("m-redirect-url"); // 모바일 url
				var version = $activeItem.data("version") || 'old'; // 에스크로 분기 처리를 위한 값  new or old

				var useReplaceReturnUrl = false;
				var cUrl = Core.Utils.url.getCurrentUrl();

				// 접근한 URL이 mshop 일 때
				if( cUrl.indexOf( 'mshop' ) > -1){
					useReplaceReturnUrl = true;
				}else{
					// 접근한 URL이 mshop 이 아닌데 deviceOs 가 ios 일때
					if( String(Core.Utils.url.getQueryStringParams(cUrl).deviceOs ).toLowerCase() == 'ios' ){
						useReplaceReturnUrl = true;
					}
				}

				if( useReplaceReturnUrl ){
					if( mRedirectUrl != null ){
						mRedirectUrl = mRedirectUrl.replace('shop', 'mshop');
					}
				}

				var appScheme = $activeItem.data("app-scheme"); // 모바일 앱 스키마 정보
				var identityCode = $activeItem.data("identity-code"); // iamport key
				var pg = $activeItem.data("pg");

				if( paymentMethod == '' || identityCode == '' || pg == ''){
					UIkit.modal.alert('결제 수단 정보로 인한 문제가 발생하였습니다.<br/>고객센터('+_GLOBAL.SITE.TEL+ ')로 문의 주시면 신속히 처리 도와드리겠습니다.');
					return;
				}

				IMP.init(identityCode);

				var $orderList = $priceInfo.find('[data-order]');
				var name = $orderList.eq(0).find('[data-name]').text();
				if( $orderList.length > 1 ){
					name += '외' + $orderList.length-1;
				}
				var buyerName = $.trim($orderinfo.find('[data-name]').data('name')) || $shippingInfo.find('[data-name]').data('name');

		    	var param = {
					//pay_method : _GLOBAL.PAYMENT_TYPE_BY_IAMPORT[ paymentMethod ], // 추후 provider 에 따라 변수변경 *서버에서 내려오고 있음
					pg : pg,
					pay_method : paymentMethod, // 추후 provider 에 따라 변수변경
					merchant_uid : Method.$that.find("input[name='cartId']").val() + '_' + new Date().getTime(),
					name: name,
					amount:$priceInfo.find('[data-amount]').data('amount'),
					buyer_email:$orderinfo.find('[data-email]').data('email'),
					//buyer_name:$orderinfo.find('[data-email]').data('email'),
					buyer_name:buyerName,
					buyer_tel:$shippingInfo.find('[data-phone]').data('phone'),
					buyer_addr:$shippingInfo.find('[data-address]').data('address'),
					buyer_postcode:$shippingInfo.find('[data-zipcode]').data('zipcode'),
					m_redirect_url:mRedirectUrl,
					app_scheme:appScheme
				};

				var depositPeriod = $activeItem.find('[name="depositPeriod"]').val() || 2;

				if( paymentMethod == 'vbank' ) {
					param.vbank_due = moment().add(depositPeriod, 'day').format('YYYYMMDD2359');
					param.custom_data = $activeItem.find('form').serialize().replace(/=/gi, ':').replace(/&/gi, '|');
				}


				if( paymentMethod == 'escrow') {
					if( version == 'new'){
						// 신 버전
					    param.pay_method='vbank';
						param.escrow = true;
					}else{
						// 기존 버전
						param.vbank_due = moment().add(depositPeriod, 'day').format('YYYYMMDD2359');
						param.custom_data = 'paymethod:escrow';
						param.escrow = false;
					}
				}

				IMP.request_pay(param, function(rsp) {
					//결제 완료시
					if ( rsp.success ) {
						var msg = '결제가 완료되었습니다.<br>';
						msg += '고유ID : ' + rsp.imp_uid + '<br>';
						msg += '상점 거래ID : ' + rsp.merchant_uid + '<br>';
						msg += '결제 금액 : ' + rsp.paid_amount + '<br>';
						msg += 'custom_data : ' + rsp.custom_data + '<br>';

						if ( rsp.pay_method === 'card' ) {
							msg += '카드 승인번호 : ' + rsp.apply_num + '<br>';
						} else if ( rsp.pay_method === 'vbank' ) {
							msg += '가상계좌 번호 : ' + rsp.vbank_num + '<br>';
							msg += '가상계좌 은행 : ' + rsp.vbank_name + '<br>';
							msg += '가상계좌 예금주 : ' + rsp.vbank_holder + '<br>';
							msg += '가상계좌 입금기한 : ' + rsp.vbank_date + '<br>';
						}
						//alert( msg );
						location.href = '/iamport-checkout/hosted/return?imp_uid=' + rsp.imp_uid + '&pay_method=' + rsp.pay_method + '&custom_data=' + rsp.custom_data;
					} else {

						//실패 메시지에 따라 그냥 넘길것인지 어떤 액션을 취할것인지 확인
						var msg = '결제에 실패하였습니다.' + '<br>';
						msg += '에러내용 : ' + rsp.error_msg + '<br>';
						//alert( msg );
					}

				});
			}

		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-payment]',
					attrName:'data-module-order-payment',
					moduleName:'module_order_payment',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);