(function(Core){
	var addOnProduct = function(){
		'use strict';

		var $this, $addonListWrap, $msg, args, selectComponent = null, isRequired=false, forceDependent=false, isValidate=false, isFireEvent = true, isEquivalent;
		var objChildItem={}; //single 만 됨
		var setting = {
			selector:'[data-component-addon-product-option]',
			selectbox:'select',
			optionTemplate:'#addon-sku-option',
			listTemplate:'#addon-sku-list',
			resultWrap:'.addon-list-wrap'
		}

		var addChildItem = function(key, skuData, requestChildItem){
			/*
				원상품 : 애드온 상품
					x_FORCED
					NONE
					1 : 1	EQUIVALENT			quantityComponent 추가 (수량체크는 원상품의 주문 수량만큼 추가가능) 인데 일단 수량 컴포넌트 삭제
					1 : x	PROPORTION			quantityComponent 추가 (relativeQuantity 값 만큼 추가가능)
					x : 1	PROPORTION_REV
			*/
			var appendTxt = '';
			if($(setting.listTemplate).length <= 0){
				throw new Error(setting.listTemplate + ' template is not defined');
				return;
			}

			if(args['data-component-addon-product-option'].addOnListType === 'single'){
				objChildItem = {};
				$addonListWrap.empty();
			}

			if(!objChildItem.hasOwnProperty(key)){
				skuData.isSubmit = args['data-component-addon-product-option'].isSubmit;
				skuData.privateId = key;
				skuData.relativeQuantity = requestChildItem.quantity;
				appendTxt = Handlebars.compile($(setting.listTemplate).html())(skuData);
				objChildItem[key] = requestChildItem;
				$addonListWrap.append(appendTxt);

				if(skuData.isQuantity){
					Core.getComponents('component_quantity', {context:$addonListWrap}, function(){
						this.addEvent('change', function(qty){
							objChildItem[key].quantity = qty;
						});
					});
				}
			}

			isValidate = true;
			if($msg.length > 0){
				$msg.text('');
			}
		}

		var addMessage = function(){
			if($msg.length > 0){
				$msg.text('상품을 선택해 주세요');
			}else{
				UIkit.notify('상품을 선택해 주세요', {timeout:3000,pos:'top-center',status:''});
			}
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				args = arguments[0];

				//에드온의 템플릿이 없으면 에러 throw
				if($(setting.optionTemplate).lenght <= 0){
					throw new Error('#addon-sku-option template is not defined');
					return;
				}

				//sku 옵션생성
				$this = $(setting.selector);
				$addonListWrap = $this.find(setting.resultWrap);
				$msg = $this.siblings().filter('.tit').find('.msg');

				var $select = $this.find(setting.selector);
				var optionData = args['data-product-options'];
				var skuData = args['data-sku-data'];
				var skuOpt = [];
				var addOnProductType = args['data-component-addon-product-option'].addOnProductType;
				var relativeType = args['data-component-addon-product-option'].addOnRelativeType;
				var relativeQuantity = args['data-component-addon-product-option'].addOnRelativeQuantity;
				forceDependent = (args['data-component-addon-product-option'].forceDependent === 'true') ? true : false;
				isRequired = (args['data-component-addon-product-option'].isRequired === 'true') ? true : false;
                isEquivalent = args['data-component-addon-product-option'].addOnRelativeType == 'EQUIVALENT';

				for(var i=0; i<skuData.length; i++){
					if(skuData[i].inventoryType !== 'UNAVAILABLE'){
						if(skuData[i].quantity === 'null' || skuData[i].quantity > 0 || skuData[i].inventoryType === 'ALWAYS_AVAILABLE'){
						    (function(){
						        var obj = {};
								obj['privateId'] = skuData[i].skuId;
								obj['name'] = args['data-component-addon-product-option'].name;
								obj['retailPrice'] = Core.Utils.price(args['data-component-addon-product-option'].retailPrice);
						        obj['salePrice'] = Core.Utils.price(args['data-component-addon-product-option'].salePrice);
								obj['isQuantity'] = (relativeType === 'PROPORTION' || relativeType === 'NONE' || relativeType === 'null') ? true : false;
	    						obj['quantity'] = (relativeQuantity > skuData[i].quantity || relativeQuantity === 'null' || relativeQuantity === '') ? skuData[i].quantity : relativeQuantity;
	    						obj['selectedOptions'] = (function(index){
	    						    var arr = [];
	    						    skuData[index].selectedOptions.forEach(function(a, i){
										var obj = {};
	    						        obj[optionData[i].attributeName] = optionData[i]['values'][a];
										arr.push(obj);
	    						    });
									obj['options'] = JSON.stringify(arr);
	    							return arr;
	    						})(i);
								obj['label'] = (function(index){
	    						    var arr = [];
	    						    skuData[index].selectedOptions.forEach(function(a, i){
										arr.push(optionData[i]['values'][a]);
	    						    });
	    							return arr;
	    						})(i).join(' / ');
	    						skuOpt.push(obj);
						    })();
						}
					}
				}

				selectComponent = Core.getComponents('component_select', {context:$this}, function(){
					this.getThis().find('select').append(Handlebars.compile($(setting.optionTemplate).html())(skuOpt));
					this.rePaintingSelect();
					this.addEvent('change', function(val, $selected, index){
						var requestChildItem = {};
						var privateId = $selected.attr('data-privateid');
						requestChildItem.productId = args['data-component-addon-product-option'].addonId;
						requestChildItem.quantity = isEquivalent ? $('[data-component-quantity]').find('input').val() : 1;

						for(var i=0; i<skuOpt[index-1].selectedOptions.length; i++){
							for(var key in skuOpt[index-1].selectedOptions[i]){
								requestChildItem['itemAttributes['+ key +']'] = skuOpt[index-1].selectedOptions[i][key];
							}
						}

						addChildItem(privateId, skuOpt[index-1], requestChildItem);
						if(isFireEvent){
							_self.fireEvent('addToAddOnItem', this, [privateId, $selected]);
						}else{
							isFireEvent = true;
						}
					});
				});

                if(isEquivalent){
					var parentQuantity = $('[data-component-quantity]').find('input');
                    parentQuantity.off('change.addOnEqivalent focusout.addOnEqivalent').on('change.addOnEqivalent focusout.addOnEqivalent',function(){
                        var key = $('.addon-state').attr('data-privateId');
                        if(objChildItem.hasOwnProperty(key)){
                            var $selectedvalue = $(selectComponent.getThis()).find('select').val();
                            $('.addon-state').find('.btn-delete').trigger('click');
							selectComponent.trigger($selectedvalue,$selectedvalue);
                        }
                    });
				}
				var skuQuantity = 0;
				skuData.forEach(function(rowData,eq){
			    	skuQuantity += rowData.quantity;
				});
			    if (skuQuantity <= 0){
			        $(selectComponent.getThis()).wrap('<div class="no-data"></div>').parent().html('해당 사은품의 재고가 전부 소진 되었습니다.');
			    }

				/* delete btn addEvent */
				$addonListWrap.on('click', '.btn-delete', function(e){
					e.preventDefault();

					var $parent = $(this).closest('.addon-state');
					var key = $parent.attr('data-privateId');
					$parent.remove();

					if(objChildItem.hasOwnProperty(key)){
						delete objChildItem[key];
						selectComponent.reInit();
					}

					if(Core.Utils.objLengtn() <= 0){
						isValidate = false;
					}

					_self.fireEvent('itemDelete', this, [key]);
				});

				/* isSubmit === true */
				$addonListWrap.on('click', '.btn-submit', function(e){
					e.preventDefault();
					_self.fireEvent('submit', this, [_self.getChildAddToCartItems()]);
				});

				return this;
			},
			setTrigger:function(privateId){
				isFireEvent = false;
				if(selectComponent) selectComponent.trigger(privateId, privateId);
			},
			getChildAddToCartItems:function(){
				var arrChildItem = [];
				for(var key in objChildItem){
					arrChildItem.push(objChildItem[key]);
				}

				return arrChildItem;
			},
			getValidateChk:function(){
				if((!isRequired || isRequired === 'null' ) && (!forceDependent || forceDependent === 'null')){
					isValidate = true;
				}else{
					if(!isValidate){
						addMessage();
					}
				}
				return isValidate;
			},
			getAddonId:function(){
				return args['data-component-addon-product-option'].addonId;
			},
			removeItems:function(){
				$addonListWrap.find('.btn-delete').trigger('click');
			},
			getAddOnOrderId:function(){
				return args['data-component-addon-product-option'].addOnOrderId;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_addon_product_option'] = {
		constructor:addOnProduct,
		attrName:['data-component-addon-product-option', 'data-product-options', 'data-sku-data']
	}
})(Core);
