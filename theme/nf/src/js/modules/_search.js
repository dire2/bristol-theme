(function(Core){
	Core.register('module_search', function(sandbox){
		var $this, args, clickIS, endPoint, latestKeywordList, arrLatestKeywordList = [];

		var setSearchKeyword = function(keyword){
			var temp_latestKeywordList = $.cookie('latestSearchKeyword') || '';
			var pattern = new RegExp(keyword, 'g');
			arrLatestKeywordList = sandbox.utils.rtnMatchComma(temp_latestKeywordList.replace(pattern, ''));
			arrLatestKeywordList.unshift(keyword);

			if(arrLatestKeywordList.length >= args.keywordMaxLen){
				arrLatestKeywordList = arrLatestKeywordList.slice(0, -1);
			}
			$.cookie('latestSearchKeyword', arrLatestKeywordList.join(','));
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				clickIS = false;

				latestKeywordList = $.cookie('latestSearchKeyword') || '';
				arrLatestKeywordList = sandbox.utils.rtnMatchComma(latestKeywordList || '');

				endPoint = Core.getComponents('component_endpoint');

				sandbox.getComponents('component_searchfield', {context:$this, resultTemplate:'#latest-search-keyword', resultWrap:'.etc-search-wrap'}, function(){
					this.addEvent('resultSelect', function(data){
						var text = $(data).text();
						
						//nf는 인기검색어 앞에 순번이 있어 아이템 선택시 순번 제거 필요.(인기검색어일때만)
						if($('#latest-keyword').attr('aria-hidden')=='true'){
							if(text.startsWith('10')){
	                            text = text.substring(4);
	                        } else if(text.match(/^\d/)){
	                            text = text.substring(3);
	                        }  
			            }
						var endPointData = {
							key : text,
							text : text
						}
						endPoint.call( 'searchSuggestionClick', endPointData );

						this.getInputComponent().setValue(text);
						setSearchKeyword(text);
						location.href='/search?q='+ text;
					});

					this.addEvent('beforeSubmit', function(data){
						setSearchKeyword(data);
					});
					
		            //검색어 탭 - 최근검색어 일때만 검색어 전체삭제 버튼 노출
					$this.find('.sort-tabs').click(function(e){
		               if($('#latest-keyword').attr('aria-hidden')=='true'){
		                  $('#delete-all-latest').css('display','none');
		               } else {
		                  $('#delete-all-latest').css('display','block');
		               }
		            });
					
					//최근검색어 비우기 클릭시
					$this.find('#delete-all-latest').click(function(e){
						e.preventDefault();
						$.cookie('latestSearchKeyword',null);
						$this.find('#latest-keyword').html(Handlebars.compile($("#latest-search-keyword").html())({label:'최근 검색어',keyword:[]}));
						this.focus();
					});

					/* 최근검색어 */
					if(args.isLatestKeyword === 'true'){
						this.setResultAppend('#keyword-container', '#latest-search-keyword', {
							label:'최근 검색어',
							keyword:arrLatestKeywordList
						});
					}
				});

				// search btn (search field show&hide)
				$('.gnb-search-btn').click(function(e){
					e.preventDefault();

					if(clickIS){
						clickIS = false;
						$this.removeClass('active');
					}else{
						clickIS = true;
						$this.addClass('active');
					}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-search]',
					attrName:'data-module-search',
					moduleName:'module_search',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			searchTrigger:function(){
				$('.gnb-search-btn').trigger('click');
			}
		}
	});
})(Core);
