(function(Core){
	Core.register('module_filter_nf', function(sandbox){
		'use strict';

		var $this, $that, latestKeywordList, $it;
		var setting = {
			selector:'[data-module-filter-nf]',
			selector2:'[data-module-gnb]',
			selector3:'[data-module-search]'
		}
		var getV = function(dataList,key) {
			dataList = dataList.replace("{","").replace("}","").replace(/\s/g,"");
			var ary = dataList.split(",");
			var map = "";
			var val = "";
			for(var i=0; i < ary.length; i++){
			    if(ary[i].indexOf(key) > -1){
			        map = ary[i].split(":");
			        val = map[1];
			        break;
			    }
			} 
	        return val;
    	}

		var Method = {
			moduleInit:function(){
				$this = $(setting.selector);
				$that = $(setting.selector2);
				$it = $(setting.selector3);
				
//				var searVal = $it.attr("data-module-search");
//				var isLatestKeyword = getV(searVal,'isLatestKeyword');
//				if(isLatestKeyword === 'true') {
//					$it.find('.empty-btn').click(function(e){
//						e.preventDefault();
//						$.cookie('latestSearchKeyword',null);
//						$it.find('#latest-keyword').html(Handlebars.compile($("#latest-search-keyword").html())({label:'최근 검색어',keyword:[]}));
//					});
//				}
				
				//기본 검색바노출
				$that.find(".search-panel").css('display', 'block');
				
//				var scHeight = $that.find(".search-panel").css('height');
//				var thisHeight = $this.find(".contents").css('margin-top');
//				var addHeight = parseInt(scHeight,10) + parseInt(thisHeight,10);
//				var minusHeight = parseInt(addHeight,10) - parseInt(scHeight,10);
				
				//모바일이 아닐때만
//				if(!Core.Utils.mobileChk) {
//					$this.find(".contents").css('margin-top', addHeight+'px');
//					
//					$that.find('.btn-search-close').click(function(e){
//						e.preventDefault();
//						$that.find('.search-panel').removeAttr('style');
//						$this.find(".contents").css('margin-top', minusHeight+'px');
//					});
//					
//					$that.find('.gnb-search-field a').click(function(e){
//						e.preventDefault();
//						$that.find('.search-panel').css('display', 'block');
//						$that.find('.search-field').find('input[type=search]').focus();
//						$this.find(".contents").css('margin-top', addHeight+'px');
//					});
//				}
				
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-filter-nf]',
					attrName:['data-module-filter-nf'],
					moduleName:'module_filter_nf',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
