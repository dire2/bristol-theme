(function(Core){
	Core.register('module_filter', function(sandbox){
		'use strict';

		var $filter, args, currentMinPrice, currentMaxPrice, minPrice, maxPrice, limit, arrInputPrice = [], arrQuery = [], currentRangePrice = '', endPoint;
		var pricePattern = 'price=range[{{minPrice}}:{{maxPrice}}]';

		var limitPrice = function(price){
			if(price < minPrice) return minPrice;
			else if(price > maxPrice) return maxPrice;
			else return price;
		}

		var replaceComma = function(str){
			return str.replace(/,|\.+[0-9]*/g, '');
		}

		var getPriceByPercent = function(price){
			return (price-minPrice) / (maxPrice-minPrice) * 100;
		}

		var getPercentByPrice = function(per){
			return Math.round(minPrice+(limit * per)/ 100);
		}


		var callEndPoint = function( option ){
			var temp = option.split("=");
			if( temp.length > 1){
				var opt = {
					key : temp[0],
					value : temp[1]
				}
				endPoint.call( 'applyFilter', opt );
			}
		}
		
		var initSearchOption = function() {
			$('input[type=checkbox]').prop('checked', false);
			$('.checked-txt').text('전체');
		}
		
		var getPriceVal = function(val) {
			var priceVal = JSON.parse(val.replace('range','').replace(":",","));
			return Core.Utils.price(priceVal[0]) + " ~ " + Core.Utils.price(priceVal[1]);
		}
		
		var serachOptionNameVal = function(sel,title,arr) {
			var sumVal = '';
			var tempLen = 0;
			for(var i=0; i<arr.length; i++) {
				var temp = arr[i].split("=");
				if(temp.length > 1){
					var opt = {
						key : temp[0],
						value : temp[1]
					}
					if(title == opt.key) {
						if(tempLen > 0) sumVal += ',';
						if(title == 'price') {
							var priceVal = getPriceVal(decodeURIComponent(opt.value));
							sumVal += decodeURIComponent(priceVal);
						} else {
							sumVal += decodeURIComponent(opt.value);
						}
						tempLen ++;
					}
				}
			}
			$(sel).closest('.f-refine-sect').find('.checked-txt').text(sumVal);
		}

		var Method = {
			moduleInit:function(){
				args = arguments[0];
				$filter = $(this);
				endPoint = Core.getComponents('component_endpoint');
				
				//$('input[type=checkbox]').prop('checked', false);


				//초기 query 분류
				//arrQuery = sandbox.utils.getQueryParams(location.href, 'array');
				var query = sandbox.utils.getQueryParams(location.href);
				for(var key in query){
					if(key !== 'page'){
						if(typeof query[key] === 'string'){
							arrQuery.push(key+'='+query[key]);
						}else if(typeof query[key] === 'object'){
							for(var i=0; i < query[key].length; i++){
								arrQuery.push(key+'='+query[key][i]);
							}
						}
					}
				}

				//filter price range
				var priceRange = sandbox.getComponents('component_range', {context:$filter}, function(){
					this.addEvent('change', function(per){
						if($(this).hasClass('min')){
							currentMinPrice = getPercentByPrice(per);
							arrInputPrice[0].setValue(sandbox.rtnPrice(currentMinPrice));
						}else if($(this).hasClass('max')){
							currentMaxPrice = getPercentByPrice(per);
							arrInputPrice[1].setValue(sandbox.rtnPrice(currentMaxPrice));
						}
					});

					this.addEvent('touchEnd', function(per){
						var val = sandbox.utils.replaceTemplate(pricePattern, function(pattern){
							switch(pattern){
								case 'minPrice' :
									return currentMinPrice;
									break;
								case 'maxPrice' :
									return currentMaxPrice;
									break;
							}
						});

						if(arrQuery.indexOf(currentRangePrice) > -1){
							arrQuery.splice(arrQuery.indexOf(currentRangePrice), 1);
						}

						callEndPoint( val );
						arrQuery.push(val);
						currentRangePrice = val;

						Method.appendCateItemList();
					});
				});

				var textfield = sandbox.getComponents('component_textfield', {context:$filter}, function(){
					this.addEvent('focusout', function(e){
						var type = $(this).attr('data-name');
						var per = getPriceByPercent(limitPrice(replaceComma($(this).val())));

						if(type === 'min'){
							priceRange.getSlide(0).setPercent(per);
						}else if(type === 'max'){
							priceRange.getSlide(1).setPercent(per);
						}
					});

					arrInputPrice.push(this);
				});

				if(priceRange){
					var objPrice = (priceRange) ? priceRange.getArgs() : {min:0, max:1};
					minPrice = (objPrice.min == 'null') ? 0:parseInt(objPrice.min);
					maxPrice = (objPrice.max == 'null') ? 1:parseInt(objPrice.max);
					limit = maxPrice - minPrice;
					currentMinPrice = replaceComma(arrInputPrice[0].getValue());
					currentMaxPrice = replaceComma(arrInputPrice[1].getValue());
					priceRange.getSlide(0).setPercent(getPriceByPercent(currentMinPrice));
					priceRange.getSlide(1).setPercent(getPriceByPercent(currentMaxPrice));

					currentRangePrice = sandbox.utils.replaceTemplate(pricePattern, function(pattern){
						switch(pattern){
							case 'minPrice' :
								return currentMinPrice;
								break;
							case 'maxPrice' :
								return currentMaxPrice;
								break;
						}
					});
				}
				
				//모바일일때 필터 검색방법시 주석해제
				if(sandbox.utils.mobileChk){
					args['data-module-filter'].filterSearchType = "all";
					
					//선택한 항목 표시
					var searNum = 0;
					$(".f-refine-sect").each(function(){
						$(this).find('input').each(function(){
							if($(this).parent().hasClass('checked')){
								var title = $(this).attr('name');
								serachOptionNameVal(this,title,arrQuery);
								searNum++;
							}
						});
					});
					if(searNum > 0) {
						$(".filter-etc").find('span').text('('+searNum+')');
					}
					
					$(document).on('touchmove', function(e) {
						if($("div").hasClass('uk-tooltip')) {
							e.preventDefault();
							if(!$(".uk-tooltip").hasClass('uk-hidden')) {
								$(".uk-tooltip").addClass("uk-hidden");
							}
						}
					});
				}


				// 필터 클릭 처리
				sandbox.getComponents('component_radio', {context:$filter, unlock:true}, function(i){
					var currentValue = '';

					//처음 라디오 박스에 체크 되었을때만 이벤트 발생
					this.addEvent('init', function(){
						var val = this.attr('name') +'='+ encodeURIComponent($(this).val());
						currentValue = val;
					});

					this.addEvent('click', function(input){
						var val = $(input).attr('name') +'='+ encodeURIComponent($(input).val());
						if($(this).parent().hasClass('checked')){
							arrQuery.splice(arrQuery.indexOf(val), 1);
						}else{
							if(currentValue !== '') arrQuery.splice(arrQuery.indexOf(currentValue), 1);
							callEndPoint( val );
							arrQuery.push(val);
							currentValue = val;
						}

						Method.appendCateItemList();
					});
				});

				sandbox.getComponents('component_checkbox', {context:$filter}, function(){
					this.addEvent('change', function(){
						var val = $(this).attr('name') +'='+ encodeURIComponent($(this).val());
						if(arrQuery.indexOf(val) !== -1){
							arrQuery.splice(arrQuery.indexOf(val), 1);
						}else{
							callEndPoint( val );
							arrQuery.push(val);
						}

						Method.appendCateItemList();
						
						var title = $(this).attr('name');
						serachOptionNameVal(this,title,arrQuery);
					});
				});

				//필터 동작
				$(document).on('click', '.filter-remove-btn', function(e){
					e.preventDefault();

					var href = $(this).attr('href');
					var query = '';

					if(href.match(/[가-힣]/g)){
						query = encodeURI(href);
					}else{
						query = href;
					}

					arrQuery.splice(arrQuery.indexOf(query), 1);
					Method.appendCateItemList('removeBtn');
				});

				$(document).on('click', args['data-module-filter'].filterOpenBtn, function(e){
					e.preventDefault();

					$filter.show();
					// $filter.stop().animate({opacity:1, left:0}, 300);
					// $filter.find('.result-btn').stop().animate({opacity:1, left:0}, 300);
					// $('.dim').addClass('active');
					$('html').addClass('uk-modal-page');
					$('body').css('paddingRight', 15);
				});

				$(document).on('click', '.close-btn', function(e){
					$filter.hide();
					// $filter.stop().animate({opacity:0, left:-300}, 300, function(){
					// 	$(this).removeAttr('style');
					// });
					// $filter.find('.result-btn').stop().animate({opacity:0, left:-300}, 300, function(){
					// 	$(this).removeAttr('style');
					// });
					// $('.dim').removeClass('active');
					$('html').removeClass('uk-modal-page');
					$('body').removeAttr('style');
				});
				
				//선택해제
				$(document).on('click', '.reset-btn', function(e){
					arrQuery = [];
					initSearchOption();
				});

				$(document).on('click', args['data-module-filter'].filterSearchBtn, function(e){
					e.preventDefault();
					Method.appendCateItemList('searchBtn');
				});
				
				if(args['data-module-filter'].filterSearchType == "all"){
					$(this).find(".searchFilterArea").show();
				}

				//필터 더보기 버튼
				$filter.find('.more-btn').each(function(){
					var $this = $(this);
					var $target = $this.prev();
					var minHeight = $target.height();
					var maxHeight = $target.children().height();

					$(this).click(function(e){
						e.preventDefault();

						if($this.hasClass('active')){
							$this.removeClass('active');
							$target.stop().animate({'height':minHeight}, 300);
						}else{
							$this.addClass('active');
							$target.stop().animate({'height':maxHeight}, 300);
						}
					});
				});
			},

			appendCateItemList:function(param){
				//console.log(getPagingType);
				var query = arrQuery.join('&');
				query += sandbox.getModule('module_pagination') ? (sandbox.getModule('module_pagination').getPagingType() === 'number' ? '&page=1&' : '') : '';
				var searchType = args['data-module-filter'].filterSearchType;

				if(searchType == 'all') {
					if(param == 'searchBtn' || param == 'removeBtn') {
						window.location.assign(location.pathname + '?' + query);
					}
				} else {
					window.location.assign(location.pathname + '?' + query);
				}
				//

				//$(args.form).serialize();
				//$(args.form).submit();
				//console.log($('[' + args['data-module-filter'].form + ']').serialize());
				//$('[' + args['data-module-filter'].form + ']').submit();

				/*sandbox.utils.ajax(url, 'GET', queryString, function(data){
					var responseText = $(data.responseText).find(args['data-module-filter'].target)[0].innerHTML;
					$(args['data-module-filter'].target).empty().append(responseText);
					sandbox.moduleEventInjection(responseText);
				});*/
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-filter]',
					attrName:['data-module-filter'],
					moduleName:'module_filter',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
