(function(Core){
	Core.register('module_checkout', function(sandbox){
		var Method = {
			moduleInit:function(){
				var $this = $(this);

				// header
				$this.find('[data-order-tab] .header').on("mousedown", Method.updateOrderTab);
				
				var sumtit = "";
				$("#order-summary").find(".order-info").find(".tit").each(function(i){
					if(i>0) {
						sumtit += ",";
					}
					sumtit += $(this).text();
					//console.log(sumtit);
					$(".product-checkout").find(".preview").text(sumtit);
				});
			},
			
			updateOrderTab:function(e){
				e.preventDefault();
				var $icon = $(this).find('[class^="icon-toggle-"]');
				var $view = $(this).closest('.order-tab').find('.view');
				var $preview = $(this).find('.preview');

				if( $view.length > 0 ){
					$preview.toggleClass('uk-hidden');
					$icon.toggleClass('uk-hidden');
					$view.toggleClass('uk-hidden');
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-checkout]',
					attrName:'data-module-checkout',
					moduleName:'module_checkout',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
