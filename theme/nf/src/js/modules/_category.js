(function(Core){
	Core.register('module_category', function(sandbox){
		var $that;
		var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];
		var Method = {
			moduleInit:function(){
				$this = $(this);
				var $filterWrap = $this.find('.filter-wrapper');
				//uk-width-medium-1-3 uk-width-large-1-3
				//view Length 2:maxLen
				$this.find('.select-view > button').click(function(e){
					e.preventDefault();

					if(!$(this).hasClass('active')){
						$(this).addClass('active').siblings().removeClass('active');
						$this.find('[data-component-categoryitem]').parent()
						.removeClass(arrViewLineClass.join(' '))
						.addClass('uk-width-large-1-'+$(this).attr('data-value'));
						$this.find('[data-component-categoryitem1]').parent()
						.removeClass(arrViewLineClass.join(' '))
						.addClass('uk-width-large-1-'+$(this).attr('data-value'));
						$this.find('[data-component-categoryitem2]').parent()
						.removeClass(arrViewLineClass.join(' '))
						.addClass('uk-width-large-1-'+$(this).attr('data-value'));

						//category lineSize
						sandbox.getModule('module_pagination').setLineSize($(this).attr('data-value'));
					}
				});

				$this.find('.filter-btn').click(function(e){
					e.preventDefault();

					if($filterWrap.hasClass('pc-only')){
						$filterWrap.removeClass('pc-only');
					}else{
						$filterWrap.addClass('pc-only');
					}

				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-category]',
					attrName:'data-module-category',
					moduleName:'module_category',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
