(function(Core){
	var CategoryItem = function(){
		'use strict';

		var $this, $overlayTxt, $quickViewBtn, $hover, modal, args, endPoint;
		var setting = {
			selector:'[data-component-categoryitem2]',
			overlayTxt:'.category-overlaytext',
			quickViewBtn:'.quick-btn',
			hover:'.action-hover'
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;

				args = arguments[0];
				$this = $(setting.selector);
				//endPoint = Core.getComponents('component_endpoint');
				$overlayTxt = $this.find(setting.overlayTxt);
				$quickViewBtn = $this.find(setting.quickViewBtn);
				$hover = $this.find(setting.hover);
				modal = UIkit.modal('#common-modal', {modal:false, center:true});
				modal.off('.uk.modal.categoryItem').on({
					'hide.uk.modal.categoryItem':function(){
						console.log('categoryItem modal hide');
						$('html').removeClass('uk-modal-page');
						$('body').removeAttr('style');
					}
				});

				//category item image lazy load
				//$this.find('.lazy').lazyload();

				$quickViewBtn.click(function(e){
					e.preventDefault();

					//var id = $(this).siblings().filter('input[name=productId]').attr('value');
					var target = $(this).attr('data-href');
					var url = $(this).siblings().filter('input[name=producturl]').attr('value');

					Core.Utils.ajax(url, 'GET', {quickview:true}, function(data){
						var domObject = $(data.responseText).find('#quickview-wrap');
						$(target).find('.contents').empty().append(domObject[0].outerHTML);
						$(target).addClass('quickview');
						Core.moduleEventInjection(domObject[0].outerHTML);
						modal.show();

						var $product = $(domObject[0].outerHTML);
						var productData = Core.Utils.strToJson( $product.find('[data-module-product]').data('module-product'), true );
						var data = {
							id : productData.productId,
							name : $product.find('[data-name]').data('name'),
							price : $product.find('[data-price]').data('price'),
							isDefaultSku : productData.isDefaultSku
						}
						//endPoint.call('quickView', {product : data})
					});
				});

				$this.find('a').click(function(e){
					sessionStorage.setItem('isHistoryBack', true);
					sessionStorage.setItem('categoryScrollTop', $(document).scrollTop());
					sessionStorage.setItem('categoryTarget', args.parentWrapper);
					sessionStorage.setItem('categoryPathname', location.pathname);
					sessionStorage.setItem('categoryList', $(args.parentWrapper)[0].innerHTML);
				});

				if(!Core.Utils.mobileChk){
					$hover.on('mouseenter', function(e){
						$(this).addClass('over');
					});

					$hover.on('mouseleave', function(e){
						$(this).removeClass('over');
					});
				}

				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_categoryitem2'] = {
		constructor:CategoryItem,
		reInit:true,
		attrName:'data-component-categoryitem2'
	}
})(Core);
