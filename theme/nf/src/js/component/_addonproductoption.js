(function(Core){
	var addOnProduct = function(){
		'use strict';

		var $this, $addonListWrap, $msg, args, selectComponent = null, isRequired=false, isValidate=false, isFireEvent = true;
		var objChildItem={}; //single 만 됨
		var setting = {
			selector:'[data-component-addon-product-option]',
			selectbox:'select',
			optionTemplate:'#addon-sku-option',
			listTemplate:'#addon-sku-list',
			resultWrap:'.addon-list-wrap',
		}

		var addChildItem = function(key, skuData, requestChildItem){
			/*
				원상품 : 애드온 상품
					x_FORCED
					NONE
					1 : 1	EQUIVALENT			quantityComponent 추가 (수량체크는 원상품의 주문 수량만큼 추가가능) 인데 일단 수량 컴포넌트 삭제
					1 : x	PROPORTION			quantityComponent 추가 (relativeQuantity 값 만큼 추가가능)
					x : 1	PROPORTION_REV
			*/

			var appendTxt = '';
			if($(setting.listTemplate).length <= 0){
				throw new Error(setting.listTemplate + ' template is not defined');
				return;
			}

			if(args['data-component-addon-product-option'].addOnListType === 'single'){
				objChildItem = {};
				$addonListWrap.empty();
			}

			if(!objChildItem.hasOwnProperty(key)){
				skuData.privateId = key;
				appendTxt = Handlebars.compile($(setting.listTemplate).html())(skuData);
				objChildItem[key] = requestChildItem;
				$addonListWrap.append(appendTxt);

				if(skuData.isQuantity){
					Core.getComponents('component_quantity', {context:$addonListWrap}, function(){
						this.addEvent('change', function(qty){
							objChildItem[key].quantity = qty;
						});
					});
				}
			}

			isValidate = true;
			if($msg.length > 0){
				$msg.text('');
			}
		}

		var addMessage = function(){
			if($msg.length > 0){
				$msg.text('상품을 선택해 주세요');
			}else{
				UIkit.notify('상품을 선택해 주세요', {timeout:3000,pos:'top-center',status:''});
			}
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				args = arguments[0];

				//에드온의 템플릿이 없으면 에러 throw
				if($(setting.optionTemplate).lenght <= 0){
					throw new Error('#addon-sku-option template is not defined');
					return;
				}

				//sku 옵션생성
				$this = $(setting.selector);
				$addonListWrap = $this.find(setting.resultWrap);
				$msg = $this.siblings().filter('.tit').find('.msg');

				var $select = $this.find(setting.selector);
				var optionData = args['data-product-options'];
				var skuData = args['data-sku-data'];
				var skuOpt = [];
				var addOnProductType = args['data-component-addon-product-option'].addOnProductType;
				var relativeType = args['data-component-addon-product-option'].addOnRelativeType;
				var relativeQuantity = args['data-component-addon-product-option'].addOnRelativeQuantity;
				isRequired = (addOnProductType === 'DEPENDENT') ? true : false;

				for(var i=0; i<skuData.length; i++){
					if(skuData[i].inventoryType !== 'UNAVAILABLE'){
						if(skuData[i].quantity === null || skuData[i].quantity > 0){
						    (function(){
						        var obj = {};
								obj['privateId'] = skuData[i].skuId;
								obj['name'] = args['data-component-addon-product-option'].name;
								obj['retailPrice'] = Core.Utils.price(args['data-component-addon-product-option'].retailPrice);
						        obj['salePrice'] = Core.Utils.price(args['data-component-addon-product-option'].salePrice);
								obj['isQuantity'] = (relativeType === 'PROPORTION' || relativeType === 'NONE' || relativeType === 'null') ? true : false;
	    						obj['quantity'] = (relativeQuantity > skuData[i].quantity || relativeQuantity === 'null' || relativeQuantity === '') ? skuData[i].quantity : relativeQuantity;
	    						obj['selectedOptions'] = (function(index){
	    						    var arr = [];
	    						    skuData[index].selectedOptions.forEach(function(a, i){
										var obj = {};
	    						        obj[optionData[i].attributeName] = optionData[i]['values'][a];
										arr.push(obj);
	    						    });
	    							return arr;
	    						})(i);
								obj['label'] = (function(index){
	    						    var arr = [];
	    						    skuData[index].selectedOptions.forEach(function(a, i){
										arr.push(optionData[i]['values'][a]);
	    						    });
	    							return arr;
	    						})(i).join(' / ');
	    						skuOpt.push(obj);
						    })();
						}
					}
				}

				selectComponent = Core.getComponents('component_select', {context:$this}, function(){
					console.log(this);
					this.getThis().find('select').append(Handlebars.compile($(setting.optionTemplate).html())(skuOpt));
					this.rePaintingSelect();
					this.addEvent('change', function(val, $selected, index){
						var requestChildItem = {};
						var privateId = $selected.attr('data-privateid');
						requestChildItem.productId = args['data-component-addon-product-option'].addonId;
						requestChildItem.quantity = 1;

						for(var i=0; i<skuOpt[index-1].selectedOptions.length; i++){
							for(var key in skuOpt[index-1].selectedOptions[i]){
								requestChildItem['itemAttributes['+ key +']'] = skuOpt[index-1].selectedOptions[i][key];
							}
						}

						addChildItem(privateId, skuOpt[index-1], requestChildItem);
						if(isFireEvent){
							_self.fireEvent('addToAddOnItem', this, [privateId]);
						}else{
							isFireEvent = true;
						}
					});
				});

				/* delete btn addEvent */
				$addonListWrap.on('click', '.btn-delete', function(e){
					e.preventDefault();

					var $parent = $(this).closest('.addon-state');
					var key = $parent.attr('data-privateId');
					$parent.remove();

					if(objChildItem.hasOwnProperty(key)){
						delete objChildItem[key];
						selectComponent.reInit();
					}

					if(Core.Utils.objLengtn() <= 0){
						isValidate = false;
					}
				});



				/*ㄴㅁㅇㄹ*/
				$this.find('.registor-btn').click(function(e){
					e.preventDefault();

					Core.Utils.promise({
						url:'/register',
						type:'GET'
					}).then(function(data){
						var defer = $.Deferred();
						var appendTxt = $(data).find('.content-area').html();
						$('#common-modal').find('.contents').empty().append(appendTxt);
						Core.moduleEventInjection(appendTxt, defer);
						UIkit.modal('#common-modal').show();
						return defer.promise();
					}).then(function(data){
						consol.log(data);
						//location.href = reDirectUrl;
					}).fail(function(msg){
						UIkit.notify(msg, {timeout:3000,pos:'top-center',status:'danger'});
					});
				});

				return this;
			},
			setTrigger:function(){
				isFireEvent = false;

				//if(selectComponent) selectComponent.update();
			},
			getChildAddToCartItems:function(){
				var arrChildItem = [];
				for(var key in objChildItem){
					arrChildItem.push(objChildItem[key]);
				}
				return arrChildItem;
			},
			getValidateChk:function(){
				if(!isRequired || isRequired === 'null'){
					isValidate = true;
				}else{
					if(!isValidate){
						addMessage();
					}
				}
				return isValidate;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_addon_product_option'] = {
		constructor:addOnProduct,
		attrName:['data-component-addon-product-option', 'data-product-options', 'data-sku-data']
	}
})(Core);
