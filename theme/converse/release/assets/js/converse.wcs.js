/*!
 * GS SHOP - WCS ( Web Collecting System ) Script
 * 
 * @author jake <__kim.jh4@gsshop.com__>
 * @copyright (c) 2017 GS SHOP
 * @link http://www.gsshop.com
 * 
 */
var Wcs = Wcs || (function(){
	var global = this;
	
	String.prototype.startsWith = function(str) {
	   return this.indexOf(str) === 0;
	};
	String.prototype.endsWith = function(str){
	    var lastIndex = this.lastIndexOf(str);
	    return (lastIndex != -1) && (lastIndex + str.length == this.length);
	};
	String.prototype.trim = function() {
	    return this.replace(/(^\s*)|(\s*$)/gi, "");
	};
	String.prototype.toBoolean = function() {
	    return (/^true$/i).test(this);
	};		
	
	var _doc = document,
	_navi = navigator,
	_screen = screen,
	_win = window,
	/* encode */
	_encodeWrapper = _win.encodeURIComponent,
	//_encodeWrapper = _win.encodeURI,
	/* decode */
	_decodeWrapper = _win.decodeURIComponent,
	//_decodeWrapper = _win.decodeURI,
	/* urldecode */
	_urldecode = unescape,
	// wcs가 로드된 window가 iframe인지 판단.
	_isIframe =  (window.location != window.parent.location),
	// top level domains
	_topLevelDomain = ['.com', '.co.kr', '.kr', '.net', '.org'];
	
	// WCS global 설정정보.
	var _globalConfig = {
//		debug: "false".toBoolean(),
		debug: "true".toBoolean(),
		denyDomains: [],
		serverHost : 'wcs.gslook.com',
		cookie: {
			// 쿠키값 저장시 해싱 여부
			hash: false,
			 // 세션 아이디를 저장할 쿠키명 및 유통기한(분)
			sessionid: {
				key: 'wa_sid',
				expires: 30
			},
			referer: {
				// 리퍼러를 쿠키에 저장할 지 여부 (백버튼 이슈)
				use: false,
				key: 'wa_ref',
				expires: 30
			},
			pcid: {
				key: 'wa_pcid',
				expires: 60*24*365*10
			},
			userid: {
				key: 'wa_userid',
				expires: 60*24*365*10
			},
			randnum: {
				key: 'wa_randnum',
				expires: 60*24*365*10
			}
		},
		event: {
			// document keydown 이벤트를 캐치할 키코드 배열
			keycode: [33, 34, 37, 38, 39, 40]
		},
		uuidLen: 21,
		ajax: {
			timeout: 2000
		},
		onlyOnload: true, // onload시점에 전송할 데이터를 onload이벤트 발생시에만 보낼지 여부
		eventSendInterval: 1000 // button event 전송 주기
	};	
	
	// site별로 종속적인 설정정보.
	var _siteConfig = {
		converseWeb: {
			cookie: {
				// 유저 아이디가 저장된 쿠키명 호출 function				
				userid: function() {
					var tempcookienm = 'ecid';
					var cinfo = util.getCookie(tempcookienm);
					if (cinfo != null && cinfo != '') {
						var result = getParamValue(cinfo, 'catvid', '~');
						if(result == "0") result = '';	
						return result;
					}
					return '';
				},
				// 매체코드 쿠키명
				appmediatype: 'appmediatype'
			},
			recommPrd: {
				params: {
					product: 'prdid',
					plan: 'planseq'
				},
				linkPattern: /(bnclick=)./				
			},
			cartEvent: {
				functionName: '',
				argsIndex: 3				
			},
			// onload이벤트로 전달할 대상 url
			loadTarget: [
			    /\/prd\/prd\.gs/gi,
			    /\/cart\/viewCart\.gs/gi,
				/\/order\/baskt\/baskt\.gs/gi,
			    /\/jsp\/jseio_viewMyCart\.jsp/gi,
			    /\/order\/main\/confirmOrder\.gs/gi,
				/\/order\/ordsht\/ordFsh\.gs/gi
			]
		},
		converseMobile: {
			cookie: {				
				userid: function() {
					var tempcookienm = 'ecid';
					var cinfo = util.getCookie(tempcookienm);
					if (cinfo != null && cinfo != '') {
						var result = getParamValue(cinfo, 'catvid', '~');
						if(result == "0") result = '';
						return result;
					}
					return '';
				},
				appmediatype: 'appmediatype'
			},			
			recommPrd: {
				params: {
					product: 'prdid',
					plan: 'planseq'
				},
				linkPattern: /(bnclick=)./				
			},
			cartEvent: {
				functionName: '',
				argsIndex: 3				
			},
			loadTarget: [
			    /\/prd\/prd\.gs/gi,
			    /\/cart\/viewCart\.gs/gi,
			    /\/jsp\/jseio_viewMyCart\.jsp/gi,
			    /\/order\/main\/confirmOrder\.gs/gi
			]			
		}
	};
	
	// tracker 수집 공통 데이터
	var _data = {
		pageInfo: {
			visitedTime: new Date().getTime()
		},
		buttonEventInfo: {
			btnNameList: []
		}
	};
	
	/**
	 * 주어진 문자열을 브라우저 console로 출력한다.
	 * 
	 * @param {String} str
	 */
	function log(str) {
		
		if (_win.console == undefined) {
			console = {log: function() {}};
		}
		
		if (_globalConfig.debug) {
			console.log('[WCS] ' + str);
		}
	}
	
	/**
	 * 주어진 props 정보를 obj에 merge한다.
	 * 
	 * @param {Object} a
	 * @param {Object} b
	 * @return {Object}
	 */
	function mixin(a, b) {
		var re = a;
		for (p in b) {
			if (a.hasOwnProperty(p)) {
				if (isPlainObject(a[p])) {
					re[p] = mixin(a[p], b[p]);
				} else {
					re[p] = b[p];
				}
			} else {
				re[p] = b[p];
			}
		}
		return re;
	}
	
	/**
	 * 주어진 object가 plain object 인지 판단한다.
	 * 
	 * - isPlainObject({}) // true
	 * - isPlainObject("test") // false
	 * 
	 * @param {Object} obj
	 * @reutrn {bool}
	 */
	function isPlainObject(obj) {
		return (obj != null && obj.constructor === Object);
	}
	
    /**
     * 주어진 property가 정의되어 있는지 판단.
     * 
     * @param {Object} property
     * @return {bool}
     */
	function isDefined(property) {
        return 'undefined' !== typeof property;
    }

    /**
     * 주어진 property가 함수형태인지 판단.
     * 
     * @param {Object} property
     * @return {bool}
     */
    function isFunction(property) {
        return typeof property === 'function';
    }

    /**
     * 주어진 property가 object인지 판단.
     * 
     * @param {Object} property
     * @return {bool} Returns true if property is null, an Object, or subclass of Object (i.e., an instanceof String, Date, etc.)
     */
    function isObject(property) {
        return typeof property === 'object';
    }

    /**
     * 주어진 property가 string인지 판단.
     * 
     * @param {Object} property
     * @return {bool}
     */
    function isString(property) {
    	return typeof property === 'string' || property instanceof String;
    }
    
    /**
     * 브라우저가 ie인지를 판단한다.
     */
    function isIE() {
    	return _navi.userAgent.toLowerCase().indexOf('msie') > -1;   	
    }
    
    /**
     * 브라우저가 ff인지를 판단한다.
     */
    function isFF() {
    	return _navi.userAgent.toLowerCase().indexOf('firefox') > -1;
    }
	
	/**
	 * 특정 event타입을 주어진 element에 바인딩 처리한다.
	 * 
     * @param {Object} element
     * @param {String} eventType 
     * @param {Function} eventHandler 
     * @return {bool}
	 */
	function addEvent(element, eventType, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventType, eventHandler, false);
        } else if (element.attachEvent) {
        	element.attachEvent('on' + eventType, eventHandler);
        }
	}
	
	/**
	 * 주어진 url에서 파라미터명의 value를 얻는다.
	 */
	function getParamValue(url, paramName, delim) {
		var result = '';
		if (url != null && url != '') {
			if (url.indexOf('?') != -1) {
				url = url.split('?')[1];
			}
			delim = delim || '&';
			var urlSplit = url.split(delim);
			if (urlSplit != null && urlSplit.length > 0) {
				for (var i = 0 ; i < urlSplit.length ; i++) {
					var params = urlSplit[i].split('=');
					if (params != null && params.length == 2) {
						if (params[0].trim() == paramName.trim()) {
							result = params[1];
							break;
						}
					}
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Raw javascript ajax
	 */
	function ajax(url, callback) {
		global.ajaxtimeout = false;
		log('sending ajax request : ' + url);
		var httpRequest;
		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
		} else if (window.ActiveXObject) { // IE
			try {
				httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} 
			catch (e) {
				try {
					httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} 
				catch (e) {}
			}
		}
		
		if (!httpRequest) {
			log('Cannot create an XMLHTTP instance');
			return false;
		}
		
		httpRequest.timeout = _globalConfig.ajax.timeout;
		httpRequest.onreadystatechange = responseHandler;
		httpRequest.open('GET', url);
		httpRequest.send(null);
		
		// timeout시간내에 응답이 없을경우 callback실행.
		httpRequest.ontimeout = function () {
			global.ajaxtimeout = true;
			log('run ajax timeout : timeout (' + _globalConfig.ajax.timeout + ' )');
			if (isFunction(callback)) {
				callback();
			}
		};

		
		function responseHandler() {
			if (httpRequest.readyState === 4 && !global.ajaxtimeout) {
				// 응답값에 따라 callback호출여부를 판단하지 않고 무조건 callback을 호출한다.
				if (isFunction(callback)) {
					log('completed ajax response : ' + url);
					callback();
				}
				/*
				if (httpRequest.status === 200 || httpRequest.status === 204) {
					// 성공.
				} else {
					// 에러
				}
				*/
			}
		}
	}
	
	/**
	 * document내에서 주어진 attr명으로된 attribute존재하는 모든 엘리멘트를 찾아서 반환한다.
	 */
	function getAllElementsWithAttribute(attr) {
		var matchingElements = [];
		var allElements = document.getElementsByTagName('*');
		
		for (var i = 0; i < allElements.length; i++) {
			if (allElements[i].getAttribute(attr)) {
				// Element exists with attribute. Add to array.
				matchingElements.push(allElements[i]);
			}
		}
		return matchingElements;
	}
	
	var util = {
		/**
		 * 주어진 json object를 문자열로 변경한다.
		 * 
		 * @param {Object} obj
		 * @return {String}
		 */
		stringify: function(obj) {
			var self = this;
	        var t = typeof (obj);
			if (t != "object" || obj === null) {
				if (t == "string") {
					obj = '"' + obj + '"';
				}

				return String(obj);
			}
			else {
				var v, json = [], arr = (obj && obj.constructor == Array);
				for (var n in obj) {
					if (obj.hasOwnProperty(n)) {
						v = obj[n];
						t = typeof (v);
						if (t == "string") {
							v = '"' + v + '"';
						}
						else if (t == "object" && v !== null) {
							v = self.stringify(v);
						}
						json.push((arr ? "" : '"' + n + '":') + String(v));
					}
				}
				return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
			}
		},
		
		/**
		 * 주어진 array에 주어진 object가 포함되어 있는지 판단한다.
		 * 
		 * @param {Array} a 찾을대상 array
		 * @param {Object} obj 검색대상 object
		 * @returns {Boolean}
		 */
		contains: function(a, obj) {
		    for (var i = 0; i < a.length; i++) {
		        if (a[i] === obj) {
		            return true;
		        }
		    }
		    return false;
		},
		
		/**
		 * 맵에서 항목값이 true인 첫번째 키 추출한다.
		 * 
		 * @param {Object} map
		 * @returns
		 */
		first: function(map) {
			for (var p in map) {
				if (map[p]) { return p; break; }
			}
			return null;
		},        
        
		/**
		 * 주어진 정규식에 매치되는 값을 얻는다.
		 * 
		 * @param {String} s
		 * @param {String} re
		 * @param {Number} i
		 * @returns
		 */
		match: function(s, re, i) {
			if (!i) {
				i = 0;
			}
			var m = s.match(re);
			if (m && m.length > i) {
				return m[i];
			}
			return null;
		},
		
		/**
		 * timestamp 기반 random number값 생성.
		 */
		getRandomNumber: function(min, max) {
			return new Date().getTime() + "" + ( Math.floor(Math.random() * (max - min + 1)) + min );
		},
		
		/**
		 * UUID값 생성.
		 * 
		 * @param {Object} env
		 * @returns {String}
		 */
		getUUID: function(len, radix) {
			var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
			var chars = CHARS, uuid = [], i;
			radix = radix || chars.length;
			if (len) {
				for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
			} else {
				// rfc4122, version 4 form
				var r;
				
				// rfc4122 requires these characters
				uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
				uuid[14] = '4';
				
				// Fill in random data.  At i==19 set the high bits of clock sequence as
				// per rfc4122, sec. 4.1.5
				
				for (i = 0; i < 36; i++) {
					if (!uuid[i]) {
						r = 0 | Math.random()*16;
						uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
					}
				}
			}
			
			return uuid.join('');
		},	
		
		/**
		 * pageview아이디 생성.
		 */
		createPvid: function(len, radix) {
			var randomLimit = 4;
			
			// 0 ~ 30 까지의  random int
            var randomInt = Math.floor(Math.random()*11)*3;
            
            var userAgentHash = this.sha1(_navi.userAgent);
            var docReferrerHash = this.sha1(_doc.referrer);
            var docLocationHash = this.sha1(_doc.location);
            
            var randomValue = this.getUUID(len, radix) + '-' 
            				+ userAgentHash.substr(randomInt, randomLimit) 
            				+ docReferrerHash.substr(randomInt, randomLimit)
            				+ docLocationHash.substr(randomInt, randomLimit) + '-'
            				+ new Date().getTime();
            
            return randomValue;
		},
		
		/**
		 * 쿠키값 호출.
		 * 
		 * @param {String} key
		 * @returns {Object}
		 */
		getCookie: function(key) {
            var cookiePattern = new RegExp('(^|;)[ ]*' + key + '=([^;]*)'),
                cookieMatch = cookiePattern.exec(_doc.cookie);
            return cookieMatch ? _decodeWrapper(cookieMatch[2]) : 0;
		},

		/**
		 * 쿠키값 설정.
		 * 
		 * @param {String} key
		 * @param {String} value
		 * @param {Object} opts
		 */
		setCookie: function(key, value, opts) {
			opts = opts || {};
			
			if (!value) {
				opts.expires = -1;
			} else if (typeof opts.expires == 'number') {
				opts.expires = new Date(new Date().getTime() + (1000*60*opts.expires)).toUTCString();
			}
			
			// 쿠키 생성시 사용될 default domain
			var defaultDomainCookie = '';
			for (var i = 0 ; i < _topLevelDomain.length ; i++) {
				var docDomain = _doc.domain;
				if (docDomain.indexOf(_topLevelDomain[i]) != -1) {
					defaultDomainCookie = _topLevelDomain[i];
			        
			        var tempDomain = docDomain.replace(_topLevelDomain[i], '');
			        var lastIdx = tempDomain.lastIndexOf('.');
			        
			        defaultDomainCookie = tempDomain.substring(lastIdx+1) + defaultDomainCookie;
			        
			        break;
				}
			}
			
			log('set cookie domain : ' + defaultDomainCookie);
			
			var cookieExpires = opts.expires ? opts.expires : '';
			var cookiePath = opts.path ? opts.path : '/';
			var cookieDomain = opts.domain ? opts.domain : defaultDomainCookie;
			var cookieSecure = opts.secure ? 'secure' : '';
			
			_doc.cookie = key + '=' + _encodeWrapper(String(value))
				+ ('; expires=' + cookieExpires)
				+ ('; path=' +  cookiePath)
				+ ('; domain=' + cookieDomain)
				+ ('; ' + cookieSecure);
		},
		
		/**
		 * 쿠키삭제
		 * 
		 * @param {String} key
		 */
		deleteCookie: function(key) {
			setCookie(key, null);
		},
		
		/**
		 * 주어진 문자열을 utf8로 인코딩 처리한다.
		 * 
		 * @param {String} argString
		 * @returns
		 */
        utf8_encode: function(argString) {
            return _urldecode(_encodeWrapper(argString));
        },	
		
        /**
         *  sha1 알고리즘
         *  
         * @param {String} str
         * @returns
         */
        sha1 : function (str) {
            // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
            // + namespaced by: Michael White (http://getsprink.com)
            // +      input by: Brett Zamir (http://brett-zamir.me)
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   jslinted by: Anthon Pang (http://piwik.org)

            var
                rotate_left = function (n, s) {
                    return (n << s) | (n >>> (32 - s));
                },

                cvt_hex = function (val) {
                    var str = '',
                        i,
                        v;

                    for (i = 7; i >= 0; i--) {
                        v = (val >>> (i * 4)) & 0x0f;
                        str += v.toString(16);
                    }
                    return str;
                },

                blockstart,
                i,
                j,
                W = [],
                H0 = 0x67452301,
                H1 = 0xEFCDAB89,
                H2 = 0x98BADCFE,
                H3 = 0x10325476,
                H4 = 0xC3D2E1F0,
                A,
                B,
                C,
                D,
                E,
                temp,
                str_len,
                word_array = [];

            str = this.utf8_encode(str);
            str_len = str.length;

            for (i = 0; i < str_len - 3; i += 4) {
                j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 |
                    str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
                word_array.push(j);
            }

            switch (str_len & 3) {
            case 0:
                i = 0x080000000;
                break;
            case 1:
                i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
                break;
            case 2:
                i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
                break;
            case 3:
                i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) << 8 | 0x80;
                break;
            }

            word_array.push(i);

            while ((word_array.length & 15) !== 14) {
                word_array.push(0);
            }

            word_array.push(str_len >>> 29);
            word_array.push((str_len << 3) & 0x0ffffffff);

            for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
                for (i = 0; i < 16; i++) {
                    W[i] = word_array[blockstart + i];
                }

                for (i = 16; i <= 79; i++) {
                    W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
                }

                A = H0;
                B = H1;
                C = H2;
                D = H3;
                E = H4;

                for (i = 0; i <= 19; i++) {
                    temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B, 30);
                    B = A;
                    A = temp;
                }

                for (i = 20; i <= 39; i++) {
                    temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B, 30);
                    B = A;
                    A = temp;
                }

                for (i = 40; i <= 59; i++) {
                    temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B, 30);
                    B = A;
                    A = temp;
                }

                for (i = 60; i <= 79; i++) {
                    temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
                    E = D;
                    D = C;
                    C = rotate_left(B, 30);
                    B = A;
                    A = temp;
                }

                H0 = (H0 + A) & 0x0ffffffff;
                H1 = (H1 + B) & 0x0ffffffff;
                H2 = (H2 + C) & 0x0ffffffff;
                H3 = (H3 + D) & 0x0ffffffff;
                H4 = (H4 + E) & 0x0ffffffff;
            }

            temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
            return temp.toLowerCase();
		},
		
		/**
		 * window의 높이를 구한다.
		 * 
		 * @returns {Number}
		 */
		getWinHeight: function() {
			return (_win.innerHeight ? _win.innerHeight : _doc.documentElement.clientHeight ? _doc.documentElement.clientHeight : _doc.body.clientHeight);
		},
		
		/**
		 * document의 높이를 구한다.
		 * 
		 * @returns {Number}
		 */
		getDocHeight: function() {
			return Math.max(
		        Math.max(_doc.body.scrollHeight, _doc.documentElement.scrollHeight),
		        Math.max(_doc.body.offsetHeight, _doc.documentElement.offsetHeight),
		        Math.max(_doc.body.clientHeight, _doc.documentElement.clientHeight)
			);
		},
		
		/**
		 * document scroll 값을 구한다.
		 * 
		 * @see YUI 2.8.1 
		 * @returns {Number}
		 */
		getDocScrollTop: function(doc) {
			doc = doc || _doc;
			return Math.max(doc.documentElement.scrollTop, doc.body.scrollTop);
		},
		
		/**
		 * document scroll의 현재 x,y 좌표값을 구한다.
		 * 
		 * @returns {Object} {x: 0, y: 0}
		 */
		getScrollPos: function() {
			var pos = {x:0, y:0};
			
			if (typeof _win.pageYOffset != 'undefined') {
				pos.x = _win.pageXOffset;
				pos.y = _win.pageYOffset;
			}
			else if (typeof _doc.documentElement.scrollTop != 'undefined' && _doc.documentElement.scrollTop > 0) {
				pos.x = _doc.documentElement.scrollLeft;
				pos.y = _doc.documentElement.scrollTop;
			}
			else if (typeof _doc.body.scrollTop != 'undefined') {
				pos.x = _doc.body.scrollLeft;
				pos.y = _doc.body.scrollTop;
			}
			
			return pos;
		},
		
		/**
		 * 사용자의 브라우저 환경정보 추출.
		 * 
		 * @param {Object} agent
		 * @returns {Object}
		 */
		getBrowserEnv: function(agent) {
			// UserAgent를 임의로 세팅한 경우 _agent를 그렇지 않은 경우 브라우저의 UserAgent값을 사용
			var a = (agent) ? agent : _navi.userAgent || _navi.vendor || _win.opera;
			var env = {};

			// 모바일 장비 여부
			env.mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4));

			// TODO 모바일 장비일 경우 벤더/디바이스 추출
			if (env.mobile) {
				env.mobileBrand = "";
				env.mobileDevice = "";
			}

			// 브라우저 대분류
			env.browserGroup = util.first({
				"ie": /MSIE/.test(a),
				"firefox": /Firefox/.test(a),
				"chrome": /Chrome/.test(a),
				"safari": /Safari/.test(a),
				"opera": /Opera/.test(a)
			}) || "unknown";
			
			// 브라우저 대분류에 따라 상세 버전 식별
			switch (env.browserGroup) {
				case "ie":
					env.browser = util.match(a, /MSIE [0-9\.]+/);
					break;
				case "firefox":
					env.browser = util.match(a, /Firefox\/[0-9\.]+/);
					break;
				case "chrome":
					env.browser = util.match(a, /Chrome\/[0-9\.]+/);
					break;
				case "safari":
					env.browser = util.match(a, /Safari\/[0-9\.]+/);
					break;
				case "opera":
					env.browser = util.match(a, /Opera\/[0-9\.]+/);
					break;
				default: 
					env.browser = "";
					break;
			}

			// 브라우저 언어.
			env.language = _navi.userLanguage || _navi.language || "";
			// OS언어는 UserAgent에 포함되지 않는 경우가 많아 파싱이 어려움. 브라우저 언어만 식별함

			// OS 대분류
			env.operatingSystemGroup = util.first({
				"ios": /like Mac OS X/.test(a),
				"android": /Android/.test(a),
				"windows": /Win/.test(a),
				"macintosh": /Mac/.test(a),
				"linux": /Linux/.test(a),
				"unix": /X11/.test(a)
			}) || "unknown";

			// OS 대분류에 따라 상세 버전 식별
			switch (env.operatingSystemGroup) {
				case "android":
					env.operatingSystem = util.match(a, /Android [^;]+/);
					break;
				case "ios":
					env.operatingSystem = util.match(a, /(iP(ad|hone); CPU .+) like Mac/, 1);
					break;
				case "windows":
					env.operatingSystem = util.match(a, /Windows [^;]+/);
					break;
				case "macintosh":
					env.operatingSystem = util.match(a, /Mac OS X [^;]+/);
					break;
				case "linux":
					env.operatingSystem = util.match(a, /X11; (Linux [^;]+)/, 1);
					break;
				case "unix":
					env.operatingSystem = "";
					break;
				default:
					break;
			}

			env.resolution = _screen.width + "x" + window.screen.height; // 사용자 해상도
			env.app = /B[ST]$/.test(a); // 앱 여부 (UserAgent가 ,BS 또는 ,BT 로 끝남)
			
			return env;
        }		
	};
	
	return {
		/**
		 * 주문페이지의 경우 주문정보를 설정한다.
		 * 
		 * @param {String} orderNumber
		 * @param {Array} orderItemList array에 적재될 데이터 구조 sample : [{itemId: "10400236", quantity: 1, price: 36800, dealNo: 14130}, {itemId: "10400236", quantity: 1, price: 36800}, ...]
		 * @param {Number} totalPay
		 */
		setOrderInfo: function(orderNum, orderItemList, totalPay) {
			_data.orderInfo = _data.orderInfo  || {};
			_data.orderInfo = {
				orderNum: orderNum,
				orderItemList: orderItemList,
				totalPay: totalPay
			};
			_data.pageInfo = _data.pageInfo || {};
			_data.pageInfo.pageType = 'ORDER';
		},
		
		/**
		 * 단품페이지의 경우 상품정보를 설정한다.
		 * 
		 * @returns
		 */
		setPrdInfo: function() {
			_data.pageInfo = _data.pageInfo || {};
			_data.pageInfo.pageType = 'PRD';
		},
		
		/**
		 * 장바구니 페이지의 경우 장바구니 상품정보를 설정한다.
		 * 
		 * @param {Array} cartItemList array에 적재될 데이터 구조 sample : [{itemId:"10385347", dealNo: "12345678"}, ...]
		 * @returns
		 */
		setCartInfo: function(obj) {
			_data.cartInfo = _data.cartInfo || {};
			
			var cartItemList = [].concat( obj );
		    var tempCartItemList = [];
		    if (cartItemList && cartItemList.length > 0) {
		        for (var i = 0 ; i < cartItemList.length ; i++) {
		            
		            var items = cartItemList[i].split(",");
		            //var items = cartItemList[i];
		            var tempObj = {};
		            if (items.length == 3) {
		                tempObj = {itemId: items[0], dealNo: items[1], addTime: items[2]};
		            } else {
		                tempObj = {itemId: items[0], dealNo: "null", addTime: "null"};
		            }
		            
		            tempCartItemList.push(tempObj);
		        }
		    }
			
			_data.cartInfo.cartItemList = tempCartItemList;
			_data.pageInfo = _data.pageInfo || {};
			_data.pageInfo.pageType = 'CART';
		},
		/**
		 * 수집된 버튼 이벤트 정보를 서버로 전송한다.
		 */
		sendBtnEvtInfo: function() {
			_data.buttonEventInfo.btnNameList = _data.buttonEventInfo.btnNameList || [];
			if (_data.buttonEventInfo.autoFlushInterval) {
				var evtCnt = _data.buttonEventInfo.btnNameList.length;
				if (evtCnt > 0) {
					log('button event send count : ' + evtCnt);
					// btnNames 스택에 저장된 값을 전송 후 비운다.
					var sendUri = '/'+ _globalConfig.clientId +'/' + _globalConfig.siteId + '/btnevt';
					report({url:sendUri});
					_data.buttonEventInfo.btnNameList = [];
					
					// 버튼 이벤트 전송은 document unload이벤트가 발생하지 않는 이상 계속해서 실행되어야 하므로 report시 skip되지 않도록 false로 값을 갱신한다.
					global.sendedUrl[sendUri] = false;
				}				
			}
		},
		/**
		 *  특정 이벤트를 받아 서버로 호출하는 메소드
		 */
		sendEvt: function(eventCode, eventValue) {
			var sendUri = '/'+ _globalConfig.clientId +'/' + _globalConfig.siteId + '/btnevt';
			// 버튼 이벤트 전송은다.
			report({url:sendUri, data: {eventCode:eventCode, eventValue:eventValue}});
			global.sendedUrl[sendUri] = false; //document unload이벤트가 발생하지 않는 이상 계속해서 실행되어야 하므로 report시 skip되지 않도록 false로 값을 갱신한
		},
		
		/**
		 * 버튼 이벤트 발생시 버튼명을 stack에 저장처리.
		 * @param btnname
		 * @returns
		 */
		setBtnEvtInfo: function(btnname) {
			//try {
			//	log('added button event : ' + btnname);
			//	_data.buttonEventInfo.autoFlush = _data.buttonEventInfo.autoFlush || false;
			//	if (!_data.buttonEventInfo.autoFlush) {
			//		_data.buttonEventInfo.autoFlush = true;
			//		_data.buttonEventInfo.autoFlushInterval = setInterval(function() {
			//			Wcs.sendBtnEvtInfo();
			//		}, _globalConfig.eventSendInterval);
			//	}
			//
			//	_data.buttonEventInfo =  _data.buttonEventInfo || {};
			//	_data.buttonEventInfo.btnNameList = _data.buttonEventInfo.btnNameList || [];
			//	_data.buttonEventInfo.btnNameList[_data.buttonEventInfo.btnNameList.length] = btnname;
			//
			//} catch (ex) {
			//	log('button event processing error : ' + ex);
			//}
		},	
		
		/**
		 * WCS 실행.
		 * 
		 * @param {Object} conf 설정정보
		 * @returns
		 */
		execute: function(conf) {
			try {
				var config = conf || {};
				var clientId = config.clientId;
				var siteId = config.siteId;
				
				log('is iframe page : ' + (_isIframe));
				
				if (clientId != '' && siteId != '' && !_isIframe) {					
					// A/B 테스트 분기를 위한 random number값
					var wcsRandomNum = util.getCookie(_globalConfig.cookie.randnum.key);
					log('exists random number : ' + wcsRandomNum);
					if (!wcsRandomNum) {
						wcsRandomNum = util.getRandomNumber(10000000, 99999999);
						log('new random number : ' + wcsRandomNum);
						util.setCookie(_globalConfig.cookie.randnum.key, wcsRandomNum, { expires: _globalConfig.cookie.randnum.expires });
					}
					
					/*
					 * reorder tracker실행. ( wcs 코드가 삽입되는 site에서 reorder tracker를 argument로 전달하지 않는 이유는
					 * reorder tracker추가 전달시 해당 site내에 wcs가 삽입된 모든곳을 다 고쳐야 하는 risk 발생으로 인함.
					 */
					//config.trackers.push({name: 'ReOrder'});
					
					var wcsTrackers = config.trackers;
					var trackerSize = config.trackers.length || 0;
					
					// page view를 구분짖는 유일값 생성.
					global.pageViewId = 'pvid-' + util.createPvid(_globalConfig.uuidLen);
					
					for (var tempIdx = 0 ; tempIdx < trackerSize ; tempIdx++) {
						
						var trackerObj = wcsTrackers[tempIdx];
						// tracker 클래스명
						var trackerName = trackerObj.name;
						// 수집대상 url패턴.
						var trackerIncludes = trackerObj.includes;
						// 수집대상 url패턴에 매치 되는 경우에만 트래커 실행.
						var isProcess = (function() {
							var tempResult = true;
							
							if (trackerIncludes && trackerIncludes.length > 0) {
								tempResult = false;
								var currentPath = _win.location.pathname;
								
								for (var includeIdx in trackerIncludes) {
									var includePath = trackerIncludes[includeIdx];
									var removeStarPath = includePath.replace(/\*/gi, '');
									
									if (includePath.startsWith('*') && includePath.endsWith('*')) {
										// includePath 앞뒤로 * 가 존재하는 경우 위치에 상관없이 존재하면 포함.
										if (currentPath.indexOf(removeStarPath) != -1) {
											tempResult = true;
										}
									} else if (includePath.startsWith('*')) {
										// includePath 앞에 * 가 존재하는 경우 includePath로 끝나면 포함.
										if (currentPath.endsWith(removeStarPath)) {
											tempResult = true;
										}									
									} else if (includePath.endsWith('*')) {
										// includePath 뒤에 * 가 존재하는 경우 includePathr로 시작하면 포함.
										if (currentPath.startsWith(removeStarPath)) {
											tempResult = true;
										}
									} else {
										if (currentPath == currentPath) {
											tempResult = true;
										}
									}
								}
							}
							return tempResult;
						})();
						if (isProcess) {
							try {
								// 사이트별 설정정보
								global.siteConfig = eval('_siteConfig.' + siteId);
								var trackerClass= new (eval(trackerName + 'Tracker'))(clientId, siteId, trackerIncludes);
								trackerClass.execute();
							} catch (e) {log(e.message);}					
						}
					}
				}				
			} catch (e) {
				log(e.message);
			}
		}		
	};
	
	//------------------------------------------------------------------------------------------------
	// [S] Tracker
	//------------------------------------------------------------------------------------------------
	
	/**
	 * 수집된 데이터를 서버로 전송처리한다.
	 * 
	 * @param conf 트래커 설정정보
	 * @param data 트래커가 수집한 정보
	 */
	function report(args) {		
		if (!args && args == null) {
			return;
		}
		
		// 공통 데이터와 merge처리.
		if (args.data && args.data != null) {
			args.data = mixin(args.data, _data);
		} else {
			args.data = _data;
		}
		
		global.sendedUrl = global.sendedUrl || [];
		var tempSendedUrl = global.sendedUrl[args.url];
		if (tempSendedUrl) {
			log('Skip the url because it is already sent : ' + args.url);
			return;
		}
		
		var docDomain = _doc.domain;
		
		if (_globalConfig.denyDomains && _globalConfig.denyDomains.length > 0) {
			for (var i = 0 ; i < _globalConfig.denyDomains.length ; i++) {
				if (docDomain.indexOf(_globalConfig.denyDomains[i]) != -1) {
					log(_globalConfig.denyDomains[i] + ' is denied!');
					return;
				}
			}
		}
		
		try {
			var procImage = new Image(1,1);
			
			//_procImage.src = _doc.location.protocol + '//' + _globalConfig.serverHost + (uri) + '?data=' + _encodeWrapper(util.stringify(data)) + '&WCS_DOC_LOC=' + _encodeWrapper(_doc.location.href);
			
			var convertData = util.stringify(args.data);
			// undefined value을 ""로 치환한다. ( for json validation )
			convertData = convertData.replace(/undefined/g, '\"\"');
			//log('convert data : ' + convertData);
			
//			var sendUrl = _doc.location.protocol + '//' + _globalConfig.serverHost + (args.url) + '?data=' + _encodeWrapper(convertData);
			//temp
			var sendUrl = 'http://' + _globalConfig.serverHost + (args.url) + '?data=' + _encodeWrapper(convertData);
			if (args.isAjax) {
				try {
					ajax(sendUrl, args.callback);
				} catch (ex) {
					log('ajax call error : ' + ex);
					// 에러가 발생해도 callback은 무조건 실행함.
					if (isFunction(args.callback)) {
						args.callback();
					}
				}
			} else {
				procImage.src = sendUrl;
			}
			log('sended data to : ' + args.url);
		} catch (e) {
			log('send error : ' + e);
		} finally {
			global.sendedUrl[args.url] = true;
		}
	}

	/**
	 * 고객행동 분석 트래커
	 * 
	 * @param clientId 클라이언트 아이디
	 * @param siteId 사이트 아이디
	 * @param includes 수집대상 url array default는 모든 페이지
	 */
	function WebanalyticsTracker(clientId, siteId, includes) {
		
		var data = {
			pageViewId: global.pageViewId,
			pageInfo: {
				currentUrl: _win.location.href,
				charset: _doc.charset || _doc.characterSet				
			},		
			userInfo: {
				revisit: true
			},
			environment: {
				
			}
		};
		
		data = mixin(data, _data);
		
		// 행동분석에 종속적인 설정 정보.
		var conf = {
			name: 'WebanalyticsTracker',
			url: '/'+ clientId +'/' + siteId + '/load',
			procEvent: 'load'
		};
		_globalConfig.clientId = clientId;
		_globalConfig.siteId = siteId;
		_globalConfig.includes = includes;
		
		mixin(conf, _globalConfig);
		
		return {
			execute: function() {
				if (_globalConfig.cookie.referer.use) {
					data.pageInfo.referrerUrl = util.getCookie(_globalConfig.cookie.referer.key) || _doc.referrer || "";
					util.setCookie(_globalConfig.cookie.referer.key, data.pageInfo.currentUrl, { expires: _globalConfig.cookie.referer.expires });
				}
				else {
					data.pageInfo.referrerUrl = _doc.referrer || "";
				}
				// 사용자 브라우저 환경정보.
				data.environment = util.getBrowserEnv();
				var pcid;
				var pcid = util.getCookie(_globalConfig.cookie.pcid.key);
				if (!pcid) {
					pcid = util.getUUID(_globalConfig.uuidLen);
					util.setCookie(_globalConfig.cookie.pcid.key, pcid, { expires: _globalConfig.cookie.pcid.expires });
					data.userInfo.revisit = false; // pcid가 세팅되지 않은 경우 재방문 아님
				}
				data.userInfo.pcId = pcid;
				
				// 해당 사이트의 userid쿠키 값이 함수형태인 경우 해당 함수를 실행.
				try {
					data.userInfo.userId = global.siteConfig.cookie.userid();
				} catch (e) {
					log('get userid error : ' + e);
				}
				log('get userid : ' + data.userInfo.userId);
				if (data.userInfo.userId == '') {
					data.userInfo.userId = util.getCookie(global.siteConfig.cookie.userid);
				}
				// 로그인 상태에서 얻어진 사용자 아이디가 존재하는 경우 해당 아이디를 wcs가 관리하는 유저아이디 쿠키생성.
				if (data.userInfo.userId != "" && data.userInfo.userId != "0") {
					util.setCookie(_globalConfig.cookie.userid.key, data.userInfo.userId, { expires: _globalConfig.cookie.userid.expires });
				} else {
					//log('비 로그인 상태로 wa_userid에서 호출 ');
					// 로그인상태가 아닌경우 wcs가 관리하는 사용자 아이디 쿠키값을 읽는다.
					data.userInfo.userId = util.getCookie(_globalConfig.cookie.userid.key);
					// 기존 쿠키에 userId '0' 인 경우, 쿠키 삭제.(catvid 0 으로 인해 발생한 쿠키)
 					if(data.userInfo.userId && data.userInfo.userId == "0") {
 						data.userInfo.userId = "";
						util.setCookie(_globalConfig.cookie.userid.key, data.userInfo.userId, { expires: _globalConfig.cookie.userid.expires });
					}
				}
				
				data.userInfo.visitorId = (data.userInfo.userId && data.userInfo.userId != "" && data.userInfo.userId != "0") ? data.userInfo.userId : data.userInfo.pcId;

				// 성별, 연령값
				data.userInfo.gender = util.getCookie("gd");
				data.userInfo.age = util.getCookie("yd");

				// SessionID 설정. 쿠키값이 존재하면 쿠키값을 사용, 아니면 새로 생성
				var sid = util.getCookie(_globalConfig.cookie.sessionid.key);
				if (!sid) {
					sid = data.pageInfo.visitedTime + "-" + data.userInfo.pcId;
				}
				// 무조건 현재 시점을 기준으로 만료시간 30분후로 쿠키 설정. (action이 계속해서 존재하는 경우 sessionId를 유지시키기 위
				util.setCookie(_globalConfig.cookie.sessionid.key, sid, { expires: _globalConfig.cookie.sessionid.expires });
				data.sessionId = sid;
				
				//app매체코드값
				data.appmediatype = util.getCookie(global.siteConfig.cookie.appmediatype) || '';
				
				//외부유입 매체코드값 
				data.mediatype = '';
				data.catvid = '';
				
				// wcs자체 관리하는 catvid 쿠키값
				var waCatvid = util.getCookie('wa_catvid') || '';
				
				var ecid = util.getCookie('ecid') || '';
				
				// ecid쿠키값이 존재하는 경우 즉 로그인 한 경우는 ecid값을 읽음.
				if (ecid && ecid != '') {
					var ecidValArray = ecid.split('~');
					
					/**
					 * ecid값에서 주어진 attrName에 해당되는 value를 찾는다.
					 */
					function getEcidValue(attrName) {
						var resultVal = '';
						for (var i = 0, j = ecidValArray.length ; i < j ; i++) {
							var tempValue = ecidValArray[i];
							if (tempValue.indexOf(attrName) != -1) {
								var tempSplits = tempValue.split('=');
								if (tempSplits && tempSplits.length == 2) {
									resultVal = tempSplits[1];
								}
								
								break;
							}
						}
						
						return resultVal;
					}
					
					// 4번째가 mediatype임.
					if (ecidValArray) {
						// media type 추출
						data.mediatype = getEcidValue('mediatype');
						
						// catvid(고객번호) 추출
						data.catvid = getEcidValue('catvid');
						// wa_catvid 쿠키값을 호출하여 catvid값과 틀린경우 갱신한다. 
						if (data.catvid != waCatvid) {
							util.setCookie('wa_catvid', data.catvid, { expires: 60*24*365*10 });
						}
					}
				} else {
					data.catvid = waCatvid;
				}
				
				// onload이전 데이터 전송시 위에 수집된 데이터를 이용하기 위해. _data에 mixed 한다.
				_data = mixin(_data, data);
				
				// onload를 통해서만 데이터 수집이 가능한 페이지와 아닌 경우를 판단하여 수집데이터 전송처리 실행.
				var sendOnload = _globalConfig.onlyOnload;
				if (!sendOnload) {
					var loadTargetArray = global.siteConfig.loadTarget;
					var isloadEvent = false;
					if (loadTargetArray != null && loadTargetArray.length > 0) {
						for (var i = 0 ; i < loadTargetArray.length ; i++) {
							// onload이벤트로 실행할 url과 매치되는 경우
							if (loadTargetArray[i].test(data.pageInfo.currentUrl)) {
								//log('onload target 매치 : ' + loadTargetArray[i]);
								//log('onload target 매치 : ' + data.pageInfo.currentUrl);
								isloadEvent = true;
								break;
							}
						}
					}
					
					sendOnload = isloadEvent;
				}
				
				if (sendOnload) {
					log('send data using onload');
					addEvent(_win, conf.procEvent, function() {
						report({url: conf.url});
					});					
				} else {
					log('send data using execute');
					report({url: conf.url});
				}
				
				var tempEvent = isFF() ? 'unload' : 'beforeunload';
				addEvent(_win, tempEvent, function() {
					// onload 이벤트가 발생하기전 사용자가 링크를 클릭하여 페이지를 이동하는 경우 onload데이터는 수집안되고, unload데이터만 수집되는 것을 방지하고함.
					log('send onload data on unload event : ' + conf.url);
					report({url: conf.url});
				});
			}
		};
	}
	
	/**
	 * 추천 분석 트래커
	 * 
	 * @param clientId 클라이언트 아이디
	 * @param siteId 사이트 아이디
	 * @param includes 수집대상 url array default는 모든 페이지
	 */
	function RecommendTracker(clientId, siteId, includes) {
		
		var data = {
			pageViewId: global.pageViewId,
			pageInfo: {
				visitedTime: new Date().getTime()
			},		
			pageInfoExt: {},
			exposedRecommPrdList: []
		};
		
		// 추천에 종속적인 설정 정보.
		var conf = {
			name: 'RecommendTracker',
			url: '/'+ clientId +'/' + siteId + '/leave',
			procEvent: (isFF() ? 'unload' : 'beforeunload'),
			scrollLatency: 200
		};
		
		_globalConfig.clientId = clientId;
		_globalConfig.siteId = siteId;
		_globalConfig.includes = includes;
		
		mixin(conf, _globalConfig);
		
		return {
			
			execute: function() {
				// 마우스 스크롤 카운트.
				global.scrollCount = 0;
				// 마우스 스크롤 범위.
				global.scrollRange = 0;
				// 화면 스크롤 방향.
				global.pageDirection = 'down';
				// 화면상 현재 스크롤 이전 위치값.
				global.prevY = 0;
				
				/**
				 * 스크롤 카운트 계산.
				 * 
				 * @param {Event} e Event
				 * @returns
				 */
				function parseScrollCount() {
					
					if (global.countTimer) {
						clearTimeout(global.countTimer);
					}
					global.countTimer = setTimeout(function() {
						var pos = util.getScrollPos();
						var currentY = pos.y;
						
						if (global.prevY < currentY && global.pageDirection == 'down') {
							global.pageDirection = 'up';
							global.scrollCount++;
						} else if (global.prevY > currentY && global.pageDirection == 'up') {
							global.pageDirection = 'down';
							global.scrollCount++;						
						}
						
						global.prevY = currentY;
						
						//log('current Y : ' + global.scrollCount);
						
					}, conf.scrollLatency);
				}
				
				/**
				 * 스크롤 범위 백분율 계산.
				 * 
				 * @returns
				 */
				function parseScrollPercent() {
					if (global.rangeTimer) {
						clearTimeout(global.rangeTimer);
					}
					global.rangeTimer = setTimeout(function() {
						// 페이지 높이 대비 화면 노출 정도
						var tempScrollRange = (100.0 * (util.getWinHeight() + util.getDocScrollTop()) / util.getDocHeight()) | 0;
						//현재 스크롤 범위 값이 이전 스크롤 범위보다 큰 경우만 저장.
						var pos = util.getScrollPos();
						if (global.scrollRange < tempScrollRange && pos.y > 0) {
							global.scrollRange = tempScrollRange;
						}
					}, conf.scrollLatency);
				}
				
				/**
				 * 장바구니 버튼 클릭시 
				 * @returns
				 */
				function bingEventToCartButton() {
					
					function isScriptAction(searchAction, functionName) {
						if (searchAction.indexOf(functionName) != -1) {
							return true;
						}
						return false;
					}
					
					
					var docA = _doc.getElementsByTagName('a');
					var cartFunctionInfo = eval('addCartEventInfo.'+ conf.siteId);
					
					if (docA != null && docA.length > 0) {
						for (var i = 0 ; i < docA.length ; i++) {
							//log('- node : ' + docA[i].href);
							
							// 해당사이트의 장바구니담기 버튼 or a 의 경우 클릭이벤트 바인딩.
							var scriptAction = docA[i].href;
							var isTargetEL = isScriptAction(scriptAction, cartFunctionInfo.functionName);
							if (!isTargetEL) {
								scriptAction = docA[i].attributes.getNamedItem('onclick').value;
								
								isTargetEL = isScriptAction(scriptAction, cartFunctionInfo.functionName);
							}
							
							//log('- script action : ' + scriptAction);
							// 장바구니 버튼인 경우 click이벤트 바인딩.
							if (isTargetEL) {
								addEvent(docA[i], 'click', function(e) {
									e.preventDefault();
								});								
							}
						}
					}
				}
				
				/**
				 * 추천상품 노출상품 파싱.
				 * 
				 * @returns
				 */
				function parseRecommendPrd() {
					var docA = _doc.getElementsByTagName('a');
					if (docA != null && docA.length > 0) {
						
						var exposedPrds = [];
						// 해당 사이트의 추천노출 정보.
						var tempRecommPrdInfo = global.siteConfig.recommPrd;						
						
						for (var i = 0 ; i < docA.length ; i++) {
							var hrefLink = docA[i].href;
							
							//log('- 매치대상 링크정보 : ' + hrefLink);
							//log('- 매치여부 : ' + tempRecommPrdInfo.linkPattern.test(hrefLink));
							
							// 해당 사이트의 추천상품 링크패턴과 일치하는지 판단.
							if (tempRecommPrdInfo.linkPattern.test(hrefLink)) {
								
								hrefLink = hrefLink.replace(/&amp;/, '&');
								
								var bnclickValue = getParamValue(hrefLink, 'bnclick');
								var bnclickValueSplit = bnclickValue.split('-');
								
								var exposeInfo = {rset: bnclickValueSplit[0], algo: bnclickValueSplit[1], prdid: '', planid: ''};
								if (!exposeInfo.algo) {
									exposeInfo.algo = '';
								}
								
								// 노출 상품아이디 파라미터 값을 얻는다.
								var exposeKey = getParamValue(hrefLink, tempRecommPrdInfo.params.product);
								if (exposeKey != null && exposeKey != '') {
									exposeInfo.prdid = exposeKey;									
								} else {
									// 상품아이디가 없는경우 추천기획전임.
									exposeKey = getParamValue(hrefLink, tempRecommPrdInfo.params.plan);
									exposeInfo.planid = exposeKey;
								}
								
								// 동일 추천링크를 배제하기 위한
								var isDuple = false;
								for (var m = 0 ; m < exposedPrds.length ; m++) {
									var tempExposeInfo = exposedPrds[m];
									if (tempExposeInfo.prdid == exposeKey || tempExposeInfo.planid == exposeKey) {
										isDuple = true;
										break;
									}
									
								}
								if (!isDuple) {
									exposedPrds[exposedPrds.length] = exposeInfo;
								}					
							}
						}
						
						data.exposedRecommPrdList = exposedPrds;
					}			
				}
				
				// 브라우저에 따라 마우스 휠 이벤트를 달리한다.  DOMMouseScroll
				var mousewheelevt = (/Firefox/i.test(_navi.userAgent)) ? 'DOMMouseScroll' : 'mousewheel';
				addEvent(_doc, mousewheelevt, function(e) {
					parseScrollCount();
					parseScrollPercent();				
				});
				
				// keydown 이벤트 바인딩.
				addEvent(_doc, 'keydown', function(evt) {
					var eventReference = (typeof evt !== "undefined") ? evt : event;
					if (util.contains(_globalConfig.event.keycode, eventReference.keyCode)) {
						parseScrollCount();
						parseScrollPercent();					
					}
				});
				
				// 스크롤 이벤트 바인딩
				addEvent(_doc, 'scroll', function(e) {
					parseScrollCount();
					parseScrollPercent();
				});
				
				// 추천 노출상품 정보 수집.
				addEvent(_win, 'load', function(e) {
					try{
						parseRecommendPrd();
					} catch (ex) {}
				});
				
				// 장바구니 클릭 이벤트 바인딩.
				/*
				addEvent(_win, 'load', function(e) {
					bingEventToCartButton();
				});
				*/
				addEvent(_win, conf.procEvent, function() {
					if (_data.buttonEventInfo.autoFlushInterval) {
						// interval을 클리어 시킨다.
						clearInterval(_data.buttonEventInfo.autoFlushInterval);
					}
					
					// 버튼 이벤트 수집정보 서버 전송. (FF의 경우 unload에 다중request가 실행되면 맨 마지막 request처리만 실행됨.)
					Wcs.sendBtnEvtInfo();
					
					data.pageInfoExt.durationTime = new Date().getTime() - data.pageInfo.visitedTime; // 페이지에 머문 시간 
					data.pageInfoExt.scrollUpDownCount = global.scrollCount;
					data.pageInfoExt.scrollRange = global.scrollRange;
					
					report({url: conf.url, data: data});
				});
			}
		};
	}

	//------------------------------------------------------------------------------------------------
	// [E] Tracker
	//------------------------------------------------------------------------------------------------	
	
})();