(function(Core){

	var ProductOptionSelected = function(){
		'use strict';

		var receiveToEvent = function(checkedOpt){
			var key = Object.keys(checkedOpt)[0];
			var index = 0;
			var resetIS = false;
			var objOpt = {};
			var currentIndex = 0;

			//currentSku 초기화하여 사용해야 하지만 returnToSkuData에 currentSku값을 사용하는 문제점 해결시 사용해야함
			//if(key === firstOptName) currentSku = allSkuData;
			//선택된 옵션 인덱스

			for(var i=0; i<optionData.length; i++){
				if(optionData[i].type === key){
					currentIndex = i;
					break;
				}
			}

			optionData.map(function(data, i, o){
				if(data.type === key) data.selectedValue = (checkedOpt[key] !== '' && checkedOpt[key] !== null) ? checkedOpt[key] : null;

				//선택된 다음 옵션들 리셋
				if(i > currentIndex) disabled(data.type, i);
				if(data.selectedValue === null){
					if(checkedOpt[key] !== '' && checkedOpt[key] !== null && index === i){
						//data.type : 다음 옵션 아이디
						//data : 다음 옵션 리스트
						//returnToSkuData(key, checkedOpt[key]) : sku 리스트
						nextOpt(data.type, data, returnToSkuData(objOpt));
					}
				}else{
					index++;
					objOpt[data.type] = data.selectedValue;

					if(index === o.length){
						if(productOptionType === 'multi') multiAddOption(objOpt);
						if(productOptionType === 'single') singleAddOption(objOpt);
					}
				}
			});
		}

		var returnToSkuData = function(objSkuValue){
			var arrData = [];
			var len = Object.keys(objSkuValue).length;
			var counter = 0;

			//currentSku값에서만 옵션을 체크하여 값을 전달 해야 하지만
			//currentSku값의 초기화 문제때문에 allSkuData의 배열을 이용해야한다.
			allSkuData.forEach(function(data){
				counter = 0;
				for(var key in objSkuValue){
					if(data[key] == objSkuValue[key]){
						counter++;
						if(counter === len){
							arrData.push(data);
						}
					}else{
						return false;
					}
				}
			});

			return arrData;
		}

		var nextOpt = function(componentID, opt, sku){
			currentSku = sku;
			optionDom[componentID].receiveToData(opt, sku);
		}

		var disabled = function(componentID, index){
			optionData[index].selectedValue = null;
			optionDom[componentID].disabled();
		}

		var noAddOption = function(){
			var obj = {};
			obj['qty'] = $('input[name=quantity]').eq(0).val();
			currentOptList = {};
			currentOptList['noOption'] = obj;
		}

		var singleAddOption = function(objOpt){
			var obj = {};
			var listKey = '';
			var sku = null;
			var counter = 0;

			sku = returnToSkuData(objOpt)[0];

			//옵션타입이 selectbox일때 value가 없는 즉, 선택하세요.. 이 선택될 경우 sku가 없으므로 리턴시킨다.
			if(!sku) return;

			obj['price'] = sku.price;
			obj['qty'] = $this.find('.qty').val();
			obj['maxQty'] = sku.quantity;
			obj['retailPrice'] = sku.retailPrice;
			obj['salePrice'] = sku.salePrice;
			obj['inventoryType'] = sku.inventoryType;
			obj['options'] = {};
			obj['upc'] = sku.upc;
			obj['id'] = sku.skuId;

			for(var optionKey in option){
				listKey += option[optionKey].values[option[optionKey].selectedValue];
				obj['options'][optionKey] = option[optionKey].values[option[optionKey].selectedValue];

				counter++;
			}

			currentOptList = {};
			currentOptList[listKey] = obj;

			if($submitBtn.hasClass('disabled') && submit){
				$submitBtn.removeClass('disabled');
			}

			if(obj.salePrice){
				var salePrice = parseInt(obj.salePrice.replace(/[￦,가-힣]/g, ''));
				var retailPrice = parseInt(obj.retailPrice.replace(/[￦,가-힣]/g, ''));

				if(salePrice < retailPrice){
					$salePrice.find('strong').text(obj.salePrice).data("price", salePrice);
					$retailPrice.text(obj.retailPrice);
					$('.marketing-msg').show();
				}else{
					$salePrice.find('strong').text(obj.retailPrice).data("price", obj.retailPrice);
					$retailPrice.text('');
					$('.marketing-msg').hide();
				}
			}else{
				$salePrice.find('strong').text(obj.retailPrice).data("price", obj.retailPrice);
				$retailPrice.text('');
			}
			$upcCode.data('upc', obj.upc);
			$upcCode.text(obj.upc);

			/*
				fireEvent 가 등록되기전에 호출하여 처음 이벤트는 발생하지 않는다 그래서 setTimeout으로 자체 딜레이를 주어 해결하였다.
				조금 위험한 방법아지만 해결방법을 찾기 전까지 사용해야 할꺼 같다.
			*/
			setTimeout(function(){
				__self.fireEvent('skuComplete', __self, [obj]);
			});
		}

		var multiAddOption = function(objOpt){
			console.log('multiAddOption');
		}

		var __self,
			$this,
			$optionWrap,
			$submitBtn,
			$titleWrap,
			$salePrice,
			$retailPrice,
			$upcCode,
			optionData = [],
			allSkuData = {},
			bundleDefaultSkuData = {},
			skuData = {},
			option = {},
			optionDom = {},
			qtyComponent = null,
			currentOptList = {},
			currentSku = [],
			currentSkuArray = [],
			productId = 0,
			submit = false,
			firstOptName = 'COLOR',
			productOptionType = 'step',
			selectOptAppendType = false,
			secondIS = false,
			objOptType,
			//isFireEvent = false,	// adobe 수정 요청 pdp default color click false 로 변경 '19-08-27
			isFireEvent = true,	    // 장바구니/바로구매 상품 담기지 않음으로 인해 true 로 변경 '20-01-10
			componentType,
			endPoint;

		var setting = {
			selector:'[data-component-product-option]',
			optionWrap:'.option-wrap',
			submitBtn:'[data-cartbtn]',
			radio:'[data-component-radio]',
			select:'[data-component-select]',
			quantity:'[data-component-quantity]',
			titlewrap:'.title-wrap',
			salePrice:'.price',
			retailPrice:'.price-sale',
			upcCode:'.upc-code'
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this; //closure, Dom Object 아님
				__self = _self;
				$this = $(setting.selector);
				$optionWrap = $this.find(setting.optionWrap);
				$submitBtn = $this.find(setting.submitBtn);
				$titleWrap = $this.find(setting.titlewrap);
				$salePrice = $this.find(setting.salePrice);
				$retailPrice = $this.find(setting.retailPrice);
				$upcCode = $this.find(setting.upcCode);

				objOptType = arguments[0];
				firstOptName = objOptType['data-component-product-option'].first;
				productOptionType = objOptType['data-component-product-option'].productType;
				selectOptAppendType = objOptType['data-component-product-option'].selectOptAppendType;
				componentType = objOptType['data-component-product-option'].componentType || 'component_radio';

				//['data-component-product-option', 'data-product-options', 'data-sku-data']

				productId = $this.attr('data-product-id') || false;
				optionData = objOptType['data-product-options'] //Core.Utils.strToJson($this.attr('data-product-options'));
				allSkuData = objOptType['data-sku-data'] //Core.Utils.strToJson($this.attr('data-sku-data'));
				bundleDefaultSkuData = Core.Utils.strToJson($this.attr('data-bundleDefaultSkuData-data'));
				endPoint = Core.getComponents('component_endpoint');

				//옵션 데이터 나누기 예) COLOR:{}, SIZE:{} ...
				for(var k in optionData){
					option[optionData[k].type] = optionData[k];

					for(var i=0; i<allSkuData.length; i++){
						allSkuData[i][optionData[k].type] = allSkuData[i].selectedOptions[k];
					}
				}

				$optionWrap.find('[data-brz-components-type]').each(function(i){
					optionDom[$(this).attr('data-brz-components-type')] = Core.getComponents(componentType, {context:$this, selector:this}, function(){
						this.addEvent('change', function(attributeValue, valueId, id, friendlyName){
							var obj = {};
							var _that = this;
							var $that = $(_that);
							var attributeName = $that.attr('data-attributename');
							var friendlyName = $that.attr('data-friendly-name');

							obj[$(_that).attr('name')] = valueId;
							receiveToEvent(obj);

							$optionWrap.find('input[type=hidden]').each(function(i){
								if($(this).attr('name') === 'itemAttributes['+attributeName+']'){
									$(this).val(attributeValue);
								}
							});

							/*
								fireEvent 가 등록되기전에 호출하여 처음 이벤트는 발생하지 않는다 그래서 setTimeout으로 자체 딜레이를 주어 해결하였다.
								조금 위험한 방법아지만 해결방법을 찾기 전까지 사용해야 할꺼 같다.
							*/

							setTimeout(function(){
								if(isFireEvent){
									_self.fireEvent('changeFirstOpt', _that, [firstOptName, $(_that).attr('name'), productId, attributeValue, valueId, id, friendlyName]);
									if( attributeName.toLowerCase() == 'color' ){
										endPoint.call( 'pdpColorClick', { color : attributeValue })
									}
								}
								isFireEvent = true;
								$that.closest('div').prev().find('.over-txt').text(friendlyName);
							});

						});
					});

					optionData[i]['name'] = $(this).attr('data-attribute-name');
				});

				/*
					optionDom : radio, selectbox 컴포넌트
					optionData : 상품의 총 옵션 ( COLOR, SIZE .... )
					allSkuData : 상품 옵션으로 생성된 총 SkuData
					처음 옵션 init 로드 후 allSkuData를 가지고 해당 quantity를 체크하여 옵션의 상태를 처리한다.
					firstOptName은 현 컴포넌트 arguments의 objOptType['data-component-product-option'].first의 값을 가지고 와서 처리 한다. 해당 값은 템플릿에서 newProductOption에서 첫번째, 즉
					iterator.first 옵션의 type이며, COLOR, SIZE 만 테스트가 되어 있는 상태라 다른 옵션타입을 사용 할 경우 테스트를 해야 한다.
					단, 단품이 아닐때 실행한다.
				*/
				if(optionData && $optionWrap.find('[data-brz-components-type]').length > 0) optionDom[firstOptName].receiveToData(optionData[0], allSkuData);


				//first option select trigger
				$optionWrap.find('.input-radio.checked > label').each(function(i){
					$(this).trigger('click');
				});

				return this;
			},
			setTrigger:function(optionName, value, valueId){
				isFireEvent = false;
				optionDom[optionName].trigger(value, valueId);
			},
			getValidateChk:function(msg){
				var arrIsValidateChk = [];
				var isValidate = false;
				for(var key in optionDom){
					isValidate = optionDom[key].getValidateChk();
					arrIsValidateChk.push(isValidate);
					if(isValidate){
						optionDom[key].getThis().prev().find('.msg').removeClass('msg-on').text('');
					}else{
						optionDom[key].getThis().prev().find('.msg').addClass('msg-on').text(msg);
					}
				}

				return (arrIsValidateChk.indexOf(false) === -1) ? true : false;
			},
			getProductId:function(){
				return productId;
			},
			getDefaultSkuData:function(){
				// 단품일경우 (옵션이 없는경우) defaultSku 가 담겨서 넘어온다.
				// bundleDefaultSkuData 의 length 가 > 1 일때 번들 상품으로 bundleDefaultSkuData 넘김
				return (bundleDefaultSkuData.length > 0) ? bundleDefaultSkuData : allSkuData;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_product_option'] = {
		constructor:ProductOptionSelected,
		attrName:['data-component-product-option', 'data-product-options', 'data-sku-data']
	}
})(Core);
