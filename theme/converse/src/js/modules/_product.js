(function(Core){
	Core.register('module_product', function(sandbox){
		var currentFirstOptValue = '';
		var currentQuantity = 1;
		var itemAttributes = '';
		var miniOptionIS = false;
		var objProductOption = {};
		var minOffsetTop = 30;
		var maxOffsetTop = 0;
		var args = null;
		var $this;
		var imgCurrentIndex;
		var productId = '';
		var skuId = '';
		var isQuickView = false;
		var isQuantity = true;
		var productOption;
		var quantity;
		var endPoint;

		var quantityCheck = function(inventoryType, maxQty){
			var obj = {isQuantity:false, maxQty:0}
			if(inventoryType !== 'UNAVAILABLE'){
				if(inventoryType === 'CHECK_QUANTITY'){
					obj.isQuantity = (maxQty > 0) ? true : false;
					obj.maxQty = maxQty;
				}else if(inventoryType === 'ALWAYS_AVAILABLE'){
					obj.isQuantity = true;
					obj.maxQty = null;
				}
			}else{
				obj.isQuantity = false;
				obj.maxQty = 0;
			}

			return obj;
		}

		var defaultSkuSetup = function(productOptComponents){
			var skuData, quantityState;

			if(quantity){
				if(Array.isArray(productOptComponents)){
					$.each(productOptComponents, function(i){
						skuData = this.getDefaultSkuData()[0];
						quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
						quantity[i].setMaxQuantity(quantityState.maxQty);
						isQuantity = quantityState.isQuantity;
					});
				}else{
					skuData = productOptComponents.getDefaultSkuData()[0];
					quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
					quantity.setMaxQuantity(quantityState.maxQty);
					isQuantity = quantityState.isQuantity;
				}
			}
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				productId = args.productId;
				endPoint = Core.getComponents('component_endpoint');

				var $dim = $('[data-miniOptionDim]');
				var guideModal = UIkit.modal('#guide', {modal:false});
				var miniCartModule = sandbox.getModule('module_minicart');
				var gallery = sandbox.getComponents('component_gallery', {context:$this});

				quantity = sandbox.getComponents('component_quantity', {context:$(document)}, function(i){
					var INDEX = i;
					this.addEvent('change', function(qty){
						for(var i=0;i<quantity.length;i++){
							if(i !== INDEX){
								quantity[i].setQuantity(qty);
							}
						}
					});
				});


				productOption = sandbox.getComponents('component_product_option', {context:$(document)}, function(i){ //product Option select components
					var CURRENT_INDEX = i;
					var INDEX = 0;
					var _self = this;
					var currentOptValue = '';
					var key = this.getProductId();

					if(!objProductOption.hasOwnProperty(key)){
						objProductOption[key] = [];
						INDEX = 0;
					}else{
						INDEX++;
					}
					objProductOption[key].push(this);

					this.addEvent('changeFirstOpt', function(firstOptName, optionName, productId, value, valueId, id){
						if(currentOptValue != value){
							currentOptValue = value;

							for(var i=0; i<objProductOption[productId].length; i++){
								if(i != INDEX){
									skuId = '';
									objProductOption[productId][i].setTrigger(optionName, value, valueId);
								}

								if(optionName !== 'COLOR'){
									objProductOption[productId][i].getValidateChk();
								}
							}

							if(optionName === 'COLOR'){
								gallery.setThumb(value);
							}
						}
					});

					this.addEvent('skuComplete', function(skuOpt){
						//args.isDefaultSku
						if(quantity){
							var quantityState = quantityCheck(skuOpt.inventoryType, skuOpt.maxQty);
							isQuantity = quantityState.isQuantity;
							skuId = skuOpt.id;

							if(args.isDefaultSku !== 'true'){
								if(Array.isArray(quantity)){
									quantity[CURRENT_INDEX].setQuantity(1);
									quantity[CURRENT_INDEX].setMaxQuantity(quantityState.maxQty);
								}else{
									quantity.setQuantity(1);
									quantity.setMaxQuantity(quantityState.maxQty);
								}
							}
						}
					});
				});


				/* isDefaultSku - true  ( option이 없는 경우, 번들상품일 경우 )  */
				if(args.isDefaultSku === 'true') defaultSkuSetup(productOption);

				/* cart Update */
				$('[data-add-item]').each(function(i){
					var INDEX = i;
					$(this).find(' > .btn-link').click(function(e){
						e.preventDefault();

						var validateChk = (args.isDefaultSku === 'true') ? true : false;
						var qty = 0;

						if(args.isDefaultSku === 'false'){
							if(Array.isArray(productOption)){
								$.each(productOption, function(i){
									validateChk = this.getValidateChk('옵션을 선택해 주세요.');
								});
							}else{
								validateChk = productOption.getValidateChk('옵션을 선택해 주세요.');
							}
						}

						if(Array.isArray(quantity)){
							qty = quantity[INDEX].getQuantity();
						}else{
							qty = quantity.getQuantity();
						}


						if(validateChk && isQuantity && qty != 0){
							var $form = $(this).closest('form');
							var actionType = $(this).attr('action-type');
							var url = $(this).attr('href');
							var itemRequest = BLC.serializeObject($form);
							itemRequest['productId'] = productId;
							itemRequest['quantity'] = qty;

							BLC.ajax({
								url:url,
								type:"POST",
								dataType:"json",
								data:itemRequest
							}, function(data, extraData){
								if(data.error){
									UIkit.modal.alert(data.error);
								}else{
									var cartData = $.extend( data, {productId : productId, quantity : qty, skuId : skuId });

									if(actionType === 'add'){
										endPoint.call('addToCart', cartData );
										miniCartModule.update();
										//PLP 페이지에서 MOBON 스크립트 호출
										if(marketingData.pageType == 'category'){
											var $mobonTarget = $('#minicart .section-minicart .cart-order_list .order-list');
											var mobonPnm = $mobonTarget.find('.order-info .tit').text();
											var mobonImg = $mobonTarget.find('.image-wrap img').attr('src');
											mobonImg = document.location.origin + mobonImg;
											var mobonPrice = $mobonTarget.find('.order-info .price-wrap .price-txt.price-txt-retail').text();
											mobonPrice = mobonPrice.replace(' ','').replace('원','').replace(',','');

											mobRfShopPlp(cartData.productId,mobonPnm,mobonImg,mobonPrice,marketingData.categoryInfo.name,'addCart');

											var mobonSc = document.createElement('script');
											mobonSc.setAttribute('src','https://cdn.megadata.co.kr/js/enliple_min2.js');
											mobonSc.setAttribute('defer', 'defer');
											mobonSc.setAttribute('onload', 'mobRfShopPlp()');
											document.getElementsByTagName('body')[0].appendChild(mobonSc);
										}
									}else if(actionType === 'modify'){
										var url = Core.Utils.url.removeParamFromURL( Core.Utils.url.getCurrentUrl(), $(this).attr('name') );
										endPoint.call('cartAddQuantity', cartData );
										window.location.assign( url );
									}else if('redirect'){
										endPoint.call('buyNow', cartData );
										window.location.assign( '/checkout' );
									}
								}
							});
						}else if(!isQuantity || qty == 0){
							UIkit.notify(args.errMsg, {timeout:3000,pos:'top-center',status:'warning'});
						}

					});
				});


				//scrollController
				var optionTop = 0;
				var scrollController = sandbox.scrollController(window, document, function(per, scrollTop){
					if(sandbox.reSize.getState() === 'pc'){
						var maxOffsetTop = this.getScrollTop($('footer').offset().top);
						var maxHeight = this.setScrollPer(maxOffsetTop);

						if(per < maxOffsetTop){
							$('.product-option-container').removeClass('absolute').removeAttr('style');
						}else if(per > maxOffsetTop){
							$('.product-option-container').addClass('absolute').css('top', maxHeight - scrollTop);
						}
					}else{
						$('.product-option-container').removeClass('absolute').removeAttr('style');
					}
				}, 'product');

				if(sandbox.utils.mobileChk){
					var scrollArea = sandbox.scrollController(window, document, function(percent, scrollTop){
						var maxOffsetTop = this.getScrollTop($('footer').offset().top);
						var maxHeight = this.setScrollPer(maxOffsetTop);

						if(percent < minOffsetTop && miniOptionIS){
							miniOptionIS = false;
							$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
							$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
							$dim.removeClass('active');
						}else if(percent >= minOffsetTop && percent <= maxOffsetTop && !miniOptionIS){
							miniOptionIS = true;
							$('.mini-option-wrap').stop().animate({bottom:0}, 200);
						}else if(percent > maxOffsetTop && miniOptionIS){
							miniOptionIS = false;
							$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
							$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
							$dim.removeClass('active');
						}
					}, 'miniOption');
				}

				$('.minioptbtn').click(function(e){
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product').addClass('active');
					$dim.addClass('active');
				});

				$('.mini-option-wrap').on('click', '.close-btn', function(e){
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
					$dim.removeClass('active');
				});


				//guide option modal
				$this.find('.option-guide').on('click', function(e){
					e.preventDefault();
					guideModal.show();
				});


				$('.uk-quickview-close').click(function(e){
					guideModal.hide();
					isQuickView = true;
				});

				guideModal.off('.uk.modal.product').on({
					'hide.uk.modal.product':function(){
						if(isQuickView){
							setTimeout(function(){
								$('html').addClass('uk-modal-page');
								$('body').css('paddingRight',15);
							});
						}
					}
				});

				//thefit converse에서만 씀
				//thefit GA 효율 측정
				/*var TFSOBJ = new TFS_B2B_LIB();
				TFSOBJ.init("4ba7dad3-8a76-401a-ab3b-fbfe3007342f");
				TFSOBJ.setSelectedButtonClickExternalFunction(function(){
					ga('master.send', 'event', '버튼클릭', '사이즈선택', '더핏', {nonInteration: true});
				});*/
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-product]',
					attrName:'data-module-product',
					moduleName:'module_product',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			destroy:function(){
				$this = null;
				args = [];
				productOption = null;
				quantity = null;
			}
		}
	});
})(Core);
