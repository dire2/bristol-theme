(function(Core){
	Core.register('module_filter_converse', function(sandbox){
		'use strict';

		var $filter, args, arrInputPrice = [], arrQuery = [];
		var Method = {
			moduleInit:function(){
				args = arguments[0];
				$filter = $(this);

				//초기 query 분류
				//arrQuery = sandbox.utils.getQueryParams(location.href, 'array');
				var query = sandbox.utils.getQueryParams(location.href);
				for(var key in query){
					if(key !== 'page'){
						if(typeof query[key] === 'string'){
							arrQuery.push(key+'='+query[key]);
						}else if(typeof query[key] === 'object'){
							for(var i=0; i < query[key].length; i++){
								arrQuery.push(key+'='+query[key][i]);
							}
						}
					}
				}

				// 필터 클릭 처리
				sandbox.getComponents('component_radio', {context:$filter, unlock:true}, function(i){
					var currentValue = '';

					//처음 라디오 박스에 체크 되었을때만 이벤트 발생
					this.addEvent('init', function(){
						var val = this.attr('name') +'='+ encodeURIComponent($(this).val());
						currentValue = val;
					});

					this.addEvent('click', function(input){
						var val = $(input).attr('name') +'='+ encodeURIComponent($(input).val());
						if($(this).parent().hasClass('checked')){
							arrQuery.splice(arrQuery.indexOf(val), 1);
						}else{
							if(currentValue !== '') arrQuery.splice(arrQuery.indexOf(currentValue), 1);
							arrQuery.push(val);
							currentValue = val;
						}

						Method.appendCateItemList();
					});
				});

				//filter tab
				sandbox.getComponents('component_tabs', {context:$filter}, function(i){
					this.addEvent('tabClick', function(index){
						if(!$('#filter-container').children().eq(index).hasClass('open')){
							$('#filter-container').children().eq(index).addClass('open').siblings().removeClass('open');
						}else{
							$('#filter-container').children().eq(index).removeClass('open');
						}
					});
				});

				//필터 동작
				$(document).on('click', '.filter-remove-btn', function(e){
					e.preventDefault();
					location.href = location.pathname;
				});

				$filter.on('click', '.remove-refinement', function(e){
					e.preventDefault();

					var val = $(this).attr('data-filter') +'='+ encodeURIComponent($(this).attr('data-filter-val'));
					arrQuery.splice(arrQuery.indexOf(val), 1);
					Method.appendCateItemList();
				});


				var currentBrandQuery = ($('.brandSegment').attr('data-brand-query')) ? 'br=' + encodeURIComponent($('.brandSegment').attr('data-brand-query')) : '';
				$('.brandSegment').find('a').click(function(e){
					e.preventDefault();
					var val = $(this).attr('data-filter') +'='+ encodeURIComponent($(this).attr('data-value'));
					if($(this).parent().hasClass('selected')){
						arrQuery.splice(arrQuery.indexOf(val), 1);
					}else{
						if(currentBrandQuery !== '') arrQuery.splice(arrQuery.indexOf(currentBrandQuery), 1);
						arrQuery.push(val);
						currentBrandQuery = val;
					}

					Method.appendCateItemList();
				});
			},
			appendCateItemList:function(){
				window.location.assign( location.pathname + (sandbox.getModule('module_pagination').getPagingType() === 'number' ? '?page=1&' : '?') + arrQuery.join('&') );
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-filter-converse]',
					attrName:['data-module-filter-converse'],
					moduleName:'module_filter_converse',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
