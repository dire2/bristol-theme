(function(Core){
	Core.register('module_product_detail', function(sandbox){
		var $that;
		var arrViewLineClass=['uk-width-medium-1-3', 'uk-width-large-1-2', 'uk-width-large-1-3', 'uk-width-large-1-4', 'uk-width-large-1-5'];
		var Method = {
			moduleInit:function(){
				$this = $(this);
				if(Core.Utils.mobileChk) {
					//상품평에 리뷰개수 표시
					var cnt = $("#detail-review").find(".star-review-num").attr("data-totalratingcount");
					$("#detail-review").find(".detail-title").text('상품평 ('+cnt+')');
					$("#detail-review").find(".detail-title").append("<i class='icon-arrow_bottom'></i>");
					$("#detail-review2Pc").hide();
					$("#detail-container").attr('data-uk-accordion', '{showfirst:true, duration:300, collapse:false, animate:false}');
					$("#detail-container").removeClass("uk-switcher").addClass("uk-accordion");
					$(".detail-cont-inner").addClass("uk-accordion-content");				
				} else {
					$("#detail-review2Pc").show();
					$("#detail-container").removeData('data-uk-accordion');
					$("#detail-container").removeClass("uk-accordion").addClass("uk-switcher");
					$(".detail-cont-inner").removeClass("uk-accordion-content");					
				}
			}
		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-product-detail]',
					attrName:'data-module-product-detail',
					moduleName:'module_product_detail',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
