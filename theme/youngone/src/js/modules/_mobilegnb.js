(function(Core){
	Core.register('module_mobilegnb', function(sandbox){

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var args = arguments[0];
				var clickIS = false;
				var isSearch = false;

				var $modile = $('#mobile-menu');
				$modile.find('.mobile-onedepth_list').on('click', '> a', function(e){
					if(!$(this).hasClass('link')){
						e.preventDefault();
						//$(this).siblings().show().stop().animate({'left':0}, 300);
					}
				});

				$modile.find('.location').on('click', function(e){
					e.preventDefault();
					$(this).parent().stop().animate({'left':-270}, 300, function(){
						$(this).css('left', 270).hide();
					});
				});

				$('.gnb-search-btn').click(function(e){
					e.preventDefault();
					$('.search-panel, .gnb-search-field').css('display', 'block');
					$('.search-field').find('input[type=search]').focus();
				});


				$modile.on('show.uk.offcanvas', function(event, area){
				    Core.ga.pv('pageview', '/mobileMenu');
				});

				$modile.on('hide.uk.offcanvas', function(event, area){
					if(isSearch){
						sandbox.getModule('module_search').searchTrigger();
					}
					isSearch = false;
				});

				$modile.find('.mobile-lnb-search').on('click', function(e){
					e.preventDefault();
					isSearch = true;
					UIkit.offcanvas.hide();
				});
			}
		}
		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-mobilegnb]',
					attrName:'data-module-mobilegnb',
					moduleName:'module_mobilegnb',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
