(function(Core){
	Core.register('module_gnb', function(sandbox){

		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var $oneDepth = $('.onedepth-list');
				var args = arguments[0]; 
				var rotationWords = new Array(), rollingTimer, rotationIndex = 0, searchTxt = '';
				if(args.type === 'type1'){
					var timeoutId = null
					$oneDepth.on({
						'mouseenter.lnb':function(){
							clearInterval( timeoutId );
							$(this).find('>').addClass('active');
							$(this).siblings().find('>').removeClass('active');
						},
						'mouseleave.lnb':function(){
							var $this = $(this);
							timeoutId = setTimeout( function(){
								$this.find('>').removeClass('active');
							}, 300);
						},
						'click.lnb':function(e){
							var href = $(this).attr("href");
							if( href == "#" || href == "javascript:;" ){
								e.preventDefault();
								$(this).find('>').addClass('active');
							}
						}
					});
				}else if(args.type === 'type2'){
					$oneDepth.on({
						'mouseenter.lnb':function(){
							$(this).find('>').addClass('active');
							$(this).find('.header-menu_twodepth').css({'display':'block'});
							$(this).find('.menu-banner-conts').css({'display':'block'});
						},
						'mouseleave.lnb':function(){
							$(this).find('>').removeClass('active');
							$(this).find('.header-menu_twodepth').removeAttr('style');
							$(this).find('.menu-banner-conts').removeAttr('style');
						},
						'click.lnb':function(e){
							var href = $(this).attr("href");
							if( href == "#" || href == "javascript:;" ){
								e.preventDefault();
								$(this).find('>').addClass('active');
							}
						}
					});
				}

				var $modile = $('#mobile-menu');
				$modile.find('.mobile-onedepth_list').on('click', '> a', function(e){
					if(!$(this).hasClass('link')){
						e.preventDefault();
						$(this).siblings().show().stop().animate({'left':0}, 300);
					}
				});

				$modile.find('.location').on('click', function(e){
					e.preventDefault();
					$(this).parent().stop().animate({'left':-270}, 300, function(){
						$(this).css('left', 270).hide();
					});
				});

				/* 검색어 롤링 */
				//인기검색어 목록 얻어옴
				if($("ul#favorite-keyword-header li").length > 1){
					$('#favorite-keyword-header').find("li").each( function(){
						if($(this).text().trim() != ""){
							rotationWords.push($(this).text());
						}
					});
				}else if($("ul#favorite-keyword-header li").length == 1){
					if($('#favorite-keyword-header li:first').text() != ""){						
						$("#search-header").val($('#favorite-keyword-header li:first').text());
						searchTxt = $('#favorite-keyword-header li:first').text();
					}
				}else{
					$("label[for='search-header']").text("검색어 입력");
				}

				//인기검색어 롤링
				function rollingSearchWord(){
					if(rotationIndex == rotationWords.length){
						rotationIndex = 0;
					}
					var word = rotationWords[rotationIndex++];	
					$('#search-header').val(word);
					searchTxt = word;
				}
				//인기검색어 롤링 시작
				function startRollingSearchWord(){
					if(rotationWords.length > 0){
						rollingTimer = setInterval(rollingSearchWord, 5000);
						//5초 후에 첫 검색어가 표시됨. 바로 표시되도록 함.
						rollingSearchWord();
					}
				}
				//인기검색어 롤링 종료
				function endRollingSearchWord(){
					clearInterval(rollingTimer);
				}

				$('.gnb-search-field a').mouseover(function(){
					endRollingSearchWord();
					searchTxt = $('#search-header').val();
				});				
				$('.gnb-search-field a').mouseleave(function(){
					//검색어 롤링 재시작
					startRollingSearchWord();
				});				

				/* search */
				$('.gnb-search-field a').click(function(e){
					e.preventDefault();
					$('.search-panel').css('display', 'block');
					$('.search-field').find('input[type=search]').focus();
					if(searchTxt != ""){
						$("label[for='search']").text('');
						$('.search-field').find('input[type=search]').val(searchTxt);
					}					
					$('body').append('<div class="dimed"></div>');
				});

				$('.btn-search-close').click(function(e){
					e.preventDefault();
					$('.search-panel').removeAttr('style');
					$('.dimed').remove();
				});

				$(document).on('click', '.dimed', function(e){
					e.preventDefault();
					$('.search-panel').removeAttr('style');
					$(this).remove();
				});

				//검색어 롤링 시작
				startRollingSearchWord();
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-gnb]',
					attrName:'data-module-gnb',
					moduleName:'module_gnb',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
