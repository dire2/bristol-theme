(function(Core){
	var ThumbNail = function(){
		var setting = {
			selector:'[data-component-thumbnail-mk]',
			container:'.thumb-wrap',
			scrollWrap:'.scroll-Wrap',
			list:'.thumb-list'
		}

		var $this, $container, $list, $zoomGalleryWrap, arrThumbList=[], iScroll, args, currentScrollCount=0,
			prevCode, nextCode, arrGalleryOffsetTop, $galleryWrap, $mobileGalleryWrap, setTimeId, arrthumbClickOffsetTop = [], galleryWrapHeight=0;

		var Closure = function(){}
		Closure.prototype.setting = function(){
			var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
		}
		Closure.prototype.init = function(){
			var _self = this;

			$this = $(setting.selector);
			$galleryWrap = $this.siblings().filter('[data-gallery-pc]');
			$mobileGalleryWrap = $this.siblings().filter('[data-gallery-mobile]');
			$zoomGalleryWrap  = $this.siblings().filter('[data-gallery-zoom]');
			$container = $this.find(setting.container);
			$list = $this.find(setting.list);
			args = arguments[0];

			var arrList = [];

			$container.find(setting.list).each(function(i){
				var data = Core.Utils.strToJson($(this).attr('data-thumb'), true);
				var imgUrl = $(this).find('img').attr('src').replace(/\?[a-z]+/g, '');
				var imgAlt = $(this).find('img').attr('alt') || '';
				var pushIS = true;

				data.thumbUrl = imgUrl;
				data.imgAlt = imgAlt;

				/* 중복 이미지 처리 */
				for(var i=0; i < arrList.length; i++){
					if(arrList[i].thumbSort === data.thumbSort && arrList[i].thumbUrl === data.thumbUrl){
						pushIS = false;
						return;
					}
				}

				if(pushIS){
					arrList.push(data);
					arrThumbList.push(data);
				}
			});

			$galleryWrap.click(function(){
				$('html').addClass('uk-modal-page');
				$('body').css('paddingRight', 15);
				$zoomGalleryWrap.addClass('show');
				$zoomGalleryWrap.scrollTop($(document).scrollTop());
			});

			$zoomGalleryWrap.click(function(){
				if($('#quickview-wrap').length <= 0){
					$('html').removeClass('uk-modal-page');
					$('body').removeAttr('style');
				}
				$(this).removeClass('show');
			});

			this.setThumb(args.sort);
			return this;
		}
		Closure.prototype.setThumb = function(sort){
			var _self = this;
			var sortType = sort || args.sort;
			var arrThumbData = arrThumbList.filter(function(item, index, array){
				if(item.thumbSort === sortType || item.thumbSort === 'null'){
					return item;
				}
			});

			var galleryTemplate = Handlebars.compile($("#product-gallery-template").html())(arrThumbData);
			var thumbTemplate = Handlebars.compile($("#product-thumb-template").html())(arrThumbData);
			var mobileTemplate = Handlebars.compile($("#product-gallery-template-mobile").html())(arrThumbData);
			var zoomTemplate = Handlebars.compile($("#product-gallery-zoom").html())(arrThumbData);

			$galleryWrap.empty().append(galleryTemplate);
			$container.empty().append(thumbTemplate);
			$mobileGalleryWrap.empty().append(mobileTemplate);
			$zoomGalleryWrap.find('.gallery-images').empty().append(zoomTemplate);


			Core.moduleEventInjection(mobileTemplate);
		}
		Closure.prototype.setPosition = function(state){
			if(state == 'fixed'){
				$this.removeClass('absolute').addClass('fixed');
			}else if(state == 'absolute'){
				$this.removeClass('fixed').addClass('absolute').css('bottom', 460);
			}
		}
		Closure.prototype.getGalleryHeight = function(){
			return galleryWrapHeight;
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_thumbnail_mk'] = {
		constructor:ThumbNail,
		attrName:'data-component-thumbnail-mk'
	};
})(Core);
