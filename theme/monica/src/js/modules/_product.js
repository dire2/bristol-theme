(function(Core){
	Core.register('module_product_monica', function(sandbox){
		var currentFirstOptValue = '';
		var currentQuantity = 1;
		var itemAttributes = '';
		var miniOptionIS = false;
		var objProductOption = {};
		var minOffsetTop = 30;
		var maxOffsetTop = 0;
		var args = null;
		var $this;
		var imgCurrentIndex;
		var productId = '';
		var isQuickView = false;
		var isQuantity = true;
		var productOption;
		var textField;
		var quantity;

		var quantityCheck = function(inventoryType, maxQty){
			var obj = {isQuantity:false, maxQty:0}
			if(inventoryType !== 'UNAVAILABLE'){
				if(inventoryType === 'CHECK_QUANTITY'){
					obj.isQuantity = (maxQty > 0) ? true : false;
					obj.maxQty = maxQty;
				}else if(inventoryType === 'ALWAYS_AVAILABLE'){
					obj.isQuantity = true;
					obj.maxQty = null;
				}
			}else{
				obj.isQuantity = false;
				obj.maxQty = 0;
			}

			return obj;
		}

		var defaultSkuSetup = function(productOptComponents){
			var skuData, quantityState;

			if(quantity){
				if(Array.isArray(productOptComponents)){
					$.each(productOptComponents, function(i){
						skuData = this.getDefaultSkuData()[0];
						quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
						quantity[i].setMaxQuantity(quantityState.maxQty);
						isQuantity = quantityState.isQuantity;
					});
				}else{
					skuData = productOptComponents.getDefaultSkuData()[0];
					quantityState = quantityCheck(skuData.inventoryType, skuData.quantity);
					quantity.setMaxQuantity(quantityState.maxQty);
					isQuantity = quantityState.isQuantity;
				}
			}
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				productId = args.productId;

				var $dim = $('[data-miniOptionDim]');
				var guideModal = UIkit.modal('#guide', {modal:false});
				var miniCartModule = sandbox.getModule('module_minicart');
				var gallery = sandbox.getComponents('component_thumbnail_mk', {context:$this});

				quantity = sandbox.getComponents('component_quantity', {context:$(document)}, function(i){
					var INDEX = i;
					this.addEvent('change', function(qty){
						for(var i=0;i<quantity.length;i++){
							if(i !== INDEX){
								quantity[i].setQuantity(qty);
							}
						}
					});
				});

				textField = sandbox.getComponents('component_textfield', {context:$(document), selector:'.option-input-type'}, function(i){
					var CURRENT_INDEX = i;

					this.addEvent('focusout', function(){
						for(var i=0; i<textField.length; i++){
							if(i != CURRENT_INDEX){
								textField[i].setValue($(this).val());
							}
						}
					});
				});

				productOption = sandbox.getComponents('component_product_option', {context:$(document)}, function(i){ //product Option select components
					var CURRENT_INDEX = i;
					var INDEX = 0;
					var _self = this;
					var currentOptValueId = '';
					var key = this.getProductId();
					if(!objProductOption.hasOwnProperty(key)){
						objProductOption[key] = [];
						INDEX = 0;
					}else{
						INDEX++;
					}
					objProductOption[key].push(this);

					this.addEvent('changeFirstOpt', function(firstOptName, optionName, productId, value, valueId, id, friendlyName){
						if(currentOptValueId != valueId){
							currentOptValueId = valueId;

							for(var i=0; i<objProductOption[productId].length; i++){
								if(i != CURRENT_INDEX){
									objProductOption[productId][i].setTrigger(optionName, value, valueId);
								}
							}

							if(optionName === 'COLOR'){
								gallery.setThumb(value);

								/* 상품상세 stone 설명가이드 */
								sandbox.utils.ajax('/processor/execute/guide', 'GET', {
									'name':friendlyName,
									'productId':productId,
									'mode':'template',
									'templatePath':'/modules/guide',
									'resultVar':'guide',
									'cache':new Date().getTime()
								}, function(data){
									var resultData = data.responseText;
									if(resultData.replace(/[\s\n]/g, '') !== '') {
										$('#stone').removeClass('stone-guide').find('.discription').html(resultData);
									}
								}, false, true);
							}
						}

						//console.log( "changeFirstOpt");
						if( _.isFunction(marketingUpdateProductInfo)){
							marketingUpdateProductInfo();
						}

					});

					this.addEvent('skuComplete', function(skuOpt){
						//args.isDefaultSku
						if(quantity){
							var quantityState = quantityCheck(skuOpt.inventoryType, skuOpt.maxQty);
							isQuantity = quantityState.isQuantity;

							if(args.isDefaultSku !== 'true'){
								if(Array.isArray(quantity)){
									quantity[CURRENT_INDEX].setQuantity(1);
									quantity[CURRENT_INDEX].setMaxQuantity(quantityState.maxQty);
								}else{
									quantity.setQuantity(1);
									quantity.setMaxQuantity(quantityState.maxQty);
								}
							}
						}

						//console.log( "skuComplete");
						if( _.isFunction(marketingUpdateProductInfo)){
							marketingUpdateProductInfo();
						}
					});
				});


				/* isDefaultSku - true  ( option이 없는 경우, 번들상품일 경우 )  */
				if(args.isDefaultSku === 'true') defaultSkuSetup(productOption);

				/* cart Update */
				$('[data-add-item]').each(function(i){
					var INDEX = i;
					$(this).find(' > .btn-link').click(function(e){
						e.preventDefault();

						var validateChk = (args.isDefaultSku === 'true') ? true : false;
						var qty = 0;

						if(args.isDefaultSku === 'false'){
							if(Array.isArray(productOption)){
								$.each(productOption, function(i){
									validateChk = this.getValidateChk('옵션을 선택해 주세요.');
								});
							}else{
								validateChk = productOption.getValidateChk('옵션을 선택해 주세요.');
							}


						}

						if(Array.isArray(quantity)){
							qty = quantity[INDEX].getQuantity();
						}else{
							qty = quantity.getQuantity();
						}


						if(validateChk && isQuantity && qty != 0){
							var $form = $(this).closest('form');
							var actionType = $(this).attr('action-type');
							var url = $(this).attr('href');
							var itemRequest = BLC.serializeObject($form);
							itemRequest['productId'] = productId;
							itemRequest['quantity'] = qty;

							BLC.ajax({
								url:url,
								type:"POST",
								dataType:"json",
								data:itemRequest
							}, function(data, extraData){
								if(data.error){
									UIkit.modal.alert(data.error);
								}else{
									if( _.isFunction( marketingAddCart )){
										marketingAddCart();
									}
									if(actionType === 'add'){
										miniCartModule.update();
									}else if(actionType === 'modify'){
										var url = Core.Utils.url.removeParamFromURL( Core.Utils.url.getCurrentUrl(), $(this).attr('name') );
										window.location.assign( url );
									}else if('redirect'){
										window.location.assign( data.redirectUrl );
									}
								}
							});
						}else if(!isQuantity || qty == 0){
							UIkit.notify(args.errMsg, {timeout:3000,pos:'top-center',status:'warning'});
						}

					});
				});


				//scrollController
				var scrollArea = sandbox.scrollController(window, document, function(percent){
					var maxOffsetTop = this.getScrollTop($('footer').offset().top);
					var maxHeight = this.setScrollPer(maxOffsetTop);

					if(percent < minOffsetTop && miniOptionIS){
						miniOptionIS = false;
						$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
						$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
						$dim.removeClass('active');
					}else if(percent >= minOffsetTop && percent <= maxOffsetTop && !miniOptionIS){
						miniOptionIS = true;
						$('.mini-option-wrap').stop().animate({bottom:0}, 200);
					}else if(percent > maxOffsetTop && miniOptionIS){
						miniOptionIS = false;
						$('.mini-option-wrap').stop().animate({bottom:-81}, 200);
						$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
						$dim.removeClass('active');
					}
				}, 'miniOption');

				$('.minioptbtn').click(function(e){
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product').addClass('active');
					$dim.addClass('active');
				});

				$('.mini-option-wrap').on('click', '.close-btn', function(e){
					e.preventDefault();
					$('.mini-option-wrap').find('.info-wrap_product').removeClass('active');
					$dim.removeClass('active');
				});


				//guide option modal
				$this.find('.option-guide').on('click', function(e){
					e.preventDefault();
					guideModal.show();
				});


				$('.uk-quickview-close').click(function(e){
					guideModal.hide();
					isQuickView = true;
				});

				guideModal.off('hide.uk.modal.product').on({
					'hide.uk.modal.product':function(){
						if(isQuickView){
							setTimeout(function(){
								$('html').addClass('uk-modal-page');
								$('body').css('paddingRight',15);
							});
						}
					}
				});

			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-product-monica]',
					attrName:'data-module-product-monica',
					moduleName:'module_product_monica',
					handler:{context:this, method:Method.moduleInit}
				});
				//console.log( "product init");
				if( _.isFunction(marketingUpdateProductInfo)){
					marketingUpdateProductInfo();
				}
			},
			destroy:function(){
				$this = null;
				args = [];
				productOption = null;
				quantity = null;
				textField = null;
			}
		}
	});
})(Core);
