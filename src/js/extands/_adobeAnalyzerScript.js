(function(Core){
	var marketingData = _GLOBAL.MARKETING_DATA();
	var isSetBase = false;
	var eventList = [];
	var useAa = marketingData.useAa;
	var traceType = "";
	var pageName = "";
	var pageType = "";
	var breadcrumb = "";
	var processorUrl = "/processor/execute/adobe_enhanced_ec";

	function isValid() {
		return useAa;
	}
	function updateMarketingData(){
		marketingData = _GLOBAL.MARKETING_DATA();
	}
	function pv(){
		if( isValid()){
			traceType = "default";
			pageName = marketingData.pathName.split('/');
			pageType = marketingData.pageType;
			breadcrumb = marketingData.pathName;
			var newAcount = false;
			var newLogin = false;

			if( marketingData.pathName.length == 1 ){
				pageName = pageName[0];
			}else{
				pageName = pageName[ pageName.length - 1 ];
			}

			if( marketingData.pageType == 'home' ){
				pageName = 'Home';
				breadcrumb = 'Home';
			}

			if( marketingData.pageType == 'registerSuccess'){
				//newAcount = true;
				traceType = 'createAccount';
				setEventData( 'newAccountCreated', {});
			}
			
			if( marketingData.pageType == 'content'){
				if(pageName == 'mypage'){
					pageType = 'my account';
				}
			}

			var param = getPageData( traceType, pageName, pageType, breadcrumb, newAcount, newLogin );
			processor( param );
		}
	}
	function commerce( type ){
		updateMarketingData();
		var param = {};

		switch( type ){
			case 'category' :
				if( marketingData.categoryInfo == null ){
					return;
				}
				var categoryInfo = marketingData.categoryInfo;
				var itemList = marketingData.itemList;

				if( categoryInfo.paging == null ){
					return;
				}

				if( itemList != null ){
					var productIds = $.map(itemList, function(item){ return item.id });
					param.productIds = String(productIds);
				}					

				traceType = 'plp';
				pageName = categoryInfo.name;
				pageType = 'plp';
				breadcrumb = getBreadcrumbData(categoryInfo.breadcrumbs);

				/*
				var page = categoryInfo.paging.page - 1;
				var pageSize = categoryInfo.paging.pageSize;
				param.categoryId = categoryInfo.id;
				param.limit = pageSize;
				param.offset = page * pageSize;
				param.url = marketingData.pathName;
				*/

				param.categoryId = categoryInfo.id;
				param.totalCount = categoryInfo.totalCount;
			break;

			case 'product' :
				if( marketingData.productInfo == null ){
					return;
				}
				var categoryInfo = marketingData.categoryInfo;
				var productInfo = marketingData.productInfo;

				traceType = 'pdp';
				pageName = productInfo.name;
				pageType = 'pdp';
				breadcrumb = getBreadcrumbData(categoryInfo.breadcrumbs);

				param.categoryId = categoryInfo.id;
				param.productIds = productInfo.id;
				param.url = marketingData.pathName;
				//param.colorName = productInfo.color;
				//param.url = breadcrumb;
			break;
			case 'search' :
				if( marketingData.searchInfo == null ){
					return;
				}

				var searchInfo = marketingData.searchInfo;
				var itemList = marketingData.itemList;


				traceType = 'searchResult';
				pageName = 'search result'; // productName을 노출해야 하는데
				pageType = 'search result';
				breadcrumb = 'Search';

				param.totalResultCount = searchInfo.totalCount;
				param.searchTerm = searchInfo.keyword;

				if( itemList != null ){
					var productIds = $.map(itemList, function(item){ return item.id });
					param.productIds = String(productIds);
				}
				
//				param.searchTerm = escape(param.searchTerm);
				param.searchTerm = param.searchTerm.replace(/&/gi,'&amp;').replace(/</gi,'&lt;').replace(/>/gi,'&gt;').replace(/'/gi,'&apos;').replace(/\"/gi,'&quot;');
				
			break;
			case 'cart' :
				traceType = 'cartView';
				pageName = 'Cart'
				pageType = 'cart';
				breadcrumb = 'Cart';
			break;

			case 'checkout' :
				// || _.isEmpty( checkoutInfo )
				if( marketingData.checkoutInfo == null ){
					return;
				}

				var checkoutInfo = marketingData.checkoutInfo;

				var step = checkoutInfo.step;
				if( step == "order" ){
					step = "billing";
				}
				step = step.charAt(0).toUpperCase() + step.slice(1);

				traceType = 'checkout';
				pageName = 'Checkout: ' + step;
				pageType = 'checkout';
				breadcrumb = 'Checkout';

				param.checkoutStepName = step;
			break;

			case 'confirmation' :
				traceType = 'orderConfirm';
				pageName = 'Checkout : Order Confirmation'
				pageType = 'order confirmation';
				breadcrumb = 'Checkout/Order confirmation';

				param.orderNumber = marketingData.orderNumber;
			break;
		}
		
		if( param.productIds == "" || param.productIds == null){
			param.productIds = 0;
			param.totalResultCount = 0;
			//return;
		}

//	    pageName = escape(pageName);
//	    breadcrumb = escape(breadcrumb);
	    pageName = pageName.replace(/&/gi,'&amp;').replace(/</gi,'&lt;').replace(/>/gi,'&gt;').replace(/'/gi,'&apos;').replace(/\"/gi,'&quot;');
	    breadcrumb = breadcrumb.replace(/&/gi,'&amp;').replace(/</gi,'&lt;').replace(/>/gi,'&gt;').replace(/'/gi,'&apos;').replace(/\"/gi,'&quot;');
		param = $.extend( param, getPageData( traceType, pageName, pageType, breadcrumb ));
		processor( param );
	}

	function getPageData(traceType, name, type, breadcrumb, newAcount, newLogin, errorCode ){
		var page = {};
		page.traceType = traceType;
		page.pageName = name;
		page.pageType = type;

		if( breadcrumb != null ){
			page.breadcrumb = breadcrumb;
		}

		if( type == 'error' && errorCode != null){
			page.errorCode = errorCode;
		}

		if( newAcount == true ){
			page.newAccountCreated = true;
		}

		if( newLogin == true ){
			page.newLogin = true;
		}

		return page;
	}
	// 포멧에 맞도록 카테고리 정보 재정의
	function getCategoryData( str, breadcrumb, totalCount ){

		var pattern = /\"category\":{.+\"page\"/;

		var category = {};
		var categoryList = breadcrumb.split("/");

		$.each( categoryList, function( index, data ){
			if( index == 0 ){
				category.primaryCategory = data;
			}else{
				category['subCategory'+ index ] = data;
			}
		});

		if( totalCount != null){
			category.totalResultCount = totalCount;
		}

		var result = String(str).replace(pattern, '"category":'+ JSON.stringify(category) +'},"page"' );	

		return result;
	}
	function getBreadcrumbData( breadcrumb ){
		if( breadcrumb != '' ){
			var breadcrumbList = String(breadcrumb).split('||');
			breadcrumbList = _.without( breadcrumbList, 'home', 'Home', 'HOME').join('/');
			return String(breadcrumbList);
		}
		return '';
	}
	function processor( param, sc ){
		//console.log( param );
		Core.Utils.ajax(processorUrl, 'GET', param, function(data){
			//console.log( data );
			printScript( data, param );
		}, true, true, 100, sc );
	}

	function addEvent(){
		var endPoint = Core.getComponents('component_endpoint');

		endPoint.addEvent('loadMoreProducts', function( data ){
			//console.log( data );

			updateMarketingData();
			if( marketingData.categoryInfo == null && marketingData.searchInfo == null ){
				return;
			}
			
			var itemList = data.itemList;	
			var param = {};
			var categoryInfo = {};

			if( itemList  != null ){
				var productIds = $.map(itemList, function(item){ return item.id });
				param.productIds = String(productIds);
			}

			if( marketingData.categoryInfo != null ){
				categoryInfo = marketingData.categoryInfo;
				param.categoryId = categoryInfo.id;
				param.traceType = "plpMore";
			}


			if( marketingData.searchInfo != null ){
				param.traceType = "plpMore";
			}
			
			/*
			var page = data.page - 1;
			var pageSize = data.pageSize;


			param.limit = pageSize;
			param.offset = page * pageSize;
			*/

			processor( param );
		});
		endPoint.addEvent('applyFilter', function( data ){
			//console.log( data );
			/*
			digitalData.eventData = {
				filterCategory : data.key,
				filterOption : data.value
			}
			digitalData.event = 'applyFilter';
			
			var param = {
				traceType : "search",
				filter : 'Y',
				filterCategory : data.key,
				filterOption : data.value
			}
			processor( param );
			*/

			setEventData("applyFilter", { filterCategory : data.key, filterOption : decodeURIComponent(data.value)});
			
		});
		endPoint.addEvent('quickView', function( data ){
			//console.log( data );

			var param = {
				traceType : "search",
				quickview : "Y",
				productIds : data.product.id
			}
			processor( param );
		});
		endPoint.addEvent('addToCart', function( data ){

			var param = {
				traceType : 'addToCart',
				cartType : 'add to cart',
				skuIds : data.skuId,
				quantities : data.quantity
			}
			processor( param );

		});
		endPoint.addEvent('buyNow', function( data ){
			//console.log( data );

			var param = {
				traceType : "addToCart",
				cartType : 'buy now',
				skuIds : data.skuId,
				quantities : data.quantity
			}
			processor( param );
		});
		endPoint.addEvent('addToWishlist', function( data ){
			//console.log( data );
			var param = {
					traceType : "addToWish",
					productIds : data.productId,
					pageType : "wish list"
			}
			processor( param );
		});
		endPoint.addEvent('pdpColorClick', function( data ){
			console.log( data );
			/*
			var param = {
				traceType : "colorClick",
				colorName : data.color
			}
			processor( param );
			
			var digitalData = digitalData || {};
			
			digitalData.eventData = {
				colorName : data.color
			}
			digitalData.event = 'pdpColorClick';
			*/

			setEventData("pdpColorClick", { colorName : data.color });
		});
		endPoint.addEvent('pdpImageClick', function( data ){
			//console.log( data );
			/*
			var param = {
				traceType : "imgClick"
			}
			processor( param );
			*/
			//digitalData.event = 'pdpImageClick';

			setEventData("pdpImageClick", {});

		});
		endPoint.addEvent('pdpSizeGuideClick', function( data ){
			//console.log( data );
			/*
			var param = {
				traceType : "sizeGuide"
			}
			processor( param );
			*/
			setEventData("pdpSizeGuideClick", {});
		});
		endPoint.addEvent('searchSuggestionClick', function( data ){
			// 한글을 사용하면 안되는데 검색어가 한글이 많다.
			// 바로 리다이텍트 걸려서 타이밍이 나올지 확인
			//console.log( data );
			/*
			var param = {
				traceType : 'searchSuggest',
				searchTerm : data.key,
				searchSuggestionName : data.text
			}
			processor( param, false );
			*/
			var param = {
				searchResults : {
					searchTerm : data.key,
					searchSuggestionName : data.text
				}
			}
			setEventData("searchSuggestionClick", param );

		});
		endPoint.addEvent('removeFromCart', function( data ){

			var param = {
				traceType : "removeCart",
				itemIds : data.orderItemId
			}

			//processor( param, false );
			//임시로 ajax를 빼서 작업( async 때문에 )
			$.ajax({
				url : processorUrl, 
				type : 'GET',
				data : param,
				async : false,
				complete : function(data){
					printScript( data );
				}
			})
		});
		endPoint.addEvent('cartAddQuantity', function( data ){
			//console.log( data );
			var param = {
				traceType : 'addCartQuantity',
				skuIds : data.skuId,
				quantities : data.quantity
			}
			//processor( param );

			$.ajax({
				url : processorUrl, 
				type : 'GET',
				data : param,
				async : false,
				complete : function(data){
					printScript( data );
				}
			})

		});
		endPoint.addEvent('applyPromoCode', function( data ){
			console.log( data );
			//valid, invalid, expired
			//만료된 코드를 알수는 있지만 메시지 처리 된 상태로 전달되고 있어 공통으로 처리가 불가능해 우선은 두가지로만 처리
			var result = ( data.promoAdded == false ? 'invalid' : 'valid' );

			if( data.exceptionKey == 'Offer Has Expired' ){
				result = "expired";
			}
			/*
			var param = {
				traceType : 'applyPromoCode',
				result : result,
				code : data.promoCode
			}
			processor( param );

			*/
			var param = {
				promoCode : {
					result : result,
					code : data.promoCode
				}
			}
			setEventData("applyPromoCode", param );
		});
		endPoint.addEvent('newsletterSubscribed', function( data ){
			//console.log( data );
			/*
			var param = {
				traceType : "newsletter",
				location : marketingData.pathName
			}
			processor( param );
			*/
			setEventData("newsletterSubscribed", { newsletterSubscriptionLocation : marketingData.pathName } );
		});

		/*
		endPoint.addEvent('newAccountCreated', function( data ){
			//console.log( data );
			var param = {
				traceType : "createAccount"
			}
			processor( param );
		});
		*/
		endPoint.addEvent('login', function( data ){
			//console.log( data );
			//digitalData.event = 'newLogin';
			setEventData("newLogin", {} );
		});
		endPoint.addEvent('socialShareClick', function( data ){
			//console.log( data );
			/*
			var param = {
				traceType : "socialShareService",
				socialShareServiceName : data.service
			}
			
			processor( param );
			*/

			setEventData("socialShareClick", { socialShareServiceName : data.service } );
		});
		endPoint.addEvent('addNewAddress', function( data ){
			//console.log( data );
			/*
			var param = {
				traceType : "addNewAddress"
			}
			processor( param );
			*/
			setEventData("addNewAddress", {} );
		});
		endPoint.addEvent('productRecommendationClick', function( data ){
			var param = {
				product : [
					{
						id : data.productId,
						name : data.name
					}
				]
			}
			setEventData("productRecommendationClick", param );
		});
	}

	function isBaseData(){
		var digitalData = digitalData || {};
		return !_.isEmpty(digitalData);
	}

	function printScript( data, param ){
		//console.log( data.responseText );
		if( data.status == 200){
			try{
				isSetBase = true;
				var result = data.responseText;

				if( param != null ){
					// plp 시 전체 수 수정
					if( param.pageType == "plp" ){
						if( param.breadcrumb != null && param.breadcrumb != "" ){
							//var pattern = /{\"totalResultCount\":[0-9]+}/;
							//result = String(result).replace(pattern, '{"totalResultCount":'+ param.totalCount +'}' );
							result = getCategoryData( result, param.breadcrumb, param.totalCount );							
						}
					}

					if( param.pageType == "pdp"){

						if( param.breadcrumb != null && param.breadcrumb != "" ){

							/*
							var pattern = /\"category\":{.+\"page\"/;

							var category = {};
							var categoryList = param.breadcrumb.split("/");

							$.each( categoryList, function( index, data ){
								if( index == 0 ){
									category.primaryCategory = data;
								}else{
									category['subCategory'+ index ] = data;
								}
							});

							result = String(result).replace(pattern, '"category":'+ JSON.stringify(category) +'},"page"' );	
							*/
							result = getCategoryData( result, param.breadcrumb );
						}
						
					}

					if( param.pageType == "order confirmation" ){
						result = result.replace( '"promoCode":""', '"promoCode":"undefined"');
						result = result.replace( '"discountCode":""', '"discountCode":"undefined"');
					}

					//result = result.replace( '"visitor":{', '"visitor":{"isRewardsMember":"undefined",');
				}
				console.log(result);
				$("body").append( result );
			}catch(e){
				//console.log(e);
			}
			printEventData();
		}
	}

	function setEventData( event, data ){
		
		console.log("digitalData.event: "+event);
		console.log("digitalData.eventData: ",data);
		window.digitalData && $.extend(window.digitalData, {event: event}) && $.extend(window.digitalData, {eventData: data});
		
		var	digitalData = digitalData || {};
		if( isSetBase ){
			digitalData.event = event;
			digitalData.eventData = data
		}else{
			eventList.push({
				event : event, 
				eventData : data
			})
		}
	}

	// ajax 통신이 끝나지 않은 상황에서 호출되는 경우가 있어 통신 이후 처리
	function printEventData(){
		if( eventList.length > 0 ){
			console.log( "남은거 있음");
			$.each( eventList, function( i, list ){
				console.log( list );
				digitalData.event = list.event;
				digitalData.eventData = list.eventData;
			})
		}
	}

	Core.aa = {
		// 함수를 구분짓는것이 큰 의미는 없지만 추후 형태의 변화가 있을것을 대비해서 구분
		init : function(){
			addEvent();	
		},
		pv : pv,
		commerce : commerce
		//addEvent : addEvent,
		//error : error
	}

})(Core);