(function(Core){
	var addOnProduct = function(){
		'use strict';

		var $this, $addonListWrap, args;
		var objChildItem={}; //single 만 됨
		var setting = {
			selector:'[data-component-addon-product-option]',
			selectbox:'select',
			optionTemplate:'#addon-sku-option',
			listTemplate:'#addon-sku-list',
			resultWrap:'.addon-list-wrap',
		}

		var addChildItem = function(key, skuData, requestChildItem){
			/*
				원상품 : 애드온 상품
					1 : 1
					1 : n
					n : 1
			*/
			var appendTxt = '';
			if($(setting.listTemplate).length <= 0){
				throw new Error('#addon-sku-list template is not defined');
				return;
			}

			if(args['data-component-addon-product-option'].addOnListType === 'single'){
				objChildItem = {};
				$addonListWrap.empty();
			}

			if(!objChildItem.hasOwnProperty(key)){
				skuData.privateId = key;
				appendTxt = Handlebars.compile($(setting.listTemplate).html())(skuData);
				objChildItem[key] = requestChildItem;
				$addonListWrap.append(appendTxt);

				//Core.moduleEventInjection(appendTxt);
				Core.getComponents('component_quantity', {context:$addonListWrap}, function(){
					this.addEvent('change', function(qty){
						objChildItem[key].quantity = qty;
					});
				});
			}


		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				args = arguments[0];

				//에드온의 템플릿이 없으면 에러 throw
				if($(setting.optionTemplate).lenght <= 0){
					throw new Error('#addon-sku-option template is not defined');
					return;
				}

				//sku 옵션생성
				$this = $(setting.selector);
				$addonListWrap = $this.find(setting.resultWrap);

				var $select = $this.find(setting.selector);
				var optionData = args['data-product-options'];
				var skuData = args['data-sku-data'];
				var skuOpt = [];

				for(var i=0; i<skuData.length; i++){
					if(skuData[i].inventoryType !== 'UNAVAILABLE' && skuData[i].quantity > 0){
					    (function(){
					        var obj = {};
							obj['name'] = args['data-component-addon-product-option'].name;
					        obj['price'] = skuData[i].price;
    						obj['quantity'] = skuData[i].quantity;
    						obj['selectedOptions'] = (function(index){
    						    var arr = [];
								var privateId = '';
    						    skuData[index].selectedOptions.forEach(function(a, i){
									var obj = {};
									privateId += a;
    						        obj[optionData[i].attributeName] = optionData[i]['values'][a];
									arr.push(obj);
    						    });
								obj['privateId'] = privateId;
    							return arr;
    						})(i);
							obj['label'] = (function(index){
    						    var arr = [];
    						    skuData[index].selectedOptions.forEach(function(a, i){
									arr.push(optionData[i]['values'][a]);
    						    });
    							return arr;
    						})(i).join(' / ');
    						skuOpt.push(obj);
					    })();
					}
				}

				Core.getComponents('component_select', {context:$this}, function(){
					var _self = this;

					this.getThis().find('select').append(Handlebars.compile($(setting.optionTemplate).html())(skuOpt));
					this.rePaintingSelect();
					this.addEvent('change', function(val, $selected, index){
						var requestChildItem = {};
						requestChildItem.productId = args['data-component-addon-product-option'].addonId;
						//obj.name = skuOpt[index].name;
						//obj.price = skuOpt[index].price;
						requestChildItem.quantity = 1;

						for(var i=0; i<skuOpt[index-1].selectedOptions.length; i++){
							for(var key in skuOpt[index-1].selectedOptions[i]){
								requestChildItem['itemAttributes['+ key +']'] = skuOpt[index-1].selectedOptions[i][key];
							}
						}

						addChildItem($selected.attr('data-privateid'), skuOpt[index-1], requestChildItem);

						/* addOnListType이 multi */
						/*setTimeout(function(){
							_self.reInit();
						}, 100);*/
					});
				});

				/* delete btn addEvent */
				$addonListWrap.on('click', '.btn-delete', function(e){
					e.preventDefault();

					var $parent = $(this).closest('.addon-state');
					var key = $parent.attr('data-privateId');
					$parent.remove();

					if(objChildItem.hasOwnProperty(key)){
						delete objChildItem[key];
					}
				});

				return this;
			},
			getChildOrderItems:function(){
				var arrChildItem = [];
				for(var key in objChildItem){
					arrChildItem.push(objChildItem[key]);
				}
				return arrChildItem;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_addon_product_option'] = {
		constructor:addOnProduct,
		attrName:['data-component-addon-product-option', 'data-product-options', 'data-sku-data']
	}
})(Core);
