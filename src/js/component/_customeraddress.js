(function(Core){
	var CustomerAddress = function(){
		'use strict';

		var $this, args;
		var setting = {
			selector:'[data-component-customer-address]',
			selectBtn:'[data-customer-address-select-btn]',
			baseDom : '[data-customer-address]'
		}

		var getAddressInfoBySelect = function( $el ){
			var data = {};
			$el.closest(setting.baseDom).find('input[type=hidden]').each(function() {
				data[$(this).attr('name')] = $(this).val() || '';
			});	
			return data;
		}

		var Closure = function(){}
		Closure.prototype = {
			setting:function(){
				var opt = Array.prototype.slice.call(arguments).pop();
				$.extend(setting, opt);
				return this;
			},
			init:function(){
				var _self = this;
				args = arguments[0];
				$this = $(setting.selector);

				if( $this.find(setting.selectBtn).length == 0 ){
					return;
				}

				$this.find(setting.selectBtn).on('click', function(e){
					e.preventDefault();
					_self.fireEvent( 'select', this, [getAddressInfoBySelect( $(this) )]);
				});

				return this;
			}
		}

		Core.Observer.applyObserver(Closure);
		return new Closure();
	}

	Core.Components['component_customer_address'] = {
		constructor:CustomerAddress,
		attrName:'data-component-customer-address'
	}
})(Core);
