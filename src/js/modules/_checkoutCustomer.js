(function(Core){
	Core.register('module_order_customer', function(sandbox){
		var Method = {
			moduleInit:function(){
				var $this = $(this);

				sandbox.validation.init( $this.find('#order_info') );
				
				var $submitBtn = $this.find('button[type="submit"]');
				
				$submitBtn.on("click", function(){
			  	    if ($this.find('#checkTerms').length > 0) {
                       if(!$this.find('#checkTerms').hasClass('checked')){
                          UIkit.modal.alert('이용약관에 동의 해주세요.');
                          return false;
                       }
                    }
                    if ($this.find('#checkPrivacy').length > 0) {
                       if(!$this.find('#checkPrivacy').hasClass('checked')){
                          UIkit.modal.alert('개인정보 수집 및 이용에 동의해주세요.');
                          return false;
                       }
                    }
				});

   				if( $this.find('input[name="isAlreadyRegistered"]').length > 0){
					UIkit.modal.confirm('이미 회원 가입된 아이디 입니다. 로그인 하시겠습니까?', function(){
						window.location.replace('/login?successUrl=/checkout');
					}, function(){},
					{
						labels: {'Ok': '로그인', 'Cancel': '비회원 주문'}
					});

				}
			}
		}


		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-customer]',
					attrName:'data-module-order-customer',
					moduleName:'module_order_customer',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
