(function(Core){
	Core.register('module_endpoint', function(sandbox){
		var $this;
		var Method = {
			moduleInit:function(){
				$this = $(this);
			},

			call:function( status, data ){
				switch( status ){
					case "loadMoreProducts":

					break;

					// filter.js  
					case "applyFilter":
					break;

					case "quickView":
					break;

					case "addToCart":
					break;

					case "addToWishlist":
					break;

					case "pdpColorClick":
					break;

					case "pdpImageClick":
					break;

					case "pdpSizeGuideClick":
					break;

					case "pdpDeliveryGuideClick":
					break;

					// 검색어를 선택했을때 ( 직접 입력이 아닌 링크를 통한 검색시 )
					case "searchSuggestionClick":
					break;

					case "removeFromCart":
					break;

					case "cartAddQuantity":
					break;

					case "applyPromoCode":
					break;

					case "newsletterSubscribed":
					break;

					case "newAccountCreated":
					break;

					case "login":
					break;

					case "logout":
					break;

					case "socialShareClick":
					break;

					case "addNewAddress":
					break;
				}
			}

		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[ data-module-endpoint]',
					attrName:'data-module-endpoint',
					moduleName:'module_endpoint',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
