(function(Core){
	Core.register('module_returnorder_history', function(sandbox){
		var Method = {
			moduleInit:function(){
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);

				var $this = $(this);

				$this.find('button.return-cancel-item').on("click", Method.returnOrderCancelItem );
			},

			// 반품 취소 요청
			returnOrderCancelItem:function(e){
				e.preventDefault();
				var $form = $(this).closest("form");

				UIkit.modal.confirm('반품을 취소 하시겠습니까?', function(){
					Core.Utils.ajax($form.attr("action"), "POST", $form.serialize(), function(data){

						UIkit.modal.alert("반품이 취소 되었습니다.").on('hide.uk.modal', function() {
							window.location.reload();
						});							

						/*
						var data = sandbox.rtnJson(data.responseText, true);
						var result = data['result'];
						if( result == true ){
							UIkit.modal.alert("반품이 취소 되었습니다.").on('hide.uk.modal', function() {
								window.location.reload();
							});							
						}else{
							UIkit.modal.alert(data['errorMsg']).on('hide.uk.modal', function() {
								window.location.reload();
							});
						}
						*/
					});
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-returnorder-history]',
					attrName:'data-module-returnorder-history',
					moduleName:'module_returnorder_history',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
