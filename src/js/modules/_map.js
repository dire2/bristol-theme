(function(Core){
	'use strict';

	Core.register('module_map', function(sandbox){
		var storeList = null,
			$this,
			$infoViewContainer = null,
			map = null,
			markers = [],
			infoWindows = [],
			filterQuery = [],
			currentStoreIndex = 0,
			searchQueryString = '',
			$btnSearch;
		/*
			store-info-view 에 추가되는 dom object

			<h2 class="tit">명동</h2>
			<span class="address">서울 영등포구 여의도동</span>
			<span class="phonenum">070-0000-0000</span>
			<dl class="info">
				<dt class="key">운영시간</dt>
				<dd class="value">평일 10:00, 주말 11:00</dd>
				<dt class="key">매장정보</dt>
				<dd class="value">네이버 지도 API v3는 JavaScript 형태로 제공되는 NAVER 지도 플랫폼으로써</dd>
			</dl>
			<button class="close-btn"><i class="icon-delete_thin"></i></button>
		*/

		/**************************

			매장검색
			매장이름, 지역검색 : _find=지하철&_search=name&_condition=like
			필터 : _find={{매장타입}}&_search=storeType&_condition=or||and

			admin에서 재고위치/대리점의 추가속성에 storeType, icon은 각각, 검색필터와 마커의 아이콘의 클래스를 입력하는 속성이다.

		***************************/

		var makeMarkerIcon = function(storeData){
			var imgDomain = _GLOBAL.SITE.IMAGE_DOMAIN;
			var icon = {
				size:new naver.maps.Size(24, 37),
				anchor:new naver.maps.Point(12, 37),
				origin:new naver.maps.Point(0, 0)
			}

			if(storeData.additionalAttributes && storeData.additionalAttributes.icon && storeData.additionalAttributes.icon !== ''){
				icon.content = '<i class="icon_map_marker '+storeData.additionalAttributes.icon+'"></i>';
			}else{
				icon.url = imgDomain+'/cmsstatic/sp_pins_spot_v3.png';
			}

			return icon;
		}

		var submitSearchMap = function(){
			//_find=서울&_search=name&_condition=like&_find=손목시계,지하철&_search=storeType&_condition=or

			var filterParams = location.pathname + '?';
			if(searchQueryString !== '') filterParams += searchQueryString + '&';
			if(filterQuery.length > 0){
				filterParams += '_find='+filterQuery.join(',')+'&_search=storeType&_condition=or';
			}
			location.href = filterParams.replace(/[?|&]$/, '');
		}

		var Method = {
			moduleInit:function(){
				var args = arguments[0];
				$this = $(this);
				$infoViewContainer = $(this).find(args['data-module-map'].infoview);
				storeList = args['data-store-list'];
				$btnSearch = $this.find('.btn_search');

				//map 초기화
				var firstLatitude = (storeList[0]) ? storeList[0].latitude:37.3595953;
				var firstLongitude = (storeList[0]) ? storeList[0].longitude:127.1053971;
				var currentParams = sandbox.utils.getQueryParams(location.href);
				var arrCurrentFilters = [];
				if(currentParams.hasOwnProperty('_find') && currentParams['_find'] instanceof Array && currentParams['_search'].indexOf("storeType") > -1){
				    var idx = currentParams['_search'].indexOf("storeType");
					arrCurrentFilters = currentParams['_find'][idx].split(',');
				}else{
					currentParams = null;
					arrCurrentFilters = null;
				}

				map = new naver.maps.Map(args['data-module-map'].target, {
					center:new naver.maps.LatLng(firstLatitude, firstLongitude),
					zoom:5
				});

				var searchField = sandbox.getComponents('component_searchfield', {context:$this}, function(){
					/*this.addEvent('beforeSubmit', function(){
						var val = arguments[0];
						$('input[name=_find]').val(val);
					});*/
					this.addEvent('searchEmpty', function($form){
						searchQueryString = $form.serialize();
						submitSearchMap();
					});

					this.addEvent('searchKeyword', function($form, value){
						searchQueryString = $form.serialize();
						submitSearchMap();
					});
				});

				var searchCheckBox = sandbox.getComponents('component_checkbox', {context:$this}, function(i, dom){
					this.addEvent('change', function(val){
						var index = filterQuery.indexOf(val);
						if(index === -1){
							filterQuery.push(val);
						}else{
							filterQuery.splice(index, 1);
						}
					});

					if(arrCurrentFilters !== null && arrCurrentFilters.indexOf(encodeURI(this.getThis().val())) > -1){
						this.getThis().prop('checked', true);
						this.getThis().trigger('change');
					}
				});

				$(this).find('.filter-btn').click(function(e){
					e.preventDefault();

					/*if(filterQuery.length > 0){
						var filterQueryParams = sandbox.utils.getQueryParams($(this).closest('form').serialize());
						filterQueryString = '_find='+filterQuery.join(',');
						if(filterQueryParams._search) filterQueryString += '&_search='+filterQueryParams._search;
						if(filterQueryParams._condition) filterQueryString += '&_condition='+filterQueryParams._condition;
					}else{
						filterQueryString = '';
					}*/

					searchField.externalAction();
				});

				$(this).find(".filter-list").find("input[type='checkbox']").on('click', function(e){ 
					setTimeout( function(){
						$btnSearch.trigger('click'); 
					}, 500);
                });
				
				$(this).find('.search-result').find('a').click(function(e){
					e.preventDefault();
					Method.mapEvent($(this).parent().index());

					$this.find('.search-result').removeAttr('style');
					if($('body').attr('data-device') !== 'pc') $this.find('.search-result').css('display', 'none');
				});

				$(this).on('click', '.close-btn', function(e){
					Method.mapEvent(currentStoreIndex);
				});

				Method.mapInit();
			},
			getStoreList:function(id){
				for(var key in storeList){
					if(storeList[key].id == id){
						return {'info':storeList[key], 'index':key}
					}
				}
			},
			showInfoDetail:function(data){				
				var template = Handlebars.compile($('#map-store-info').html())(data);

				$infoViewContainer.empty().append(template);
				$infoViewContainer.stop().animate({'left':0}, 300);
			},
			mapInit:function(){

				//store 위도, 경도 값으로 지도 마커 찍어내기
				for (var i=0; i<storeList.length; i++) {
					var position = new naver.maps.LatLng(storeList[i].latitude, storeList[i].longitude);
					var marker = new naver.maps.Marker({
						map:map,
						position:position,
						title:storeList[i].name,
						icon:makeMarkerIcon(storeList[i]),
						zIndex:100
					});

					var infoWindow = new naver.maps.InfoWindow({
						content: '<div style="width:150px;text-align:center;padding:10px;">'+ storeList[i].name +'</div>'
					});

					markers.push(marker);
					infoWindows.push(infoWindow);
				}

				for (var i=0, ii=markers.length; i<ii; i++) {
					naver.maps.Event.addListener(markers[i], 'click', getClickHandler(i));
				}

				function getClickHandler(seq){
					return function(){
						Method.mapEvent(seq);
					}
				}
			},
			mapEvent:function(seq){
				var marker = markers[seq], infoWindow = infoWindows[seq];

				if (infoWindow.getMap()) {
					infoWindow.close();
					$infoViewContainer.stop().animate({'left':$infoViewContainer.outerWidth(true)}, 300, function(){
						$infoViewContainer.removeAttr('style');
						$this.find('.search-result').removeAttr('style');
					});

				} else {
					infoWindow.open(map, marker);

					var objStoreInfo = Method.getStoreList($(this).attr('data-store-id'));
					Method.showInfoDetail(storeList[seq]);
					currentStoreIndex = seq;

					if($('body').attr('data-device') !== 'pc') $this.find('.search-result').css('display','none');

					//선택한 store 좌표로 이동
					map.setCenter(new naver.maps.LatLng(storeList[seq].latitude, storeList[seq].longitude));
					map.setZoom(10);
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-map]',
					attrName:['data-module-map', 'data-store-list'],
					moduleName:'module_map',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
