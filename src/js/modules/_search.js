(function(Core){
	Core.register('module_search', function(sandbox){
		var $this, args, clickIS, endPoint, latestKeywordList, arrLatestKeywordList = [];

		var setSearchKeyword = function(keyword){
			var pattern = new RegExp(keyword, 'g');
			arrLatestKeywordList = sandbox.utils.rtnMatchComma(latestKeywordList.replace(pattern, ''));
			arrLatestKeywordList.unshift(keyword);

			if(arrLatestKeywordList.length >= args.keywordMaxLen){
				arrLatestKeywordList = arrLatestKeywordList.slice(0, -1);
			}
			$.cookie('latestSearchKeyword', arrLatestKeywordList.join(','));
		}

		var Method = {
			moduleInit:function(){
				$this = $(this);
				args = arguments[0];
				clickIS = false;

				latestKeywordList = $.cookie('latestSearchKeyword') || '';
				arrLatestKeywordList = sandbox.utils.rtnMatchComma(latestKeywordList || '');

				endPoint = Core.getComponents('component_endpoint');

				sandbox.getComponents('component_searchfield', {context:$this, resultTemplate:'#search-list', resultWrap:'.etc-search-wrap'}, function(){
					var _self = this;
					this.addEvent('resultSelect', function(data){
						var text = $(data).text();
						var endPointData = {
							key : text,
							text : text
						}
						endPoint.call( 'searchSuggestionClick', endPointData );

						this.getInputComponent().setValue(text);
						setSearchKeyword(text);
						location.href='/search?q='+ text;
					});

					this.addEvent('beforeSubmit', function(data){
						setSearchKeyword(data);
					});

					this.addEvent('removeList', function(keyword){
						// 삭제버튼 이벤트를 받는 부분
						// keyword 가 all 일때 쿠키의 최근 검색어를 모두 삭제
						if(keyword === 'all'){
							$.cookie('latestSearchKeyword', '');
						}else{
							latestKeywordList = latestKeywordList.replace(keyword, '');
							$.cookie('latestSearchKeyword', latestKeywordList);
						}
					});

					/* 최근검색어 */
					if(args.isLatestKeyword === 'true'){
						this.setResultAppend('#keyword-container', '#latest-search-keyword', {
							label:'최근 검색어',
							keyword:arrLatestKeywordList
						});
					}
				});

				// search btn (search field show&hide)
				$('.gnb-search-btn').click(function(e){
					e.preventDefault();

					if(clickIS){
						clickIS = false;
						$this.removeClass('active');
					}else{
						clickIS = true;
						$this.addClass('active');
					}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-search]',
					attrName:'data-module-search',
					moduleName:'module_search',
					handler:{context:this, method:Method.moduleInit}
				});
			},
			searchTrigger:function(){
				$('.gnb-search-btn').trigger('click');
			}
		}
	});
})(Core);
