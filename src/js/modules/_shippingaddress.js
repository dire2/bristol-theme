(function(Core){
	'use strict';

	Core.register('module_shipping_address', function(sandbox){
		var $this, args, modal = null;
		var Method = {
			moduleInit:function(){
				// modal layer UIkit 사용
				$this = $(this);
				args = arguments[0];
				modal = UIkit.modal('#common-modal');

				$this.on('click', '.add-address', function(e){
					e.preventDefault();
					Method.modalInit($(this).attr('href'));
				});

				$this.on('click', '.modify', function(e){
					e.preventDefault();
					Method.modalInit($(this).attr('href'));
				});

				$this.on('click', '.remove', function(e){
					e.preventDefault();
					var _self = this;
					UIkit.modal.confirm('삭제 하시겠습니까?', function(){
						$(_self).parent().submit();
					});
				});

				$this.find('.address-form').remove();
			},
			modalInit:function(url){
				sandbox.utils.ajax(url, 'GET', {}, function(data){
					var appendHtml = $(data.responseText).find('.address-form').html();
					modal.element.find('.contents').empty().append(appendHtml);
					sandbox.moduleEventInjection(appendHtml);

					// 배송지 수정시 주소부분 최초 readonly 처리
					var addressInput = UIkit.modal('#common-modal').find('.search-field input#address1');
					if(addressInput.val()){
						addressInput.css('background','#ddd').prop('readonly', true);
						$('.btn_search').text('재검색').addClass('re_search');
					}

					modal.show();
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-shipping-address]',
					attrName:'data-module-shipping-address',
					moduleName:'module_shipping_address',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
