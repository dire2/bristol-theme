(function(Core){
	Core.register('module_social_login', function(sandbox){
		var Method = {
			moduleInit:function(){
				$this = $(this);
				sandbox.getComponents('component_select', {context:$this}, function(){
					this.addEvent('change', function(val){
						Method.submitFormByName( val );
					});
				});

				$(this).find('[data-social-btn]').on('click', function(e){
					e.preventDefault();
					var type = $(this).data('social-btn');
					Method.submitFormByName( type );
				})


			},
			submitFormByName:function(name){
				var $form = $this.find('form[name="' + name + '"]');
				if( $form ){
					$form.submit();
				}
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-social-login]',
					attrName:'data-module-social-login',
					moduleName:'module_social_login',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
