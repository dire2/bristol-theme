(function(Core){

	Core.register('module_order', function(sandbox){
		var Method = {
			$that:null,
			$allCheck:null, // 팝업 전체 선택 체크박스
			$itemList:null, // 선택 해서 popup에 노출되는 아이템 리스트
			$popModal:null, // 취소 팝업
			cancelOrderId:null, // 취소할 주문 id
			$popSubmitBtn:null, // 취소 submit 버튼
			$previewContainer:null, // 사용안함
			isAllFg:null, // 취소 선택시 모든 fulfilment가 취소 가능했는지 여부
			isAblePartialVoid:null, // 부분 취소 가능여부
			beforeSelectedOrder:null, // 사용안함
			$refundAccountInfo:null, //환불정보 입력 폼
			oldOrderUrl:null, //이전 사이트 주문 정보 URL
			moduleInit:function(){
				var args = Array.prototype.slice.call(arguments).pop();
				$.extend(Method, args);

				var $this = $(this);
				Method.$that = $this;
				Method.$popModal = UIkit.modal("#popup-cancel");
				Method.$popSubmitBtn = Method.$popModal.find('[data-cancel-submit]');
				Method.$refundAccountInfo = Method.$popModal.find('[data-refund-account-info]');
				//Method.$previewContainer = Method.$that.find("#cancel-order-preview");

				// 주문 취소
				$this.find('[data-cancel-order-btn]').on("click", function(e){
					e.preventDefault();
					Method.isAblePartialVoid = $(this).data("cancel-order-btn");
					Method.openCancelOrderPopup( $(this).closest('form') );
				});

				// 임시로 체크박스 노출되도록 설정
				//$("input:checkbox").css({"-webkit-appearance":"checkbox"});

				// modal쪽 이벤트

				// 전체 선택 체크박스 처리
				Method.$allCheck = Method.$popModal.find('input[name="check-all"]');
				Method.$allCheck.on("change", Method.changeAllCheck );
				/*
				sandbox.getComponents('component_checkbox', {context:$this}, function(){
					//Method.changeAllCheck
					this.addEvent('change', Method.changeAllCheck);
				});
				*/

				// 주문 취소 submit
				Method.$popSubmitBtn.on('click', Method.cancelOrderSubmit );


				// 이전 주문 보기
				// $this.find('[data-old-order-history]').on('click', function(){
				// 	if( $(this).hasClass('disabled') ){
				// 		UIkit.modal.alert('준비중입니다.');
				// 		return;
				// 	}
				// 	window.open( unescape(Method.oldOrderUrl ));
				// });

				Method.setOldOrderView();

				// 부분 취소 - 사용안함
				/*
				$this.find('button.partial-cancel-order').on("click", function(e){
					e.preventDefault();
					Method.partialCancleOrder( $(this) );
				});
				*/
				// 전체 부분 취소
				/*
				$this.find('button.partial-cancel-order-all').on("click", function(e){
					e.preventDefault();
					var param = Method.$previewContainer.find('form').serialize();
					Method.partialCancleOrder( Method.beforeSelectedOrder.find('[data-order-item].active'), param );
				});
				*/
				// component를 사용할수 없어서 임시로 적용해 놓은 이벤트
				//$this.find('[data-order-item]').find('select').change( Method.cancelOrderPreviewUpdate );

				// 부분 취소 checkbox 선택시
				/*
				sandbox.getComponents('component_checkbox', {context:$this}, function(){
					this.addEvent( 'change', Method.cancelOrderPreviewUpdate );
				});
				*/

				//$this.find(".order-list input[type='checkbox']").on("change", Method.cancelOrderPreviewUpdate );

				//checkbox.addEvent( "change", Method.cancelOrderPreviewUpdate );
				//Method.$popModal.show();


			},

			setOldOrderView:function(){
				$.ajax({
					url: '/site-migration/go',
					type: 'GET',
					success:function(data){
						if( $(data).find("result").text() == "true" ){
							Method.oldOrderUrl = $(data).find("URL").text();
							Method.$that.find('[data-old-order-history]').removeClass('uk-hidden');
						}
					}
				})
			},

			openCancelOrderPopup:function($form){
				var $orderId = $form.closest("[data-order]").find("[name='orderId']");
				var $orderNumber = $form.closest("[data-order]").find("[name='orderNumber']");
				var $items = $form.find('[data-order-item]');
				var $availableItems = $form.find('.order-item-wrap.available').find('[data-order-item]'); //취소 가능한 아이템
				var $modal = Method.$popModal;

				// 취소 선택시 모든 fulfilment가 취소 가능했는지 여부
				Method.isAllFg = ( $items.length == $availableItems.length );

				Method.$itemList = $modal.dialog.find('[data-return-reason-list]>ul');
				var $baseItem = $modal.dialog.find('.uk-hidden[data-return-reason-item]');

				Method.$itemList.empty();

				$orderId.clone().attr('data-marketing','').data('marketing',{
					"cancelOrderNumber" : $orderNumber.val()
				}).appendTo( Method.$itemList );


				$availableItems.each( function(index, $this){
					// 아이템 dom 복사
					var $newItem = $baseItem.clone().removeClass("uk-hidden");

					// 정보 수정
					var $info = $newItem.find('[return-reason-info]');
					var $thumb = $newItem.find('[return-reason-thumb]');
					//var $quantitySelect = $newItem.find('[return-reason-partials-quantity]');

					$info.empty();
					$thumb.empty();

					$(this).find('input[name="orderItemId"]').clone().appendTo( $info );
					$(this).find('.info-wrap').clone().appendTo( $info );
					$(this).find('.img-wrap').clone().appendTo( $thumb );
					$(this).find('input[name="fgItemId"]').clone().appendTo( $info );
					quantity = $(this).find('input[name="quantity"]').val();

					// 복사된 정보중 수량은 삭제
					$info.find('.opt.quantity').remove();
					//$info.find('.opt.quantity').removeClass('uk-margin-left').addClass('uk-display-block').prepend('주문수량 : ');
					//$info.find('.info-wrap').append( '<span class="opt">주문수량 : ' + $(this).find('[name="quantity"]').val() + ' 개</span>');
					//$quantity.val( $(this).find('[name="quantity"]').val() );


					if( Method.isAblePartialVoid ){
						var $quantity = $newItem.find('[return-reason-partials-quantity]');
						for( var i=1; i<=quantity; i++){
							// 전체 수량과 같을때 선택 처리
							$quantity.find("select").removeAttr('disabled').append( '<option ' + ( i==quantity ? 'selected' : '')+ ' value="' + i + '">' + i +'</option>');
						}
					}else{
						var $quantity = $newItem.find('[return-reason-order-quantity]');
						$quantity.find('input[name="quantity"]').removeAttr('disabled').val(quantity);
					}

					// 취소 가능 수량 노출
					$quantity.removeClass('uk-hidden').find('[data-quantity]').text( quantity);
					$newItem.appendTo( Method.$itemList );
					Core.moduleEventInjection(Method.$itemList.html());
				});

				//sandbox.moduleEventInjection( $itemList.html() );

				Method.updatePaymentInfo( false );

				if( Method.isAblePartialVoid ){
					Method.$itemList.find('input[type="checkbox"]').on("change", Method.changeItemCheck );
					Method.$itemList.find('select').on("change", Method.updateStatus );

					Method.updateInfoByIsPartial( true );
					Method.updateSubmitBtn( false );

					// 전체 취소로 초기화
					Method.$allCheck.prop('checked', false ).trigger('change');
				}else{

					Method.updateInfoByIsPartial( false );
					Method.updateSubmitBtn( true );
					Method.updateStatus();
				}

				Method.$popModal.show();
			},


			// modal에서 전체 선텍
			changeAllCheck:function(e){
				var isCheck = $(this).prop('checked');
				Method.$itemList.find('>li').each( function(){
					$(this).find('input[type="checkbox"]').prop( 'checked', isCheck );
					if( isCheck ){
						$(this).find('select[name="quantity"]').val( $(this).find('[data-quantity]').text() ).trigger('update');
					}
					Method.showHideAvailabeQuantity( $(this).find('input[type="checkbox"]') );
				});

				Method.updateStatus();
			},

			// modal 에서 아이템 체크박스 변경시 전체 선택 체크박스 상태처리
			updateCheckAll:function(){
				if( Method.$itemList.find('>li').length == Method.$itemList.find('>li').find('input[type="checkbox"]:checked').length ){
					Method.$allCheck.prop( 'checked', true );
				}else{
					Method.$allCheck.prop( 'checked', false );
				}
			},

			// modal에서 체크박스 선택시
			changeItemCheck:function(e){
				Method.showHideAvailabeQuantity( $(this ));
				Method.updateCheckAll();
				Method.updateStatus();
			},

			// 아이템 단위로 수량 선택할 수 있는 select 노출 처리
			showHideAvailabeQuantity:function( $checkbox ){
				var $cancelQuantity = $checkbox.closest('li').find('[return-reason-partials-quantity]');
				if( $checkbox.prop('checked')){
					$cancelQuantity.slideDown('fast');
				}else{
					$cancelQuantity.slideUp('fast');
				}
			},

			// 부분 취소 가능 여부에 따른 정보 노출 처리
			updateInfoByIsPartial:function( $bool ){
				var $checkAllContainer = Method.$popModal.dialog.find('.container.check-all');
				var $checkboxs = Method.$itemList.find('.checkbox');
				var $info = Method.$popModal.dialog.find('[data-info-text]');

				if( $bool ){
					$checkAllContainer.show();
					$checkboxs.show();
					$info.show();
					Method.$popSubmitBtn.text('선택상품 주문 취소');
				}else{
					$checkAllContainer.hide();
					$checkboxs.hide();
					$info.hide();
					Method.$popSubmitBtn.text('주문 취소');
				}
			},


			// 리턴 상황에 따른 가격정보와 버튼 상태 처리
			updatePaymentInfo:function( $bool ){
				var $paymentContainer = Method.$popModal.find('[data-payment-conatiner]');

				if( $bool ){
					$paymentContainer.show();
					Method.$popSubmitBtn.removeAttr('disabled').removeClass('disabled');
				}else{
					$paymentContainer.hide();
					Method.$popSubmitBtn.attr('disabled','true').addClass('disabled');
				}
			},

			// 버튼 활성화/비활성화 처리
			updateSubmitBtn:function( $bool ){
				if( $bool ){
					Method.$popSubmitBtn.removeAttr('disabled').removeClass('disabled');
				}else{
					Method.$popSubmitBtn.attr('disabled','true').addClass('disabled');
				}
			},

			// 취소 가격 및 추가 정보 입력 여부 처리
			updateStatus:function(){
				var $modal = Method.$popModal;
				Method.cancelOrderId = $modal.find('[name=orderId]').val();
				var $paymentContainer = $modal.find('[data-payment-conatiner]');
				var $totalAmount = $modal.find('[data-total-amount]');
				var $fulfillmentCharge = $modal.find('[data-fulfillment-charge]');

				if( Method.isAblePartialVoid ){
					var $items = Method.$itemList.find('>li').find('input[type="checkbox"]:checked').closest('li');
				}else{
					var $items = Method.$itemList.find('>li');
				}

				if( $items.length > 0 ){
					var $form = $modal.find('form');
					//var url = '/account/order/partial-cancel-simulator/' + $currentOrder.data('order');
					//var action = $form.attr('action').replace('cancel','partial-cancel-calculator');
					var action = '/account/order/partial-cancel-calculator/' + Method.cancelOrderId;
					var $itemForm = Method.getFormByPartialItem( $items );
					var totalAmount = 0;

					//console.log(action);
					//console.log($itemForm.serialize());
					//console.log( "부분취소 : " +  Method.getIsAvailablePartialCancel());

					Core.Utils.ajax(action, 'GET', $itemForm.serialize(), function(data){
						var data = sandbox.rtnJson(data.responseText, true);
						var result = data['result'];

						if( result == true ){
							Method.updatePaymentInfo( true );
							Method.updateSubmitBtn( true );
							var paymentInfo = data['ro']['refundPayments'];
							var isRefundAccountNeed = data['ro']['refundAccountNeed'];
							var fulfillmentCharge = data['ro']['fulfillmentCharge'];

							// 환불 계좌 필요여부
							if( isRefundAccountNeed ){
								Method.$refundAccountInfo.show();
								sandbox.validation.reset( Method.$refundAccountInfo.find('form') );
							}else{
								Method.$refundAccountInfo.hide();
							}

							// 추가 배송비 여부
							if( fulfillmentCharge.amount > 0){
								$fulfillmentCharge.find('.price').text( fulfillmentCharge.amount );
								$fulfillmentCharge.removeClass('uk-hidden');
							}else{
								$fulfillmentCharge.addClass('uk-hidden');
							}

							// 결제 정보
							if( paymentInfo != null ){
								var $paymentList = $modal.find('[data-payment-list]');
								var $paymentItem = $modal.find('.uk-hidden[data-payment-item]');

								$paymentList.empty();

								$.each( paymentInfo, function( index, payment ){
									var $newItem = Method.getReplacePaymentItem( $paymentItem, _GLOBAL.PAYMENT_TYPE[ payment.paymentType.type ] + ' : ', payment.totalAmount.amount );
									$newItem.appendTo( $paymentList );
									totalAmount += Number( payment.totalAmount.amount );
								});
							}else{
								UIkit.modal.alert( 'REFUND_PAYMENTS 정보 오류' );
								Method.updatePaymentInfo( false );
								Method.updateSubmitBtn( false );
							}
							
							if(totalAmount <= 0){
								UIkit.modal.alert( '환불 가능한 금액이 없습니다.' );
								Method.updateSubmitBtn( false );
							}
							
							$totalAmount.find('.price').text( sandbox.utils.price(totalAmount) );
							$totalAmount.find('.price').data('marketing', { "cancelPrice" : totalAmount } );
						}else{
							UIkit.modal.alert( data.errorMsg );
							Method.updatePaymentInfo( false );
							Method.updateSubmitBtn( false );
						}
					}, true);
				}else{
					// 아이템 없을때
					Method.updatePaymentInfo( false );
					Method.updateSubmitBtn( false );
				}
			},

			getReplacePaymentItem:function( $base, type, amount ){
				var $newItem = $base.clone().removeClass("uk-hidden");
				$newItem.find('[data-type]').text( type );
				if( amount ){
					$newItem.find('[data-amount]').text( sandbox.utils.price(amount) );
				}else{
					$newItem.find('[data-amount]').remove();
				}
				return $newItem;
			},
			// 부분 취소 인지 전체 취소인지 체크
			getIsAvailablePartialCancel:function(){
				// 부분취소 자체가 안되는 상태
				if( !Method.isAblePartialVoid ) return false;

				if( !Method.isAllFg ) return true;

				var $checkedList = Method.$itemList.find('>li').find('input[type="checkbox"]:checked').closest('li');
				// 아이템 전체 루프

				if( Method.$itemList.find('>li').length != $checkedList.length ){
					return true;
				}

				var isAvailable = false;
				$checkedList.each(function() {
					// 선택한 취소 수량과 가능 취수 수량이 같지 않으면 부부 취소 가능
					if( $(this).find('[name="quantity"]').val() != $(this).find('[data-quantity]').text() ){
						isAvailable = true;
						return false;
					}
				});
				return isAvailable;
			},

			// 선택된 아이템을 하나의 form으로 만들어 리턴
			getFormByPartialItem:function( $items ){
				//var $form = $items.closest('form');
				var $itemForm = $('<form></form>');
				//var $token = $form.find("input[name='csrfToken']");

				//$token.clone().appendTo( $itemForm );

				//체크되어있는 아이템 가져와 form에 append
				$items.each( function(){
					var quantity = $(this).find('[name="quantity"]:enabled').val();
					var $newItem = $(this).clone();
					$newItem.find('[name="quantity"]').val(quantity);
					$newItem.appendTo( $itemForm );
				});

				return $itemForm;
			},

			// 주문 취소 submit
			cancelOrderSubmit:function(){
				var param = '';

				//환불 정보를 받아야 한다면
				if( Method.$refundAccountInfo.is(":visible") ){
					var $refundAccountInfoForm = Method.$refundAccountInfo.find('form');
					sandbox.validation.reset( $refundAccountInfoForm );
					sandbox.validation.validate( $refundAccountInfoForm );

					if( !sandbox.validation.isValid( $refundAccountInfoForm )){
						return;
					}
					param = $refundAccountInfoForm.serialize();
				}

				var isPartial = Method.getIsAvailablePartialCancel();
				var url = '';
				var msg = '';
				var marketingType = '';

				if( isPartial ){
					url = '/account/order/partial-cancel/';
					msg = "부분 취소 진행 시, 취소 대상 아이템은 해당 주문내역에서 사라집니다.<br/>취소 하시겠습니까?";
					marketingType = 'PARTIAL_VOID';
				}else{
					url = '/account/order/cancel/';
					msg = "취소 하시겠습니까?";
					marketingType = 'VOID';
				}

				//전체 form을 체크하여 체크된 아이템 처리
				UIkit.modal.confirm(msg, function(){

					Method.updateSubmitBtn( false );

					var $itemForm = Method.getFormByPartialItem( Method.$itemList.find('>li').find('input[type="checkbox"]:checked').closest('li') );

					Core.Utils.ajax(url + Method.cancelOrderId, "GET", $itemForm.serialize() + '&' + param, function(data){
					//Core.Utils.ajax(url + Method.cancelOrderId, "GET", "", function(data){
						var data = sandbox.rtnJson(data.responseText, true);
						var result = data['result'];
						if( result == true ){
							var marketingData = _GLOBAL.MARKETING_DATA();
							if( marketingData.useGa == true ){
								var marketingOption = {
									orderType : marketingType,
									orderId : Method.cancelOrderId
								};
								Core.ga.processor( marketingOption );
							}

							UIkit.modal.alert("취소 되었습니다.").on('hide.uk.modal', function() {
								window.location.reload();
							});
						}else if(result == "CUSTOM"){
							UIkit.modal.alert(data['errorMsgCustom']).on('hide.uk.modal', function() {
								window.location.reload();
							});
						}else{
							UIkit.modal.alert(data['errorMsg']).on('hide.uk.modal', function() {
								window.location.reload();
							});
						}
					}, true);
				}, function(){},
				{
					labels: {'Ok': '확인', 'Cancel': '취소'}
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order]',
					attrName:'data-module-order',
					moduleName:'module_order',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
