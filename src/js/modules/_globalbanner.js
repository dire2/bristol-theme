(function(Core){
	'use strict';

	Core.register('module_text_banner', function(sandbox){
		var Method = {
			moduleInit:function(){
				var $this = $(this);
				var $banner = $this.find('ul');

				var defaultOption = {
					onSliderLoad : function(){
						//$this.find('.text-wrap').width($this.find( ".bx-viewport" ).width() )
						//$this.show();
					},
					auto: true,	
					autoHover: true,
					autoDelay : 5000,
					adaptiveHeight: true,
					pager: false,
					useCSS: false,
					mode: "fade",
				}
				var slider = $banner.bxSlider(defaultOption);

				$(this).find('.bxslider-controls .btn-next').on('click', function(e) {
					e.preventDefault();
					slider.goToNextSlide();
				});
				$(this).find('.bxslider-controls .btn-prev').on('click', function(e) {
					e.preventDefault();
					slider.goToPrevSlide();
				});
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-text-banner]',
					attrName:'data-module-text-banner',
					moduleName:'module_text_banner',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
