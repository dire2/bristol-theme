(function(Core){
	Core.register('module_order_delivery', function(sandbox){
		var Method = {
			$popAddressModal :null,
			$beforeAddress:null,
			$newAddress:null,
			isNewAddress:false,
			isSelectAddress:false,
			moduleInit:function(){
				var $this = $(this);
				
				Method.$popAddressModal = UIkit.modal("#popup-customer-address", {modal: false});
				Method.$beforeAddress = $this.find('[data-before-address]');
				Method.$newAddress = $this.find('[data-new-address]');

				// 배송지 타입이 없는건 비회원이라는 뜻
				Method.isNewAddress = ( $this.find('[data-address-type]').length == 0 ) ? true : false;
				
				//비회원일 경우  배송지 수정으로 들어 갈경우 배송지 주소를 readonly 시켜준다.
				var url = window.location.href;
				var isSignIn = _GLOBAL.CUSTOMER.ISSIGNIN;
			    if( !isSignIn  &&  url.indexOf('edit-shipping=true') > -1  ){
					$('input[name="address.addressLine1"]').attr("readonly",true).css('background','#cccccc');
					$('.re_search').removeClass("uk-hidden");
					$('.btn_search.button.width-fix').hide();
			    }

				var $personalMsg = $(this).find('[name="personalMessageText"]');
				var $personalSelect = $(this).find('select[name="selectPersonalMessage"]');
				// select가 안되어있고 msg가 있으면 직접입력 처리
				if( $personalMsg.val() != "" && $personalSelect.val() =="" ){
					$personalSelect.val('dt_1');
					$personalMsg.closest(".input-textfield").removeClass('uk-hidden');
				}

				var $personalMsgSelect = sandbox.getComponents('component_select', {context:$this}, function(){
					this.addEvent("change", function(){
						var value = $(this).val();
						if(value == ''){
							$personalMsg.val('');
							$personalMsg.closest(".input-textfield").addClass('uk-hidden');
						}else if(value == 'dt_1'){
							// 직접입력일 경우
							$personalMsg.val('');
							$personalMsg.closest(".input-textfield").removeClass('uk-hidden');
						}else{
							//$personalMsg.val( $(this).find("option:selected").val() + "||" + $(this).find("option:selected").text() );
							$personalMsg.val( $(this).find("option:selected").text());
							$personalMsg.closest(".input-textfield").addClass('uk-hidden');
						}
					});
				});	

				Method.isSelectAddress = ( $(this).find('[name="isSearchAddress"]').val() == 'true' );
				var $zipCodeInput = $(this).find('[name="address.postalCode"]');
				var $zipCodeDisplay = $(this).find('[data-postalCode]');
				var deliverySearch = sandbox.getComponents('component_searchfield', {context:$this, selector:'.search-field', resultTemplate:'#address-find-list'}, function(){
					// 검색된 내용 선택시 zipcode 처리
					this.addEvent('resultSelect', function(data){
						var zipcode = $(data).data('zip-code5');
						var city = $(data).data('city');
						var doro = $(data).data('doro');	
						var address = Method.$newAddress.find('[name="address.addressLine1"]').val();

						// 비회원이거나 첫주문고객일 경우
						if(Method.isNewAddress){
							$('input[name="address.addressLine1"]').attr("readonly",true).css('background','#cccccc');
							$('.result-wrap li').remove();
							$(".re_search").removeClass("uk-hidden");
							$('.btn_search.button.width-fix').hide();
						}
						// 배송지 주소가 없을 경우
						if(address != '' ){
							$('input[name="address.addressLine1"]').attr("readonly",true).css('background','#cccccc');
							Method.$newAddress.find('.result-wrap li').remove();
							$(".re_search").removeClass('uk-hidden');
							$('.btn_search.button.width-fix').hide();
						}
						
						this.getInputComponent().setValue(city + ' ' + doro);

						$zipCodeInput.val( zipcode );
						$zipCodeDisplay.text( zipcode );
						$zipCodeDisplay.parent().removeClass("uk-hidden");
						Method.isSelectAddress = true;
					});
				});

				var $form = $this.find('#shipping_info');
				sandbox.validation.init( $form );

				// 배송지 정보 submit 시
				$this.find('[data-order-shipping-submit-btn]').on('click', function(e){
					e.preventDefault();
				    if ($('div a:first-child').hasClass('uk-active')){
				    	if($("input[name='address.addressLine2']").val() == "" || $("input[name='address.fullName']").val() == "" ){
							UIkit.modal.alert("배송지 정보가 올바르지 않습니다. 배송지를 새로 입력 하시거나 다른 배송지를 선택 하십시요." );
							return;
				    	}
				    }
					sandbox.validation.validate( $form );
					if(sandbox.validation.isValid( $form )){
						if( Method.isNewAddress ){
							if( !Method.isSelectAddress ){
								UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
								$('.uk-modal-close').text('Ok');
								return;
							}
							if($('input[name="address.postalCode"]').val() == '' ){
								UIkit.modal.alert("검색을 통하여 배송지를 입력해주세요.");
								$('.uk-modal-close').text('Ok');
								return;
							}
						}
						//주문서 입력시 010 11자리, 이외는 10자리로..
						if( ($form.find('[aria-expanded]').attr('aria-expanded')=="true")  || ($form.find('[aria-expanded]').length == 0)){  //이전주소 일 경우
							var phoneNumber = $form.find('input[name="address.phonePrimary.phoneNumber"]').eq(0).val();

							//기본 배송지 일경우  이름이 최소 2자 이어야 주문가능 하게 수정.
							if($form.find('input[name="address.fullName"]').val().length < 2){
								UIkit.modal.alert('이름은 2자 이상 가능 합니다.');
								return false;
							}
						}else{  //새로입력
							var phoneNumber = $form.find('input[name="address.phonePrimary.phoneNumber"]').eq(1).val();
						}

						//핸드폰, 일반 전화 패턴 정의
						var hp_defalult = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;    //새로 입력이 아닐경우, 기본 정규식 패턴 체크하기 위해서..
						var hp_pattern  = /^((01[16789])[1-9][0-9]{6,7})|(010[1-9][0-9]{7})$/;
						var cd_pattern  = /^(1[568][0456789][01456789][0-9]{4})|((01[16789])[1-9][0-9]{6,7})|(010[1-9][0-9]{7})|(050[0-9]{8,9})|((02|0[3-9][0-9])[0-9]{3,4}[0-9]{4})|(0001[568][0456789][01456789][0-9]{4})$/;
						var pattern_chk1 = false;      // false 로 기본 셋팅
						var pattern_chk2 = false;

						if(hp_defalult.test(phoneNumber)){
							pattern_chk1 = true;
						};

						if(hp_pattern.test(phoneNumber)){  //휴대폰 먼저 chk.
							pattern_chk2 = true;
						}else{
							if(cd_pattern.test(phoneNumber)){   //휴대폰 패턴이 false 경우, 일반 전화 패턴 chk.
								pattern_chk2 = true;
							};
						};

						if(!pattern_chk1 || !pattern_chk2) {    //검증 pattern_chk1, pattern_chk2 모두 true 이어야만..정상 연락처로....)
							UIkit.modal.alert('배송지 연락처를 정확하게 입력해 주세요!',{labels: {'Ok': '확인'}});
							return false;
						}
						
						$form.submit();
					}
				})


				// 배송지 선택 모듈 select 이벤트 호출( 배송지 선택했을때 호출됨 )
				Core.getComponents('component_customer_address', {context:$this}, function(){
					this.addEvent('select', function(data){
						Method.updateCustomerAddress( data );
						if( Method.$popAddressModal.isActive()){
							Method.$popAddressModal.hide();
						}
					})
				});	

				// 배송지 선택 버튼
				$this.find('[data-customer-address-btn]').on('click', function(e){
					e.preventDefault();
					Method.$popAddressModal.show();
				});				


				// 배송지 입력 타입 버튼 선택시
				$this.find('[data-address-type]').on('show.uk.switcher', function(){
					Method.isSelectAddress = deliverySearch.getValidateChk();
					Method.updateAddressInput();
				});

			}, 

			updateCustomerAddress:function( data ){
				var $target = Method.$beforeAddress;
				if( $target.find('[data-user-name]').length > 0 ){
					$target.find('[data-user-name]').html($.trim(data.fullName));
				}

				if( $target.find('[data-phone]').length > 0 ){
					$target.find('[data-phone]').html($.trim(data.phoneNumber));
				}

				if( $target.find('[data-postalCode]').length > 0 ){
					$target.find('[data-postalCode]').html($.trim(data.postalCode));
				}	

				if( $target.find('[data-address]').length > 0 ){
					$target.find('[data-address]').html($.trim(data.addressLine1 + ' ' + data.addressLine2));
				}

				/*
				if( $target.find('[data-address2]').length > 0 ){
					$target.find('[data-address2]').text(data.addressLine2);
				}
				*/

				// 변경된 값 input 에 적용 
				$target.find('input[name="address.fullName"]').val($.trim(data.fullName));
				$target.find('input[name="address.phonePrimary.phoneNumber"]').val($.trim(data.phoneNumber));
				$target.find('input[name="address.addressLine1"]').val($.trim(data.addressLine1));
				$target.find('input[name="address.addressLine2"]').val($.trim(data.addressLine2));
				$target.find('input[name="address.postalCode"]').val($.trim(data.postalCode));
			},

			updateAddressInput:function(){
				if( Method.$beforeAddress.hasClass('uk-active')){
					Method.isNewAddress = false;
					Method.$beforeAddress.find('input').attr('disabled', false );
					Method.$newAddress.find('input').attr('disabled', true );
				}else{
					Method.isNewAddress = true;
					Method.$beforeAddress.find('input').attr('disabled', true );
					Method.$newAddress.find('input').attr('disabled', false );
				}				
			}
		}

		return {
			init:function(){
				sandbox.uiInit({
					selector:'[data-module-order-delivery]',
					attrName:'data-module-order-delivery',
					moduleName:'module_order_delivery',
					handler:{context:this, method:Method.moduleInit}
				});
			}
		}
	});
})(Core);
